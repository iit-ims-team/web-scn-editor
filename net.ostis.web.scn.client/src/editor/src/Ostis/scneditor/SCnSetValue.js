addInitClass(function () {

    $.extend(Ostis.scneditor.SCnSetValue.prototype, {
        hasChildren: function () {

            return true;
        },

        createJQueryNode: function () {

            var $div = $("<div/>");
            $div.append("<div>{</div>");
            var $addElementInput = $("<input/>");
            $addElementInput.addClass("scn-input");
            $addElementInput.attr("placeholder", "Новый узел");
            var thisSet = this;

            $addElementInput.keydown(function (keydownEvent) {
                if (keydownEvent.keyCode !== Ostis.scneditor.KeyCode.UP_CODE
                    && keydownEvent.keyCode !== Ostis.scneditor.KeyCode.DOWN_CODE
                    && keydownEvent.ctrlKey === false) {
                    keydownEvent.stopPropagation();
                }
                if (thisSet.$node !== undefined && thisSet.$node.parent().find(".tt-suggestion").length !== 0 &&
                    (keydownEvent.keyCode === Ostis.scneditor.KeyCode.UP_CODE
                    || keydownEvent.keyCode === Ostis.scneditor.KeyCode.DOWN_CODE)) {
                    keydownEvent.stopPropagation();
                    return false;
                }
            });
            $addElementInput.bind("keyup", function (evt) {
                if (evt.keyCode == Ostis.scneditor.KeyCode.ENTER_CODE && $addElementInput.val().trim().length != 0) {
                    thisSet.addElement($addElementInput.val());
                    $addElementInput.val('');
                    $addElementInput.siblings("span.tt-dropdown-menu").css('display', 'none');
                    $addElementInput.siblings("input.tt-hint").val('');
                }
                if (evt.keyCode == Ostis.scneditor.KeyCode.ESC_CODE && $addElementInput.val().trim().length != 0) {
                    _resetTypeahead();
                }
            });

            $addElementInput.bind("blur", _resetTypeahead);

            //See issue #112
            function _resetTypeahead() {
                $addElementInput.typeahead('val', '');
            }

            Ostis.scneditor.SCnSetValue.addAutocompletion(this, $addElementInput);
            $addElementInput.parent().addClass("add-node");

            $div.append($addElementInput.parent());
            $div.append("<div>}</div>");

            this.bindSelectNodeByClick($div);
            this.$node = $div;
        },

        addSCNode: function (scNode) {

            this.insertSCNode(scNode, this.getSCArcs().length);
        },

        insertSCNode: function (scNode, indexNode) {

            if (indexNode != 0) {
                this.scArcs[indexNode - 1].getJQueryNode().after(scNode.getJQueryNode());
            } else {
                if (this.scArcs.length === 0) {
                    this.getJQueryNode().find("span.twitter-typeahead").before(scNode.getJQueryNode());
                } else {
                    this.scArcs[0].getJQueryNode().before(scNode.getJQueryNode());
                }
            }
            this.scArcs.splice(indexNode, 0, scNode);
        },

        getSCArcs: function () {

            return this.scArcs;
        },

        getDelCommand: function () {

            return this.parentSCNode.getDelCommand();
        },

        removeSCArc: function (scArcOfSet) {

            for (var arcIndex = 0; arcIndex < this.scArcs.length; ++arcIndex) {
                if (this.scArcs[arcIndex].getId() === scArcOfSet.getId()) {
                    this.scArcs[arcIndex].getJQueryNode().detach();
                    this.scArcs.splice(arcIndex, 1);
                    break;
                }
            }
        },

        insertSCArc: function (newArc, indexArc) {

            if (indexArc != 0) {
                this.scArcs[indexArc - 1].getJQueryNode().after(newArc.getJQueryNode());
            } else {
                if (this.scArcs.length === 0) {
                    this.getJQueryNode().find("span.twitter-typeahead").before(newArc.getJQueryNode());
                } else {
                    this.scArcs[0].getJQueryNode().before(newArc.getJQueryNode());
                }
            }
            this.scArcs.splice(indexArc, 0, newArc);
        },

        setFocus: function () {

            // Do nothing.
        },

        edit: function () {

            this.getJQueryNode().find('input:last-of-type').putCursorAtEnd();
            return false;
        },

        getSelectableSCnNodes: function () {

            var selectableNodes = [];
            selectableNodes.push(this);
            Ostis.scneditor.ArrayUtils.addAll(AbstractSCnElement.getSelectableNodesFromArray(this.getSCArcs()), selectableNodes);
            return selectableNodes;
        },

        hasSelecting: function () {

            var hasSelecting = false;
            for (var scNodeIndex = 0; scNodeIndex < this.getSCArcs().length; ++scNodeIndex) {
                var scNode = this.getSCArcs()[scNodeIndex];
                if (scNode.hasSelecting() === true) {
                    hasSelecting = true;
                    break;
                }
            }
            return hasSelecting;
        },

        toJSON: function () {

            var jsonRepresentation = {};
            jsonRepresentation.SCNodes = [];
            this.getSCArcs().forEach(function (scNode) {

                jsonRepresentation.SCNodes.push(scNode.toJSON());
            });
            jsonRepresentation.set = true;
            jsonRepresentation.scaddr = this.scaddr;
            jsonRepresentation.obj = this;
            return jsonRepresentation;
        },

        childWasLeftClick: function (keydownEvent) {

            this.parentSCNode.childWasLeftClick(keydownEvent);
        },

        ctrlEnterClick: function () {

            this.addElement();
            return false;
        },

        addElement: function (textValue, scAddr) {

            Ostis.scneditor.addCommand(new Ostis.scneditor.AddSCNodeCommand(this, new Ostis.scneditor.SCnArcOfSet(this, textValue, scAddr)));
        },

        clone: function (parent) {

            var clone = new Ostis.scneditor.SCnSetValue(parent);
            this.getSCArcs().forEach(function (scNode) {

                clone.addSCNode(scNode.clone(clone));
            });
            return clone;
        },

        canAddSCNodeOfSet: function () {

            return true;
        },

        setSelected: function (selectedChildNode) {

            var addNodeSpan = this.getJQueryNode().find(".add-node")[0];
            if (addNodeSpan.isEqualNode(selectedChildNode)) {
                this.edit();
            }

            Ostis.scneditor.SCnSetValue.superclass.setSelected.apply(this);
            var $div = this.getJQueryNode().find(">div");
            $div.first().addClass("selectedSetBracket");
            $div.last().addClass("selectedSetBracket");
        },

        leaveSelecting: function () {

            Ostis.scneditor.SCnSetValue.superclass.leaveSelecting.apply(this);
            var $div = this.getJQueryNode().find(">div");
            $div.first().removeClass("selectedSetBracket");
            $div.last().removeClass("selectedSetBracket");
        },

        getSCArcIndex: function (childArc) {

            for (var childArcIndex = 0; childArcIndex < this.getSCArcs().length; ++childArcIndex) {
                if (this.getSCArcs()[childArcIndex].getId() == childArc.getId()) {
                    return childArcIndex;
                }
            }
        }
    });

    Ostis.scneditor.SCnSetValue.addAutocompletion = function (thisSet, $inputElement) {

        $inputElement.addClass('typeahead');
        $inputElement.addClass('scn-input');
        $inputElement.typeahead({
                minLength: 3,
                highlight: true
            },
            {
                name: 'idtf',
                source: function (query, cb) {
                    $inputElement.addClass('search-processing');
                    SCWeb.core.Server.findIdentifiersSubStr(query, function (data) {
                        keys = [];
                        for (key in data) {
                            var list = data[key];
                            for (idx in list) {
                                var value = list[idx]
                                keys.push({name: value[1], addr: value[0], group: key});
                            }
                        }

                        cb(keys);
                        $inputElement.removeClass('search-processing');
                    });
                },
                displayKey: 'name',
                templates: {
                    suggestion: function (item) {

                        //glyphicon glyphicon-globe
                        var html = '';
                        if (item.group === scKeynodes.nrel_idtf) {
                            return '<p class="sc-content">' + item.name + '</p>';
                        } else {
                            var cl = 'glyphicon glyphicon-user';
                            if (item.group === scKeynodes.nrel_system_idtf) {
                                cl = 'glyphicon glyphicon-globe';
                            }
                            return '<p><span class="tt-suggestion-icon ' + cl + '"></span>' + item.name + '</p>';
                        }
                        return '<p>' + item.name + '</p>';
                    }
                }
            }
        ).bind('typeahead:selected', function (evt, item) {
            $inputElement.val('');
            thisSet.addElement(item.name, item.addr);
        });

        //Somehow, typehead plugin sets spellcheck to false, so, for now, i'll set it manually here
        $inputElement.attr('spellcheck', true);
    };
});
