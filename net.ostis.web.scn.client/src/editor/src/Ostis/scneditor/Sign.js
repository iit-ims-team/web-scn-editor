addInitClass(function () {

    $.extend(Ostis.scneditor.Sign.prototype, {
        getText: function () {

            if (this.isForward == true) {
                return this.forwardSign;
            }
            if (this.isForward != true) {
                return this.backwardSign;
            }
        },

        clone: function () {

            var clone = new Ostis.scneditor.Sign(this.forwardSign, this.backwardSign, this.imgName);
            clone.setForward(this.isForward);
            return clone;
        },

        setForward: function (isForward) {
            this.isForward = isForward;
        },

        changeDirection: function () {

            this.isForward = !this.isForward;
        },

        equals: function (otherSign) {

            return this.isForward === otherSign.isForward && this.forwardSign === otherSign.forwardSign;
        }
    });

    Ostis.scneditor.Sign.getSignBySymbol = function (symbol) {

        var signs = Ostis.scneditor.Sign.getAll();
        for (var signIndex = 0; signIndex < signs.length; ++signIndex) {
            if (signs[signIndex].forwardSign === symbol) {
                return signs[signIndex].clone();
            }
            if (signs[signIndex].backwardSign === symbol) {
                var resultSign = signs[signIndex].clone();
                resultSign.setForward(false);
                return resultSign;
            }
        }
    };

    Ostis.scneditor.Sign.getAll = function (symbol) {

        return Ostis.scneditor.Sign.getConstantSigns(symbol).concat(Ostis.scneditor.Sign.getVariablesSigns(symbol),
            Ostis.scneditor.Sign.getOtherSigns(symbol));
    };

    Ostis.scneditor.Sign.getConstantSigns = function (symbol) {

        return [new Ostis.scneditor.Sign('∍', '∊', "10.png"), new Ostis.scneditor.Sign('⇒', '⇐', "4.png"),
            new Ostis.scneditor.Sign('⇔', '⇔', "18.png"), new Ostis.scneditor.Sign('∌', '∉', "6.png"),
            new Ostis.scneditor.Sign('/∍', '∊/', "9.png"), new Ostis.scneditor.Sign('~∍', '∊~', "12.png"),
            new Ostis.scneditor.Sign('~∌', '∉~', "14.png"), new Ostis.scneditor.Sign('~/∍', '∊/~', "16.png")];
    };

    Ostis.scneditor.Sign.getVariablesSigns = function (symbol) {

        return [new Ostis.scneditor.Sign('_∍', '_∊', "7.png"), new Ostis.scneditor.Sign('_⇒', '_⇐', "5.png"),
            new Ostis.scneditor.Sign('_⇔', '_⇔', "19.png"), new Ostis.scneditor.Sign('_∌', '_∉', "8.png"),
            new Ostis.scneditor.Sign('_/∍', '_∊/', "11.png"), new Ostis.scneditor.Sign('_~∍', '_∊~', "13.png"),
            new Ostis.scneditor.Sign('_~∌', '_∉~', "15.png"), new Ostis.scneditor.Sign('_~/∍', '_~/∍', "17.png")];
    };

    Ostis.scneditor.Sign.getOtherSigns = function (symbol) {

        return [new Ostis.scneditor.Sign("=", "=", "SCSynonims.png")];
    };
});
