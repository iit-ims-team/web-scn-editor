addInitClass(function () {

    $.extend(Ostis.scneditor.AbstractFileNodeValue.prototype, {
        getJQueryNode: function () {

            if (this.$node == null) {
                this.createJQueryNode();
            }
            return this.$node;
        },

        createJQueryNode: function () {

            var $SCnNoTransContent = $("<span/>");
            $SCnNoTransContent.addClass("SCnNoTransContent");
            this.$node = $SCnNoTransContent;
        },

        setFocus: function () {

            // Do nothing.
        },

        edit: function () {

            // Do nothing.
        },

        hasChildren: function () {

            return false;
        },

        hasSelecting: function () {

            return false;
        },

        toJSON: function () {

            var jsonRepresentation = {};
            jsonRepresentation.file = true;
            jsonRepresentation.fileName = this.fileName;
            jsonRepresentation.scaddr = this.scaddr;
            jsonRepresentation.obj = this;
            return jsonRepresentation;
        },

        getSelectableSCnNodes: function () {

            return [];
        },

        leaveSelecting: function () {

            // Do nothing.
        }
    });
});
