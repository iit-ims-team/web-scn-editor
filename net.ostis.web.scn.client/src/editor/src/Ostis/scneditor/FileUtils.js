Ostis.scneditor.FileUtils.isImage = function (fileName) {

    var extension = fileName.split(".")[fileName.split(".").length - 1];
    var imageExtensions = ["bmp", "jpeg", "jpg", "png", "gif"];
    for (var i = 0; i < imageExtensions.length; i++) {
        if (imageExtensions[i].toUpperCase() === extension.toUpperCase()) {
            return true;
        }
    }
    return false;
};
