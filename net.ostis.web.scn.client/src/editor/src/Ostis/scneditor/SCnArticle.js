addInitClass(function () {

    $.extend(Ostis.scneditor.SCnArticle.prototype, {
        createJQueryNode: function () {

            this.$node = $("#SCnEditorAliasWorkbenchEditor");
            var $SCnKeyword = $("<div/>");
            $SCnKeyword.addClass("SCnKeyword");
            $SCnKeyword.append(this.nodeValue.getJQueryNode());
            this.$node.append($SCnKeyword);
            this.bindSelectNodeByClick(this.getJQueryNode());
        },

        addFirstSCArcJQueryNode: function ($arcNode) {

            this.getJQueryNode().append($arcNode);
        },

        getDelCommand: function () {

            return new Ostis.scneditor.CleanSCnArticleCommand(this);
        },

        clean: function () {

            this.setText(Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER);
            this.removeAllSCArcs();
        },

        leftClick: function (keydownEvent) {

            var isShiftKey = keydownEvent === undefined ? false : keydownEvent.shiftKey;
            Ostis.scneditor.SCnEditor.setSelectedSCnNode(this, isShiftKey);
        }
    });

    Ostis.scneditor.SCnArticle.newInstance = function () {

        Ostis.scneditor.SCnArticle.scnArticle = null;
        Ostis.scneditor.SCnArticle.historyElement = undefined;
        return Ostis.scneditor.SCnArticle.getInstance();
    };

    Ostis.scneditor.SCnArticle.getInstance = function () {

        if (this.scnArticle == null) {
            this.scnArticle = new Ostis.scneditor.SCnArticle();
            Ostis.scneditor.SCnArticle.initializePage(this.scnArticle);
        }
        return this.scnArticle;
    };

    Ostis.scneditor.SCnArticle.configToolbar = function () {

        $("#SCnEditorAliasWorkbenchToolbar").toolbar({
            method: "configDisabling",
            isSelectedNodeArc: Ostis.scneditor.SCnEditor.isSelectedNodeArc(),
            canSelectedNodeAddArc: Ostis.scneditor.SCnEditor.canSelectedNodeAddArc(),
            canSelectedNodeChangeNodeValue: Ostis.scneditor.SCnEditor.canSelectedNodeChangeNodeValue()
        });
    };

    Ostis.scneditor.SCnArticle.getHistoryElement = function () {

        if (Ostis.scneditor.SCnArticle.historyElement === undefined) {
            Ostis.scneditor.SCnArticle.historyElement = $('#SCnEditorAliasWorkbenchCanvasPanel');
        }
        return Ostis.scneditor.SCnArticle.historyElement;
    };

    Ostis.scneditor.SCnArticle.initializePage = function (scnArticle) {

        Ostis.scneditor.SCnArticle.setDefaultAddingArcSign(Ostis.scneditor.Sign.getSignBySymbol("∊"));
        Ostis.scneditor.SCnArticle.getHistoryElement().historyPanel();
        Ostis.scneditor.SCnArticle.createToolButtons();
        Ostis.scneditor.SCnEditor.setSelectedSCnNode(scnArticle);
        Ostis.scneditor.SCnArticle.bindKeyboardEvents();
        Ostis.scneditor.commands = new Array();
    };

    Ostis.scneditor.SCnArticle.bindKeyboardEvents = function () {
        $(window).on("keydown", _keydownHandler);
    };

    Ostis.scneditor.SCnArticle.unbindKeyboardEvents = function () {
        $(window).off("keydown", _keydownHandler);
    };

    function _keydownHandler(keydownEvent) {
        if (keydownEvent.isPropagationStopped()) {
            return false;
        }
        if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.UP_CODE) {
            return Ostis.scneditor.SCnEditor.changeAllocationToTop(keydownEvent);
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.DOWN_CODE) {
            return Ostis.scneditor.SCnEditor.changeAllocationToBottom(keydownEvent);
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.DEL_CODE) {
            Ostis.scneditor.SCnArticle.removeSelectedSCnNode();
            return false;
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.Z_CODE && keydownEvent.ctrlKey == true) {
            Ostis.scneditor.unexecuteCommand();
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.Y_CODE && keydownEvent.ctrlKey == true) {
            Ostis.scneditor.executeUnexecutedCommand();
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.I_CODE) {
            return Ostis.scneditor.SCnEditor.editSelectedNode();
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.RIGHT_CODE) {
            Ostis.scneditor.SCnEditor.rightClickToSelectedNode(keydownEvent);
            return false;
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.LEFT_CODE) {
            Ostis.scneditor.SCnEditor.leftClickToSelectedNode(keydownEvent);
            return false;
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.K_CODE && keydownEvent.ctrlKey == true) {
            return Ostis.scneditor.SCnEditor.changeArcTypeClickToSelectedNode();
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.T_CODE) {
            Ostis.scneditor.SCnEditor.tClickToSelectedNode();
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.ENTER_CODE && keydownEvent.ctrlKey == true) {
            return Ostis.scneditor.SCnEditor.ctrlEnterClickToSelectedNode();
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.S_CODE && keydownEvent.ctrlKey == true) {
            Ostis.scneditor.SCnArticle.save();
            return false;
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.C_CODE && keydownEvent.ctrlKey == true) {
            Ostis.scneditor.SCnEditor.copySelectedNode();
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.V_CODE && keydownEvent.ctrlKey == true) {
            if (Ostis.scneditor.SCnEditor.pasteCopiedNode() === true) {
                keydownEvent.preventDefault();
            }
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.X_CODE && keydownEvent.ctrlKey == true) {
            Ostis.scneditor.SCnEditor.cutSelectedNode();
        } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.R_CODE) {
            Ostis.scneditor.SCnEditor.rClickToSelectedNode();
        } else if (keydownEvent.shiftKey == true && keydownEvent.keyCode === Ostis.scneditor.KeyCode.E_CODE) {
            return Ostis.scneditor.SCnEditor.changeSelectedNodeValueToInput();
        } else if (keydownEvent.ctrlKey == true) {
            if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.M_CODE) {
                return Ostis.scneditor.SCnEditor.changeSelectedNodeValueToSet();
            } else if (keydownEvent.keyCode === Ostis.scneditor.KeyCode.Q_CODE) {
                return Ostis.scneditor.SCnEditor.changeSelectedNodeValueToContour();
            }
        }
    }

    Ostis.scneditor.SCnArticle.removeSelectedSCnNode = function () {

        Ostis.scneditor.SCnEditor.delClickToSelectedNode();
    };

    Ostis.scneditor.SCnArticle.createToolButtons = function () {

        $("#SCnEditorAliasWorkbenchToolbar").toolbar({
            addArcClickCallback: Ostis.scneditor.SCnEditor.addSCArcClickToSelectedNode,
            addAttributeClickCallback: Ostis.scneditor.SCnEditor.addAttributeClickToSelectedNode,
            changeArcTypeClickCallback: Ostis.scneditor.SCnEditor.changeArcTypeClickToSelectedNode,
            toContourClickCallback: Ostis.scneditor.SCnEditor.changeSelectedNodeValueToContour,
            toSetClickCallback: Ostis.scneditor.SCnEditor.changeSelectedNodeValueToSet,
            changeLinkClickCallback: Ostis.scneditor.SCnEditor.changeLinkClickCallback,
            toInputClickCallback: Ostis.scneditor.SCnEditor.changeSelectedNodeValueToInput,
            toJsonClickCallback: Ostis.scneditor.SCnArticle.exportToJSON,
            toScsClickCallback: Ostis.scneditor.SCnArticle.exportToSCS,
            downloadClickCallback: function () {

                var $input = $('#downloadArticleNameInput');  //TODO: refactor
                Ostis.scneditor.SCnEditor.getDao().downloadArticle($input.val(), function (data) {

                    Ostis.scneditor.addCommand(new Ostis.scneditor.ConvertFromJSONCommand(Ostis.scneditor.SCnArticle.getInstance(), data));
                });
                return false;
            },

            saveClickCallback: Ostis.scneditor.SCnArticle.save,
            uploadFileCallback: function (fileName) {

                if (Ostis.scneditor.FileUtils.isImage(fileName) === true) {
                    Ostis.scneditor.SCnEditor.changeSelectedNodeValueToImage(fileName);
                } else {
                    Ostis.scneditor.SCnEditor.changeSelectedNodeValueToFile(fileName);
                }
            },
            selectTemplateCallback: function (articleJson) {

                Ostis.scneditor.addCommand(new Ostis.scneditor.ConvertFromJSONCommand(Ostis.scneditor.SCnArticle.getInstance(), articleJson));
            },
            toggleSettingsNode: function () {

                if (getUser() && !$('#settingsContainer').length) {
                    Ostis.scneditor.editorSettings.renderNode($('#SCnEditorAliasWorkbenchEditor'))
                }
            },
            signGroups: [{
                title: "Константные sc-коннекторы",
                signs: Ostis.scneditor.Sign.getConstantSigns()
            }, {
                title: "Переменные sc-коннекторы",
                signs: Ostis.scneditor.Sign.getVariablesSigns()
            }, {
                title: "Прочие sc-коннекторы",
                signs: Ostis.scneditor.Sign.getOtherSigns()
            }]
        });
    };

    Ostis.scneditor.SCnArticle.exportToJSON = function () {

        var jsonArticle = Ostis.scneditor.SCnArticle.getInstance().toJSON();
        var blob = new Blob([JSON.stringify(jsonArticle)], {
            type: "text/plain;charset=utf-8"
        });
        saveAs(blob, "article.json.txt");
    };

    Ostis.scneditor.SCnArticle.exportToSCS = function () {
        var jsonArticle = Ostis.scneditor.SCnArticle.getInstance().toJSON();
        var scsArr = [];
        var convertArticle = Ostis.scneditor.JsonToScsConverter.convertArticle(jsonArticle, scsArr);

        convertArticle.done(function () {
            var blob = new Blob([scsArr[0]], {
                type: "text/plain;charset=utf-8"
            });
            saveAs(blob, "article.scs.txt");
        });
    }

    Ostis.scneditor.SCnArticle.bindMakeKeywordEditableByDblClick = function ($keywordSpan, scnArticle) {

        $keywordSpan.bind("dblclick", function () {

            var $SCnKeyword = $(this).parent();
            $(this).detach();
            Ostis.scneditor.SCnArticle.addKeywordInputPart($SCnKeyword, scnArticle);
        });
    };

    Ostis.scneditor.SCnArticle.save = function () {

        Ostis.scneditor.SCnEditor.getDao().saveArticle(Ostis.scneditor.SCnArticle.getInstance().toJSON());
    };

    Ostis.scneditor.SCnArticle.setDefaultAddingArcSign = function (defaultAddingArcSign) {

        Ostis.scneditor.SCnArticle.defaultAddingArcSign = defaultAddingArcSign;
    };

    Ostis.scneditor.SCnArticle.getDefaultAddingArcSign = function () {

        return Ostis.scneditor.SCnArticle.defaultAddingArcSign.clone();
    };

    Ostis.scneditor.SCnArticle.isChanged = function () {

        if (Ostis.scneditor.commands[0] === undefined || Ostis.scneditor.lastExecutedCommandIndex === -1) {
            return true;
        } else if (Ostis.scneditor.lastExecutedCommandIndex > 0 ||
            Ostis.scneditor.commands[0].description() != 'Конвертация из json') {
            return confirm(Ostis.scneditor.resources['canselChangesQuestion']);
        } else {
            return true;
        }
    };

});
