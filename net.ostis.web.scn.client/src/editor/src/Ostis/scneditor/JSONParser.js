Ostis.scneditor.JSONParser = function () {

};

Ostis.scneditor.JSONParser.makeSCArticleFromJSON = function (jsonRepresentation, article) {

    article.setText(jsonRepresentation.label);
    article.scaddr = jsonRepresentation.scaddr;
    Ostis.scneditor.JSONParser.addEdgesToSCNode(article, jsonRepresentation.SCNodes[0]);
    return article;
};

Ostis.scneditor.JSONParser.makeSCArcFromJSON = function (jsonRepresentation, parentSCNode) {

    var scArc = new Ostis.scneditor.BinaryRelation(Ostis.scneditor.Sign.getSignBySymbol(jsonRepresentation.type), parentSCNode);
    scArc.scaddr = jsonRepresentation.scaddr;
    scArc.setSCNode(Ostis.scneditor.JSONParser.makeSCNodeFromJSON(jsonRepresentation.SCNode, scArc));
    scArc.createJQueryNode();
    jsonRepresentation.SCAttributes.forEach(function (attr) {

        scArc.addSCAttribute(Ostis.scneditor.JSONParser.makeSCAttributeFromJSON(attr, scArc));
    });
    return scArc;
};

Ostis.scneditor.JSONParser.makeSCSynonimFromJSON = function (synonimJSON, parentSCNode) {

    var scArc = new Ostis.scneditor.BinaryRelation(Ostis.scneditor.Sign.getSignBySymbol("="), parentSCNode);
    scArc.scaddr = synonimJSON.scaddr;
    scArc.setSCNode(Ostis.scneditor.JSONParser.makeSCNodeFromJSON(synonimJSON, scArc));
    return scArc;
};

Ostis.scneditor.JSONParser.makeSCAttributeFromJSON = function (jsonRepresentation, parentSCNode) {

    var attribute = new Ostis.scneditor.SCAttribute(parentSCNode);
    attribute.scaddr = jsonRepresentation.scaddr;
    attribute.setText(jsonRepresentation.label);
    return attribute;
};

Ostis.scneditor.JSONParser.makeSCNodeFromJSON = function (nodeJSON, parentSCNode) {

    var scNode = new Ostis.scneditor.SCnNode(parentSCNode);
    scNode.scaddr = nodeJSON.scaddr;
    scNode.setNodeValue(Ostis.scneditor.JSONParser.getNodeValue(nodeJSON, scNode));
    Ostis.scneditor.JSONParser.addEdgesToSCNode(scNode, nodeJSON);
    return scNode;
};

Ostis.scneditor.JSONParser.makeSCnArcOfSetFromJSON = function (nodeJSON, parentSCNode) {

    var scNode = new Ostis.scneditor.SCnArcOfSet(parentSCNode);
    scNode.scaddr = nodeJSON.scaddr;
    scNode.setSCNode(Ostis.scneditor.JSONParser.makeSCNodeFromJSON(nodeJSON, scNode));
    return scNode;
};

Ostis.scneditor.JSONParser.getNodeValue = function (nodeJSON, parentNode) {

    if (nodeJSON.set === true) {
        var scSet = new Ostis.scneditor.SCnSetValue(parentNode);
        scSet.scaddr = nodeJSON.scaddr;
        nodeJSON.SCNodes.forEach(function (scNode) {

            scSet.addSCNode(Ostis.scneditor.JSONParser.makeSCnArcOfSetFromJSON(scNode, scSet));
        });
        return scSet;
    } else if (nodeJSON.contour === true) {
        var scContour = new Ostis.scneditor.SCnContourValue(parentNode);
        // parentNode is contour
        // first parentSCNode is SCSynonim
        // second parentSCNode is root node (root node = contour)
        scContour.scaddr = parentNode.parentSCNode.parentSCNode.scaddr;
        scContour.nodeValue.scaddr = nodeJSON.scaddr;
        scContour.setText(nodeJSON.SCNodes[0].label);
        Ostis.scneditor.JSONParser.addEdgesToSCNode(scContour, nodeJSON.SCNodes[0]);
        return scContour;
    } else if (nodeJSON.file === true) {
        var result;
        if (Ostis.scneditor.FileUtils.isImage(nodeJSON.fileName) === false) {
            result = new Ostis.scneditor.FileNodeValue(nodeJSON.fileName);
        } else {
            result = new Ostis.scneditor.ImageFileNodeValue(nodeJSON.fileName);
        }
        result.scaddr = nodeJSON.scaddr;
        return result;
    } else {
        var nodeValue = new Ostis.scneditor.SCnNodeValue();
        nodeValue.scaddr = nodeJSON.scaddr;
        nodeValue.setText(nodeJSON.label);
        return nodeValue;
    }
};

Ostis.scneditor.JSONParser.addEdgesToSCNode = function (scNode, nodeJSON) {

    nodeJSON.SCArcs.forEach(function (scArcJSON) {

        scNode.addSCArc(Ostis.scneditor.JSONParser.makeSCArcFromJSON(scArcJSON, scNode));
    });
    nodeJSON.SCSynonims.forEach(function (synonim) {

        scNode.addSCArc(Ostis.scneditor.JSONParser.makeSCSynonimFromJSON(synonim, scNode));
    });
};
