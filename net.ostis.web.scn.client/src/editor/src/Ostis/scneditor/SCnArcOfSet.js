addInitClass(function () {

    $.extend(Ostis.scneditor.SCnArcOfSet.prototype, {

        getDelCommand: function () {

            return new Ostis.scneditor.RemoveSCArcCommand(this, this.parentSCNode);
        },

        remove: function () {

            var hasChangeAllocationToBottom = false;
            if (this.hasSelecting() === true) {
                Ostis.scneditor.SCnEditor.setSelectedSCnNode(this);
                Ostis.scneditor.SCnEditor.changeAllocationToTop();
                hasChangeAllocationToBottom = true;
            }
            this.parentSCNode.removeSCArc(this);
            if (hasChangeAllocationToBottom === true) {
                Ostis.scneditor.SCnEditor.changeAllocationToBottom();
            }
        },

        tClick: function () {

            // Do nothing.
        },

        toJSON: function () {

            return this.scNode.toJSON();
        },

        clone: function (parent) {

            var clone = new Ostis.scneditor.SCnArcOfSet(parent);
            clone.setSCNode(this.scNode.clone(clone));
            clone.createJQueryNode();
            this.getSCAttributes().forEach(function (attribute) {

                clone.addSCAttribute(attribute.clone(clone));
            });
            return clone;
        },

        appendTo: function (parent) {

            this.parentSCNode = parent;
            parent.addSCNode(this);
        },

        canAppendTo: function (scNode) {

            return scNode.canAddSCNodeOfSet();
        }
    });
});
