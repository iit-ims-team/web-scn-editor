(function ($) {

    $.fn.uploadComponent = function (userOptions) {

        var defaultOptions = {
            callback: function () {

            }
        };
        var options = $.extend({}, defaultOptions, userOptions);
        return this.each(function () {

            var $this = $(this);
            $this.bind("change", function () {

                var reader = new FileReader();
                reader.onload = function (e) {

                    var formData = new FormData();
                    formData.append("uploadingFile", $this.get(0).files[0]);
                    Ostis.scneditor.SCnEditor.getDao().uploadFile(formData, options.callback);
                };
                reader.readAsBinaryString($this.get(0).files[0]);
            });
        });
    };
})(jQuery);
