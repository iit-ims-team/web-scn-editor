package net.ostis.dbapi;

import java.sql.*;

/**
 * Created by antonkw on 02.12.14.
 */
public class SettingSetterObject {

    public void setAllSettings (String entryKey, String id,  String identValue, String generalSkinName, String backgroundColor, String uiFontColor, String contentTextColor, String scElementsSkinName, String isFileSavePanelOn) throws SQLException, ClassNotFoundException {

        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        System.out.print("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!rs.next())
            return;
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            return;
        }

        String sql = "UPDATE userSettings SET identValue = " + identValue + ", generalSkinName = '" + generalSkinName +  "', backgroundColor = '" + backgroundColor + "', uiFontColor = '" + uiFontColor + "', contentTextColor = '" + contentTextColor +
                "', scElementsSkinName = '" + scElementsSkinName + "', isFileSavePanelOn = '" + isFileSavePanelOn + "' WHERE id = '" + id  + "';";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();

    }
    public void setIdent (String entryKey, String id, String identValue) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!rs.next())
            return;
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            return;
        }
        String sql = "UPDATE userSettings SET identValue = '" + identValue + "' WHERE id = '" + id + "';";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }
    public void setGeneralSkinName(String entryKey, String id, String generalSkinName) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!rs.next())
            return;
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            return;
        }
        String sql = "UPDATE userSettings SET generalSkinName = '" + generalSkinName + "' WHERE id = '" + id + "';";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }
    public void setBackGroundColor(String entryKey, String id, String backgroundColor) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!rs.next())
            return;
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            return;
        }
        String sql = "UPDATE userSettings SET backgroundColor = '" + backgroundColor + "' WHERE id = '" + id + "';";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }
    public void setUiFontColor(String entryKey, String id, String uiFontColor) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!rs.next())
            return;
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            return;
        }
        String sql = "UPDATE userSettings SET uiFontColor = '" + uiFontColor + "' WHERE id = '" + id + "';";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }
    public void setContentTextColor(String entryKey, String id, String contentTextColor) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!rs.next())
            return;
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            return;
        }
        String sql = "UPDATE userSettings SET contentTextColor = '" + contentTextColor + "' WHERE id = '" + id + "';";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }
    public void setScElementsSkinName(String entryKey, String id, String scElementsSkinName) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!rs.next())
            return;
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            return;
        }
        String sql = "UPDATE userSettings SET scElementsSkinName = '" + scElementsSkinName + "' WHERE id = '" + id + "';";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }
    public void setIsFileSavePanelOn(String entryKey, String id, String isFileSavePanelOn) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!rs.next())
            return;
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            return;
        }
        String sql = "UPDATE userSettings SET isFileSavePanelOn = '" + isFileSavePanelOn + "' WHERE id = '" + id + "';";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }

    public boolean isEntryCodeTrue(String entryKey, String id) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (rs.next()){
            return Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"));
        }
        return false;
    }

}
