<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<%@ page import="net.ostis.dbapi.*" %>
<%@ page import="javax.servlet.*" %>

<html>
    <head>
        <title>Editor</title>
        <link rel="stylesheet" href="./resources/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="./resources/bootstrap-multilevel.css" type="text/css" />
        <link rel="stylesheet" href="./resources/scn_editor.css" type="text/css" />
        <link rel="stylesheet" href="./resources/server.css" type="text/css" />
        <link rel="stylesheet" href="./resources/user-forms.css" type="text/css" />
        <script src="./resources/jquery-1.8.2.js" type="text/javascript"></script>
        <script src="./resources/jquery.namespace.js" type="text/javascript"></script>
        <script src="./resources/handlebars-v2.0.0.js" type="text/javascript"></script>
        <script src="./resources/bootstrap.min.js" type="text/javascript"></script>
        <script src="./resources/bootstrap-tooltip.js" type="text/javascript"></script>
        <script src="./resources/FileSaver.js" type="text/javascript"></script>
        <script src="./resources/scn_editor.js" type="text/javascript"></script>
        <script src="./resources/jsonrpc.js" type="text/javascript" ></script>
        <script src="./resources/profileOperations.js" type="text/javascript"></script>
        <script src="./resources/cookieUserAPI.js" type="text/javascript" ></script>
        <script type="text/javascript">

            $(function startEditor() {
                Ostis.scneditor.WorkbenchActivator.start("WorkbenchContainer");
            });
            var locate = <%ServletContext context = session.getServletContext(); String realContextPath = context.getRealPath(request.getContextPath()); out.print("\"" + realContextPath.replace("\\", "\\\\") + "\"");%>;
            jsonrpc = new JSONRpcClient("JSON-RPC");
            jsonrpc.DatabaseFilename.setPath(locate);

        </script>
    </head>
    <body onload="initLoginPanel()">
    <jsp:useBean id="JSONRPCBridge" scope="session"
                 class="com.metaparadigm.jsonrpc.JSONRPCBridge" />

    <%
        DatabaseFilename databaseFilename = new DatabaseFilename();



        RegistrationObject registrationObject = new RegistrationObject();
        SettingsGetObject settingsGetObject = new SettingsGetObject();
        SettingSetterObject settingSetterObject = new SettingSetterObject();
        JSONRPCBridge.registerObject("SettingSetterObject", settingSetterObject);
        JSONRPCBridge.registerObject("RegistrationObject", registrationObject);
        JSONRPCBridge.registerObject("SettingsGetObject", settingsGetObject);
        JSONRPCBridge.registerObject("DatabaseFilename", databaseFilename);
    %>
    <form id="userTable">
        <table width="100%" border="0">
            <tr>
                <td align="left"><b><div id="userName"></div></b></td>
                <td align="right"><input class="btn" type="button" onclick="showSettingsInScsLevelOne()" value="Показать семантику настроек (SCs level 1)"/> <input class="btn" type="button" onclick="logOut()" value="Выйти"/></td>
            </tr>
        </table>
    </form>


    <form id="loginForm">


                <input onclick="clearInputByValue(this)"  value="логин" id="user" type="text" name="username"/> <input value="пароль" onclick="clearInputByValue(this)" id="pass" type="password" name="pass"/> <br> <input class="btn" type="button" onclick="login(this.form)" value="Войти"/>
                <input class="btn" type="button" onclick="registrateNewUser(this.form)" value="Зарегистрироваться"/>



    </form>
        <div id="main">
            <header>
                SCn-редактор
            </header>
            <div id="WorkbenchContainer"></div>
            <footer>
                Copyright ©&nbsp;2013-2014 <a href="http://www.ostis.net/">OSTIS</a>
            </footer>
        </div>
    </body>
</html>