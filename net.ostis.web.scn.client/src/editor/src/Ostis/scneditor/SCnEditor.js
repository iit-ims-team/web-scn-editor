addInitClass(function () {

    Ostis.scneditor.SCnEditor = {};

    Ostis.scneditor.SCnEditor.selectedNodes = [];

    Ostis.scneditor.SCnEditor.setSelectedSCnNode = function (scnNode, isMultiSelecting, isCtrlPressed, selectedChildNode) {

        if (isMultiSelecting === true) {
            var firstNode;
            if (Ostis.scneditor.SCnEditor.selectedNodes.length > 1) {
                firstNode = Ostis.scneditor.SCnEditor.firstSelectedNode;
            } else {
                firstNode = Ostis.scneditor.SCnEditor.selectedNodes[0];
            }
            Ostis.scneditor.SCnEditor.selectRange(firstNode, scnNode);
        } else if (isCtrlPressed) {
            if (Ostis.scneditor.SCnEditor.isSelected(scnNode) === true) {
                if (Ostis.scneditor.SCnEditor.selectedNodes.length !== 1) {
                    Ostis.scneditor.SCnEditor.unSelectNode(scnNode);
                } else {
                    return;
                }
            } else {
                Ostis.scneditor.SCnEditor.blurSelected();
                scnNode.setSelected();
                scnNode.setFocus();
                Ostis.scneditor.SCnEditor.selectedNodes.push(scnNode);
            }
        } else {
            if (Ostis.scneditor.SCnEditor.isAnySCnNodeSelected()) {
                /*if (Ostis.scneditor.SCnEditor.isSelected(scnNode) === true && Ostis.scneditor.SCnEditor.selectedNodes.length === 1) {
                 return;
                 }*/
                Ostis.scneditor.SCnEditor.deselectSelected(scnNode);
                Ostis.scneditor.SCnEditor.blurSelected();
            }
            Ostis.scneditor.SCnEditor.firstSelectedNode = scnNode;
            scnNode.setSelected(selectedChildNode);
            scnNode.setFocus();
            Ostis.scneditor.SCnEditor.selectedNodes = [];
            Ostis.scneditor.SCnEditor.selectedNodes.push(scnNode);
        }
        Ostis.scneditor.SCnArticle.configToolbar();
    };

    Ostis.scneditor.SCnEditor.unSelectNode = function (scnNode) {

        var selectedNodes = Ostis.scneditor.SCnEditor.selectedNodes;
        for (var nodeIndex = 0; nodeIndex < selectedNodes.length; ++nodeIndex) {
            if (selectedNodes[nodeIndex].getId() === scnNode.getId()) {
                selectedNodes.splice(nodeIndex, 1);
                scnNode.leaveSelecting(Ostis.scneditor.SCnEditor.selectedNodes[0]);
                return;
            }
        }
    };

    Ostis.scneditor.SCnEditor.blurSelected = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; ++i) {
            Ostis.scneditor.SCnEditor.selectedNodes[i].getJQueryNode().find("input").blur()
        }
    };

    Ostis.scneditor.SCnEditor.selectRange = function (firstNode, secondNode) {

        var firstIndex = Ostis.scneditor.SCnEditor.getSelectableNodeIndex(firstNode);
        var secondIndex = Ostis.scneditor.SCnEditor.getSelectableNodeIndex(secondNode);
        Ostis.scneditor.SCnEditor.firstSelectedNode = firstNode;
        if (firstIndex > secondIndex) {
            var temp = firstIndex;
            firstIndex = secondIndex;
            secondIndex = temp;
        }
        Ostis.scneditor.SCnEditor.deselectSelected();
        Ostis.scneditor.SCnEditor.selectedNodes = [];
        var selectableNodes = [];
        Ostis.scneditor.ArrayUtils.addAll(Ostis.scneditor.SCnArticle.getInstance().getSelectableSCnNodes(), selectableNodes);
        for (var i = firstIndex; i <= secondIndex; ++i) {
            selectableNodes[i].setSelected();
            Ostis.scneditor.SCnEditor.selectedNodes.push(selectableNodes[i]);
        }
    };

    Ostis.scneditor.SCnEditor.deselectSelected = function (newSelectedNode) {

        Ostis.scneditor.SCnEditor.selectedNodes.forEach(function (item) {
            item.leaveSelecting(newSelectedNode);
        });
    };

    Ostis.scneditor.SCnEditor.getSelectableNodeIndex = function (node) {

        var selectableNodes = [];
        Ostis.scneditor.ArrayUtils.addAll(Ostis.scneditor.SCnArticle.getInstance().getSelectableSCnNodes(), selectableNodes);
        for (var nodeIndex = 0; nodeIndex < selectableNodes.length; ++nodeIndex) {
            if (selectableNodes[nodeIndex].getId() === node.getId()) {
                return nodeIndex;
            }
        }
        return -1;
    };

    Ostis.scneditor.SCnEditor.isAnySCnNodeSelected = function () {

        return Ostis.scneditor.SCnEditor.selectedNodes.length !== 0;
    };

    Ostis.scneditor.SCnEditor.isSelected = function (scnNode) {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; ++i) {
            if (Ostis.scneditor.SCnEditor.selectedNodes[i].getId() === scnNode.getId()) {
                return true;
            }
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.isSelectedNodeArc = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            if (Ostis.scneditor.SCnEditor.selectedNodes[i].isArc() === false) {
                return false;
            }
        }
        return true;
    };

    Ostis.scneditor.SCnEditor.canSelectedNodeAddArc = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            if (Ostis.scneditor.SCnEditor.selectedNodes[i].canAddArc() !== true) {
                return false;
            }
        }
        return true;
    };

    Ostis.scneditor.SCnEditor.canSelectedNodeChangeNodeValue = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            if (Ostis.scneditor.SCnEditor.selectedNodes[i].canChangeNodeValue() !== true) {
                return false;
            }
        }
        return true;
    };

    Ostis.scneditor.SCnEditor.editSelectedNode = function () {


        function isEditing() {
            var selectedNodes = Ostis.scneditor.SCnEditor.selectedNodes;
            if (selectedNodes.length !== 1) {
                return false;
            } else if (selectedNodes[0].parentSCNode === undefined) {
                return true;
            } else if (selectedNodes[0].nodeValue !== undefined && !selectedNodes[0].nodeValue.scaddr) {
                return true;
            }
            return false;
        }

        if (isEditing()) {
            return Ostis.scneditor.SCnEditor.selectedNodes[0].edit();
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.rightClickToSelectedNode = function (keydownEvent) {

        Ostis.scneditor.SCnEditor.getLastSelectedNode().rightClick(keydownEvent);
    };

    Ostis.scneditor.SCnEditor.leftClickToSelectedNode = function (keydownEvent) {

        Ostis.scneditor.SCnEditor.getLastSelectedNode().leftClick(keydownEvent);
    };

    Ostis.scneditor.SCnEditor.tClickToSelectedNode = function () {

        if (Ostis.scneditor.SCnEditor.selectedNodes.length === 1) {
            Ostis.scneditor.SCnEditor.selectedNodes[0].tClick();
        }
    };

    Ostis.scneditor.SCnEditor.ctrlEnterClickToSelectedNode = function () {

        if (Ostis.scneditor.SCnEditor.selectedNodes.length === 1) {
            return Ostis.scneditor.SCnEditor.selectedNodes[0].ctrlEnterClick();
        }
        return true;
    };

    Ostis.scneditor.SCnEditor.rClickToSelectedNode = function () {

        if (Ostis.scneditor.SCnEditor.selectedNodes.length === 1) {
            Ostis.scneditor.SCnEditor.selectedNodes[0].rClick();
        }
    };

    Ostis.scneditor.SCnEditor.delClickToSelectedNode = function () {

        var elements = Ostis.scneditor.SCnEditor.getTopSelectedElementsForDelete();
        if (elements.length !== 0) {
            if (elements.length == 1) {
                var command = elements[0].getDelCommand();
                if (command) {
                    Ostis.scneditor.addCommand(command);
                } else {
                    Ostis.scneditor.addCommand(new Ostis.scneditor.RemoveSCNodesCommand(elements));
                }
            } else {
                Ostis.scneditor.addCommand(new Ostis.scneditor.RemoveSCNodesCommand(elements));
            }
        }
    };

    Ostis.scneditor.SCnEditor.getTopSelectedElementsForDelete = function () {

        var selectedNodes = Ostis.scneditor.SCnEditor.selectedNodes;
        var topNodes = [];
        for (var i = 0; i < selectedNodes.length; i++) {
            var isChild = false;
            var node = selectedNodes[i];
            for (var j = 0; j < selectedNodes.length; j++) {
                if (i !== j && selectedNodes[j].getDelCommand() !== null) {
                    if (Ostis.scneditor.SCnEditor.isSelectableChild(selectedNodes[j], node) === true) {
                        isChild = true;
                        break;
                    }
                }
                if (selectedNodes[j].scContourParents) {
                    selectedNodes[j].removeSCnKeyword();
                }
            }
            if (isChild === false) {
                topNodes.push(node);
            }
        }
        return topNodes;

    };

    Ostis.scneditor.SCnEditor.isSelectableChild = function (parent, child) {

        var selectableNodes = parent.getSelectableSCnNodes();
        for (var i = 0; i < selectableNodes.length; i++) {
            if (child.getId() === selectableNodes[i].getId()) {
                return true;
            }
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.addAttributeClickToSelectedNode = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            Ostis.scneditor.SCnEditor.selectedNodes[i].addAttributeClick();
            return false;
        }
    };

    Ostis.scneditor.SCnEditor.changeArcTypeClickToSelectedNode = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            Ostis.scneditor.SCnEditor.selectedNodes[i].changeArcTypeClick();
            return false;
        }
    };

    Ostis.scneditor.SCnEditor.changeSelectedNodeValueToFile = function (fileName) {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            Ostis.scneditor.SCnEditor.selectedNodes[i].changeNodeValueTo(new Ostis.scneditor.FileNodeValue(fileName));
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.changeSelectedNodeValueToImage = function (fileName) {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            Ostis.scneditor.SCnEditor.selectedNodes[i].changeNodeValueTo(new Ostis.scneditor.ImageFileNodeValue(fileName));
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.canAppendToSelectedNode = function (copiedNode) {

        if (Ostis.scneditor.SCnEditor.selectedNodes.length === 1) {
            return copiedNode.canAppendTo(Ostis.scneditor.SCnEditor.selectedNodes[0]);
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.pasteCopiedNode = function () {

        if (Ostis.scneditor.SCnEditor.selectedNodes.length === 1) {
            var copiedNode = Ostis.scneditor.SCnArticle.copiedNode;
            if (copiedNode !== undefined && Ostis.scneditor.SCnEditor.canAppendToSelectedNode(copiedNode) === true) {
                Ostis.scneditor.addCommand(
                    new Ostis.scneditor.AppendToCommand(
                        Ostis.scneditor.SCnEditor.selectedNodes[0],
                        copiedNode.clone(Ostis.scneditor.SCnEditor.selectedNodes[0])
                    ));
                return true;
            }
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.copySelectedNode = function () {

        if (Ostis.scneditor.SCnEditor.selectedNodes.length === 1) {
            Ostis.scneditor.SCnArticle.copiedNode = Ostis.scneditor.SCnEditor.selectedNodes[0].clone();
        }
    };

    Ostis.scneditor.SCnEditor.cutSelectedNode = function () {

        if (Ostis.scneditor.SCnEditor.selectedNodes.length === 1) {
            Ostis.scneditor.SCnArticle.copiedNode = Ostis.scneditor.SCnEditor.selectedNodes[0].clone();
            Ostis.scneditor.SCnEditor.delClickToSelectedNode();
        }
    };

    Ostis.scneditor.SCnEditor.changeSelectedNodeValueToContour = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            Ostis.scneditor.SCnEditor.selectedNodes[i].changeNodeValueTo(new Ostis.scneditor.SCnContourValue(
                Ostis.scneditor.SCnEditor.selectedNodes[i]));
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.changeSelectedNodeValueToSet = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            Ostis.scneditor.SCnEditor.selectedNodes[i].changeNodeValueTo(new Ostis.scneditor.SCnSetValue(Ostis.scneditor.SCnEditor.selectedNodes[i]));
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.changeLinkClickCallback = function () {

        function stop_modal() {
            tool.popover('destroy');
            tool.attr("disabled", false);
        }

        var isLink = Ostis.scneditor.SCnEditor.selectedNodes[0].nodeValue instanceof Ostis.scneditor.AbstractFileNodeValue;
        if (!isLink) {
            Ostis.scneditor.SCnEditor.selectedNodes[0].changeNodeValueTo(new Ostis.scneditor.FileNodeValue(null));
        }

        var obj = Ostis.scneditor.SCnEditor.selectedNodes[0].nodeValue;
        var tool = $(this);
        var container = '';
        tool.attr("disabled", true);
        $(this).popover({container: container});
        $(this).popover('show');
        var input = $(container + ' #set-content-input');
        var input_content = $(container + " input#content[type='file']");
        var input_content_type = $(container + " #set-content-type");
        input.val(obj.data);
        input_content_type.val(obj.type);
        setTimeout(function () {
            input.focus();
        }, 1);
        input.keypress(function (e) {
            if (e.keyCode == Ostis.scneditor.KeyCode.ENTER_CODE || e.keyCode == Ostis.scneditor.KeyCode.ESC_CODE) {
                if (e.keyCode == Ostis.scneditor.KeyCode.ENTER_CODE) {
                    if (obj.data != input.val() || obj.type != input_content_type.val()) {
                        Ostis.scneditor.addCommand(new Ostis.scneditor.ChangeLink(obj,
                            input.val(),
                            input_content_type.val())
                        );
                    }
                }
                stop_modal();
                e.preventDefault();
            }
        });
        $(container + ' #set-content-apply').click(function () {
            var file = input_content[0].files[0];
            if (file != undefined) {
                var fileReader = new FileReader();
                if (file.type === 'text/html') {
                    fileReader.onload = function () {
                        if (obj.data != input.val() || obj.type != 'html') {
                            Ostis.scneditor.addCommand(new Ostis.scneditor.ChangeLink(obj,
                                this.result,
                                'html')
                            );
                        }
                        stop_modal();
                    };
                    fileReader.readAsText(file);
                } else {
                    fileReader.onload = function () {
                        if (obj.data != this.result || obj.type != 'html') {
                            Ostis.scneditor.addCommand(new Ostis.scneditor.ChangeLink(obj,
                                '<img src="' + this.result + '" alt="Image">',
                                'html')
                            );
                        }
                        stop_modal();
                    };
                    fileReader.readAsDataURL(file);
                }
            } else {
                if (obj.data != input.val() || obj.type != input_content_type.val()) {
                    Ostis.scneditor.addCommand(new Ostis.scneditor.ChangeLink(obj,
                        input.val(),
                        input_content_type.val())
                    );
                }
                stop_modal();
            }
        });
        $(container + ' #set-content-cancel').click(function () {
            stop_modal();
        });
    };

    Ostis.scneditor.SCnEditor.changeSelectedNodeValueToInput = function () {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            Ostis.scneditor.SCnEditor.selectedNodes[i]
                .changeNodeValueTo(new Ostis.scneditor.SCnNodeValue(Ostis.scneditor.SCnEditor.selectedNodes[i]));
        }
        return false;
    };

    Ostis.scneditor.SCnEditor.addSCArcClickToSelectedNode = function (signText) {

        for (var i = 0; i < Ostis.scneditor.SCnEditor.selectedNodes.length; i++) {
            Ostis.scneditor.SCnEditor.selectedNodes[i].addSCArcClick(new Ostis.scneditor.BinaryRelation(Ostis.scneditor.Sign
                .getSignBySymbol(signText), Ostis.scneditor.SCnEditor.selectedNodes[i]));
        }
    };

    Ostis.scneditor.SCnEditor.changeAllocationToTop = function (keydownEvent) {

        var selectableNodes = [];
        Ostis.scneditor.ArrayUtils.addAll(Ostis.scneditor.SCnArticle.getInstance().getSelectableSCnNodes(), selectableNodes);
        for (var nodeIndex = 0; nodeIndex < selectableNodes.length; ++nodeIndex) {
            if (Ostis.scneditor.SCnEditor.isLastSelected(selectableNodes[nodeIndex]) === true) {
                if (nodeIndex !== 0) {
                    var isShiftKey = keydownEvent === undefined ? false : keydownEvent.shiftKey;
                    Ostis.scneditor.SCnEditor.setSelectedSCnNode(selectableNodes[nodeIndex - 1], isShiftKey);
                }
                return false;
            }
        }
    };

    Ostis.scneditor.SCnEditor.changeAllocationToBottom = function (keydownEvent) {

        var selectableNodes = [];
        Ostis.scneditor.ArrayUtils.addAll(Ostis.scneditor.SCnArticle.getInstance().getSelectableSCnNodes(), selectableNodes);
        for (var nodeIndex = selectableNodes.length - 1; nodeIndex >= 0; --nodeIndex) {
            if (Ostis.scneditor.SCnEditor.isLastSelected(selectableNodes[nodeIndex]) === true) {
                if (nodeIndex != selectableNodes.length - 1) {
                    var isShiftKey = keydownEvent === undefined ? false : keydownEvent.shiftKey;
                    Ostis.scneditor.SCnEditor.setSelectedSCnNode(selectableNodes[nodeIndex + 1], isShiftKey);
                }
                return false;
            }
        }
    };

    Ostis.scneditor.SCnEditor.isLastSelected = function (scnNode) {

        var lastSelectedNode = Ostis.scneditor.SCnEditor.getLastSelectedNode();
        return lastSelectedNode.getId() === scnNode.getId();
    };

    Ostis.scneditor.SCnEditor.getLastSelectedNode = function () {

        var lastSelectedNode;
        var firstSelectedNode = Ostis.scneditor.SCnEditor.firstSelectedNode;
        if (firstSelectedNode.getId() === Ostis.scneditor.SCnEditor.selectedNodes[0].getId()) {
            lastSelectedNode = Ostis.scneditor.SCnEditor.selectedNodes[Ostis.scneditor.SCnEditor.selectedNodes.length - 1];
        } else {
            lastSelectedNode = Ostis.scneditor.SCnEditor.selectedNodes[0];
        }
        return lastSelectedNode;
    };

    Ostis.scneditor.SCnEditor.getDao = function () {

        if (Ostis.scneditor.SCnEditor.dao === undefined) {
            Ostis.scneditor.SCnEditor.dao = new Ostis.scneditor.ServerDao();
        }
        return Ostis.scneditor.SCnEditor.dao;
    };

});
