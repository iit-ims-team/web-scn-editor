addInitClass(function () {

    $.extend(Ostis.scneditor.BinaryRelation.prototype, {
        createJQueryNode: function () {

            var $sentence = $("<div/>");
            $sentence.addClass("SCnSentence");
            var $SCnField = $("<div/>");
            $SCnField.addClass("SCnField");
            var $SCnFieldMarker = Ostis.scneditor.BinaryRelation.addInBeginSCnFieldMarker($SCnField, this);
            if (this.sign.forwardSign != "=") {
                Ostis.scneditor.BinaryRelation.bindEditSCnSignByDblClick(this, $SCnFieldMarker);
            }
            $sentence.append($SCnField);
            var $SCnFieldValue = $("<div/>");
            $SCnFieldValue.addClass("SCnFieldValue");
            $SCnFieldValue.append(this.scNode.getJQueryNode());
            $SCnField.append($SCnFieldValue);
            this.bindSelectNodeByClick($sentence);
            this.$node = $sentence;
        },

        addSCAttribute: function (scAttribute) {

            this.insertSCAttribute(scAttribute, this.scAttributes.length);
        },

        setFocus: function () {

            // Do nothing.
        },

        getDelCommand: function () {

            return new Ostis.scneditor.RemoveSCArcCommand(this, this.parentSCNode);
        },

        tClick: function () {

            if (this.getJQueryNode().find("> .SCnField > .SCnFieldMarker").length === 1) {
                this.getJQueryNode().find("> .SCnField > .SCnFieldMarker").dblclick();
            }
        },

        remove: function () {

            var hasChangeAllocationToBottom = false;
            if (this.hasSelecting() === true) {
                Ostis.scneditor.SCnEditor.setSelectedSCnNode(this);
                Ostis.scneditor.SCnEditor.changeAllocationToTop()
                hasChangeAllocationToBottom = true;
            }
            this.parentSCNode.removeSCArc(this);
            if (hasChangeAllocationToBottom === true) {
                Ostis.scneditor.SCnEditor.changeAllocationToBottom();
            }
        },

        removeSCNode: function () {

            this.remove();
        },

        hasSelecting: function () {

            var hasSelecting = false;
            if (Ostis.scneditor.SCnEditor.isSelected(this) === true) {
                hasSelecting = true;
            } else {
                if (this.scNode.hasSelecting() === true) {
                    hasSelecting = true;
                } else {
                    for (var scAttrIndex = 0; scAttrIndex < this.getSCAttributes().length; ++scAttrIndex) {
                        var scAttribute = this.getSCAttributes()[scAttrIndex];
                        if (scAttribute.hasSelecting() === true) {
                            hasSelecting = true;
                            break;
                        }
                    }
                }
            }
            return hasSelecting;
        },

        edit: function () {

            // Do nothing.
        },

        insertSCAttribute: function (scAttribute, indexAttribute) {

            if (indexAttribute != 0) {
                this.scAttributes[indexAttribute - 1].getJQueryNode().after(scAttribute.getJQueryNode());
            } else {
                if (this.getSCAttributes().length === 0) {
                    this.scNode.getJQueryNode().after(scAttribute.getJQueryNode()).detach();
                    var $SCnField = $("<div/>");
                    $SCnField.addClass("SCnField");
                    var $SCnFieldValue = $("<div/>");
                    $SCnFieldValue.addClass("SCnFieldValue");
                    $SCnFieldValue.append(this.scNode.getJQueryNode());
                    $SCnField.append($SCnFieldValue);
                    this.getJQueryNode().append($SCnField);
                } else {
                    this.scAttributes[0].getJQueryNode().before(scAttribute.getJQueryNode());
                }
            }
            this.scAttributes.splice(indexAttribute, 0, scAttribute);
        },

        getSCAttributes: function () {

            return this.scAttributes;
        },

        removeSCAttribute: function (removingAttribute) {

            for (var attributeIndex = 0; attributeIndex < this.getSCAttributes().length; ++attributeIndex) {
                var attribute = this.getSCAttributes()[attributeIndex];
                if (attribute.getId() == removingAttribute.getId()) {
                    this.getSCAttributes().splice(attributeIndex, 1);
                    if (this.getSCAttributes().length !== 0) {
                        attribute.getJQueryNode().detach();
                    } else {
                        this.scNode.getJQueryNode().parent().parent().detach();
                        attribute.getJQueryNode().after(this.scNode.getJQueryNode()).detach();
                    }
                }
            }
        },

        getSign: function () {

            return this.sign;
        },

        setSign: function (sign) {

            this.sign = sign;
            this.updateSignView();
        },

        addAttributeClick: function () {

            Ostis.scneditor.addCommand(new Ostis.scneditor.AddAttributeCommand(this));
        },

        changeArcTypeClick: function () {

            Ostis.scneditor.addCommand(new Ostis.scneditor.ChangeArcTypeCommand(this));
            return false;
        },

        changeArcType: function () {

            var constantSigns = Ostis.scneditor.Sign.getConstantSigns();
            var variableSigns = Ostis.scneditor.Sign.getVariablesSigns();
            var currentSignIndex = -1;
            var selected;
            for (var i = 0; i < constantSigns.length; i++) {
                selected = constantSigns[i].clone();
                if (selected.equals(this.getSign()) == true) {
                    currentSignIndex = i;
                    break;
                } else {
                    selected.changeDirection();
                    if (selected.equals(this.getSign()) == true) {
                        currentSignIndex = i;
                        break;
                    }
                }
            }
            if (currentSignIndex !== -1) {
                var changedSign = variableSigns[currentSignIndex].clone();
                changedSign.setForward(selected.isForward);
                this.setSign(changedSign);
            } else {
                for (var i = 0; i < variableSigns.length; i++) {
                    selected = variableSigns[i].clone();
                    if (selected.equals(this.getSign()) == true) {
                        currentSignIndex = i;
                        break;
                    } else {
                        selected.changeDirection();
                        if (selected.equals(this.getSign()) == true) {
                            currentSignIndex = i;
                            break;
                        }
                    }
                }
                if (currentSignIndex !== -1) {
                    var changedSign = constantSigns[currentSignIndex].clone();
                    changedSign.setForward(selected.isForward);
                    this.setSign(changedSign);
                }
            }
        },

        getSelectableSCnNodes: function () {

            var selectableSCnNodes = [];
            selectableSCnNodes.push(this);
            Ostis.scneditor.ArrayUtils.addAll(AbstractSCnElement.getSelectableNodesFromArray(this.getSCAttributes()), selectableSCnNodes);
            Ostis.scneditor.ArrayUtils.addAll(this.scNode.getSelectableSCnNodes(), selectableSCnNodes);
            return selectableSCnNodes;
        },

        setSCNode: function (scNode) {

            this.scNode.getJQueryNode().after(scNode.getJQueryNode());
            this.scNode.getJQueryNode().detach();
            this.scNode = scNode;
        },

        toJSON: function () {

            var jsonRepresentation = {};
            jsonRepresentation.type = this.getSign().getText();
            jsonRepresentation.SCAttributes = [];
            this.getSCAttributes().forEach(function (attr) {

                jsonRepresentation.SCAttributes.push(attr);
            });
            jsonRepresentation.SCNode = this.scNode.toJSON();
            jsonRepresentation.scaddr = this.scaddr;
            return jsonRepresentation;
        },

        setSelected: function () {

            Ostis.scneditor.BinaryRelation.superclass.setSelected.apply(this);
            this.getJQueryNode().find(">.SCnField>.SCnFieldMarker").addClass("selectedSCnNode");

        },

        leaveSelecting: function () {

            Ostis.scneditor.BinaryRelation.superclass.leaveSelecting.apply(this);
            this.getJQueryNode().find(">.SCnField>.SCnFieldMarker").removeClass("selectedSCnNode");
        },

        clone: function (parent) {

            var clone = new Ostis.scneditor.BinaryRelation(this.getSign().clone(), parent);
            clone.setSCNode(this.scNode.clone(clone));
            clone.createJQueryNode();
            this.getSCAttributes().forEach(function (attribute) {

                clone.addSCAttribute(attribute.clone(clone));
            });
            return clone;
        },

        appendTo: function (parent) {

            this.parentSCNode = parent;
            parent.addSCArc(this);
        },

        canAppendTo: function (scNode) {

            return scNode.canAddArc();
        },

        //wat?????

        isArc: function () {

            return true;
        },

        rClick: function () {

            Ostis.scneditor.addCommand(new Ostis.scneditor.ChangeDirectionCommand(this));
        },

        updateSignView: function () {

            this.getJQueryNode().find("> .SCnField > .SCnFieldMarker").text(this.sign.getText());
        },

        saveInMemory: function (firstArcCmp) {

            var arcProcessingDefResult = new jQuery.Deffered();
            this.scNode.saveInMemory.done(jQuery.proxy(function (secondArcCmp) {

                var arcType = this.getSign().getText();
                var memoryArcType = Ostis.scneditor.SCn2SCTypeTranslator.calcScArcType(arcType);
                var directArc = Ostis.scneditor.SCn2SCTypeTranslator.isArcDirect(arcType);
                var arcCreateDefResult = new jQuery.Deferred();
                if (directArc) {
                    window.sctpClient.create_arc(arcType, firstArcCmp.scaddr, secondArcCmp.scaddr).done(jQuery.proxy(function (scaddr) {

                        this.scaddr = scaddr;
                        console.log('arc=(' + firstArcCmp.label + ',' + secondArcCmp.label + '), scaddr=' + scaddr);
                        arcCreateDefResult.resolve(this);
                    }, this)).fail(function () {

                        console.log('Arc not saved begin=' + firstArcCmp.scaddr);
                    });
                } else {
                    window.sctpClient.create_arc(arcType, secondArcCmp.scaddr, firstArcCmp.scaddr).done(jQuery.proxy(function (scaddr) {

                        this.scaddr = scaddr;
                        console.log('arc=(' + secondArcCmp.label + ',' + firstArcCmp.label + '), scaddr=' + scaddr);
                        arcCreateDefResult.resolve(this);
                    }, this)).fail(function () {

                        console.log('Arc not saved begin=' + secondArcCmp.scaddr);
                    });
                }
            }, this)).fail(function () {

                arcProcessingDefResult.reject();
            });
        }
    });

    Ostis.scneditor.BinaryRelation.addInBeginSCnFieldMarker = function ($SCnField, scnNode) {

        var $SCnFieldMarker = Ostis.scneditor.BinaryRelation.createSCnFieldMarker(scnNode);
        $SCnField.prepend($SCnFieldMarker);
        return $SCnFieldMarker;
    };

    Ostis.scneditor.BinaryRelation.createSCnFieldMarker = function (scnNode) {

        var $SCnFieldMarker = $("<div/>");
        $SCnFieldMarker.attr("class", "SCnFieldMarker");
        $SCnFieldMarker.text(scnNode.sign.getText());
        return $SCnFieldMarker;
    };

    Ostis.scneditor.BinaryRelation.addInBeginSelectSign = function ($SCnField, scnNode) {

        $SCnField.selectSignDropdownComponent({
            scnNode: scnNode,
            signGroups: [Ostis.scneditor.Sign.getConstantSigns(),
                Ostis.scneditor.Sign.getVariablesSigns()
                //Ostis.scneditor.Sign.getOtherSigns()
            ],
            selectLeftCode: Ostis.scneditor.KeyCode.LEFT_CODE,
            selectRightCode: Ostis.scneditor.KeyCode.RIGHT_CODE,
            bindSetSignRelationCallback: Ostis.scneditor.BinaryRelation.bindSetSignRelation
        });
    };

    Ostis.scneditor.BinaryRelation.bindSetSignRelation = function (scnNode, $option) {

        $option.bind("click", function () {

            scnNode.getJQueryNode().find("> .SCnField > .dropdown").after(Ostis.scneditor.BinaryRelation.createSCnFieldMarker(scnNode)).detach();
            scnNode.getJQueryNode().find("> .SCnField > .SCnFieldMarker").addClass("selectedSCnNode");
            Ostis.scneditor.addCommand(new Ostis.scneditor.ChangeSignBinaryRelationCommand(scnNode, Ostis.scneditor.Sign.getSignBySymbol($option
                .text())));
        });
    };

    Ostis.scneditor.BinaryRelation.bindEditSCnSignByDblClick = function (scnNode, $SCnFieldMarker) {

        $SCnFieldMarker.bind("dblclick", function () {

            var $this = $(this);
            Ostis.scneditor.BinaryRelation.addInBeginSelectSign($this.parent(), scnNode);
            $this.detach();
        });
    };
});
