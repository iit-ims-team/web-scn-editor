Ostis.scneditor.handlebars.render = function (tmpl_name, tmpl_data) {
    if (tmpl_data !== undefined) {
        if (tmpl_data.text !== 'Конвертация из json') {
            return Ostis.scneditor.handlebars.compile(tmpl_name)(tmpl_data);
        }
    } else {
        return Ostis.scneditor.handlebars.compile(tmpl_name)(tmpl_data);
    }

};

Ostis.scneditor.handlebars.compile = function (tmpl_name) {

    var render = Ostis.scneditor.handlebars.render;
    if (!render.tmpl_cache) {
        render.tmpl_cache = {}
    }

    if (!render.tmpl_cache[tmpl_name]) {
        var tmpl_string;
        Ostis.scneditor.SCnEditor.getDao().getTemplate(tmpl_name, function (data) {

            tmpl_string = data;
        });
        render.tmpl_cache[tmpl_name] = Handlebars.compile(tmpl_string);
    }
    return render.tmpl_cache[tmpl_name];
};

Handlebars.registerHelper("printSignText", function (sign) {

    return sign.getText();
});
