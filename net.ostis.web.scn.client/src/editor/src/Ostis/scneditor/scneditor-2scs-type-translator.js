Ostis.scneditor.SCn2SCTypeTranslator = (function () {

    return {
        calcScNodeType: function (scnNode) {

            if (scnNode.contour) {
                return ( sc_type_node_struct | sc_type_const );
            }
            if (scnNode.set) {
                return ( sc_type_node_tuple | sc_type_const );
            }
            if (scnNode.label) {
                if (scnNode.label.endsWith("*")) {
                    return ( sc_type_node_norole | sc_type_const );
                }
                if (scnNode.label.endsWith("'")) {
                    return ( sc_type_node_role | sc_type_const );
                }
            }

            return ( sc_type_node | sc_type_const );
        },

        calcScArcType: function (scnArcType) {

            switch (scnArcType) {
                case '∍':
                    return sc_type_arc_pos_const_perm;
                case '∊':
                    return sc_type_arc_pos_const_perm;
                case '⇒':
                    return ( sc_type_arc_common | sc_type_const );
                case '⇐':
                    return ( sc_type_arc_common | sc_type_const );
                case '⇔':
                    return ( sc_type_edge_common | sc_type_const );
                case '∌':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_neg | sc_type_arc_perm );
                case '∉':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_neg | sc_type_arc_perm );
                case '/∍':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_fuz | sc_type_arc_perm );
                case '∊/':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_fuz | sc_type_arc_perm );
                case '~∍':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_pos | sc_type_arc_temp );
                case '∊~':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_pos | sc_type_arc_temp );
                case '~∌':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_neg | sc_type_arc_temp );
                case '∉~':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_neg | sc_type_arc_temp );
                case '~/∍':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_fuz | sc_type_arc_temp );
                case '∊/~':
                    return ( sc_type_arc_access | sc_type_const | sc_type_arc_fuz | sc_type_arc_temp );
                case '_∍':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_pos | sc_type_arc_perm );
                case '_∊':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_pos | sc_type_arc_perm );
                case '_⇒':
                    return ( sc_type_arc_common | sc_type_var );
                case '_⇐':
                    return ( sc_type_arc_common | sc_type_var );
                case '_⇔':
                    return ( sc_type_edge_common | sc_type_var );
                case '_∌':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_neg | sc_type_arc_perm );
                case '_∉':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_neg | sc_type_arc_perm );
                case '_/∍':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_fuz | sc_type_arc_perm );
                case '_∊/':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_fuz | sc_type_arc_perm );
                case '_~∍':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_pos | sc_type_arc_temp );
                case '_∊~':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_pos | sc_type_arc_temp );
                case '_~∌':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_neg | sc_type_arc_temp );
                case '_∉~':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_neg | sc_type_arc_temp );
                case '_~/∍':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_fuz | sc_type_arc_temp );
                case '_∊/~':
                    return ( sc_type_arc_access | sc_type_var | sc_type_arc_fuz | sc_type_arc_temp );
                default:
                    return sc_type_arc_pos_const_perm;
            }
        },

        isArcDirect: function (scnArcType) {

            switch (scnArcType) {
                case '∍':
                    return true;
                case '∊':
                    return false;
                case '⇒':
                    return true;
                case '⇐':
                    return false;
                case '∌':
                    return true;
                case '∉':
                    return false;
                case '/∍':
                    return true;
                case '∊/':
                    return false;
                case '~∍':
                    return true;
                case '∊~':
                    return false;
                case '~∌':
                    return true;
                case '∉~':
                    return false;
                case '~/∍':
                    return true;
                case '∊/~':
                    return false;
                case '_∍':
                    return true;
                case '_∊':
                    return false;
                case '_⇒':
                    return true;
                case '_⇐':
                    return false;
                case '_∌':
                    return true;
                case '_∉':
                    return false;
                case '_/∍':
                    return true;
                case '_∊/':
                    return false;
                case '_~∍':
                    return true;
                case '_∊~':
                    return false;
                case '_~∌':
                    return true;
                case '_∉~':
                    return false;
                case '_~/∍':
                    return true;
                case '_∊/~':
                    return false;
                default:
                    return true;
            }
        }
    };

})();