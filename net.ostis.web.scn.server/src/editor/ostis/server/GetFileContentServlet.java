package editor.ostis.server;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/getFileContent/")
public class GetFileContentServlet extends HttpServlet {

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("application/text");
        response.getOutputStream().write(ScnUtil.getFileContent(request.getParameter("fileName")));
    }
}
