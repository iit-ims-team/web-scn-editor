Ostis.scneditor.Workbench = function (wContainerId, plugin) {

    var wContainer = jQuery('#' + wContainerId);
    var layout = new Ostis.scneditor.WorkbenchLayout(wContainer);

    return {
        init: function () {

            layout.init(["SCnEditorAlias"]);
        },

        render: function () {

            layout.render();
        },

        destroy: function () {
            Ostis.scneditor.SCnArticle.unbindKeyboardEvents();
            wContainer.unbind('.toolbarButton');
            wContainer.unbind('.accordion-button');
            wContainer.unbind('.historyPanelToggleButton');
            wContainer.unbind('.toolbar-toggle-button');
            wContainer.unbind();
            wContainer.empty();
        }
    };
};
