function AbstractSCnElement(parentSCNode) {

    this.id = ++Ostis.scneditor.LAST_SCN_NODE_ID;
    this.parentSCNode = parentSCNode;
};

Ostis.scneditor.BinaryRelation = function (sign, parentSCNode, textValue, scAddr) {

    Ostis.scneditor.BinaryRelation.superclass.constructor.apply(this, [parentSCNode]);
    this.sign = sign;
    if (this.sign.forwardSign !== "=") {
        this.scNode = new Ostis.scneditor.SCnNode(this, textValue, scAddr);
    } else {
        this.scNode = new Ostis.scneditor.SCnContourValue(this);
    }
    this.scAttributes = [];
};

Ostis.scneditor.SCnArticle = function () {

    Ostis.scneditor.SCnArticle.superclass.constructor.apply(this);
    this.createJQueryNode();
};

Ostis.scneditor.SCnContourValue = function (parentSCNode) {

    Ostis.scneditor.SCnContourValue.superclass.constructor.apply(this, [parentSCNode]);
};

Ostis.scneditor.SCnNode = function (parentSCNode, textValue, scAddr) {

    Ostis.scneditor.SCnNode.superclass.constructor.apply(this, [parentSCNode]);
    this.scArcs = [];
    this.nodeValue = new Ostis.scneditor.SCnNodeValue(undefined, textValue, scAddr);
};

Ostis.scneditor.SCnSetValue = function (parentSCNode) {

    Ostis.scneditor.SCnSetValue.superclass.constructor.apply(this, [parentSCNode]);
    this.scArcs = [];
};

Ostis.scneditor.SCnArcOfSet = function (parentSCNode, textValue, scAddr) {

    Ostis.scneditor.SCnArcOfSet.superclass.constructor.apply(this, [new Ostis.scneditor.Sign("•", "•"), parentSCNode, textValue, scAddr]);
};

Ostis.scneditor.SCAttribute = function (parentSCNode) {

    Ostis.scneditor.SCAttribute.superclass.constructor.apply(this, [parentSCNode]);
    this.nodeValue = new Ostis.scneditor.SCnNodeValue(this);
};

Ostis.scneditor.SCnNodeValue = function (parentSCNode, settedText, scAddr) {

    this.parentSCNode = parentSCNode;
    this.settedText = settedText || Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER;
    if (scAddr)
        this.scaddr = scAddr;
    this.needUpdateInSC = false;
};

Ostis.scneditor.AbstractFileNodeValue = function (fileName) {

    this.fileName = fileName;
};

Ostis.scneditor.ImageFileNodeValue = function (fileName) {

    Ostis.scneditor.ImageFileNodeValue.superclass.constructor.apply(this, [fileName]);
    var $img = $("<img/>");
    $img.attr("src", "/getFileContent/?fileName=" + fileName);
    this.setImg($img);
};

Ostis.scneditor.FileNodeValue = function (fileName) {

    Ostis.scneditor.FileNodeValue.superclass.constructor.apply(this, [fileName]);
    var fileNodeValue = this;
    if (fileName) {
        Ostis.scneditor.SCnEditor.getDao().getFileContent(fileName, function (fileContent, format) {
            fileNodeValue.setContent(fileContent, format);
            fileNodeValue.needUpdateInSC = false;
        });
    } else {
        fileNodeValue.setContent('new link', 'string');
        fileNodeValue.setUpdateInSC();
    }
};

Ostis.scneditor.FileUtils = function () {

};

Ostis.scneditor.Sign = function (forwardSign, backwardSign, imgName) {

    this.forwardSign = forwardSign;
    this.backwardSign = backwardSign;
    this.isForward = true;
    this.imgName = imgName;
};

Ostis.scneditor.Dao = function () {

};

Ostis.scneditor.ServerDao = function () {

};

function extend(Child, Parent) {

    var F = function () {

    };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype;
}

extend(Ostis.scneditor.FileNodeValue, Ostis.scneditor.AbstractFileNodeValue);

extend(Ostis.scneditor.ImageFileNodeValue, Ostis.scneditor.AbstractFileNodeValue);

extend(Ostis.scneditor.SCnSetValue, AbstractSCnElement);

extend(Ostis.scneditor.SCnNode, AbstractSCnElement);

extend(Ostis.scneditor.SCAttribute, AbstractSCnElement);

extend(Ostis.scneditor.BinaryRelation, AbstractSCnElement);

extend(Ostis.scneditor.SCnContourValue, Ostis.scneditor.SCnNode);

extend(Ostis.scneditor.SCnArcOfSet, Ostis.scneditor.BinaryRelation);

extend(Ostis.scneditor.SCnArticle, Ostis.scneditor.SCnContourValue);

extend(Ostis.scneditor.ServerDao, Ostis.scneditor.Dao);

function addInitClass(initFunction) {

    if (Ostis.scneditor.initFunctions === undefined) {
        Ostis.scneditor.initFunctions = [];
    }
    Ostis.scneditor.initFunctions.push(initFunction);
}
