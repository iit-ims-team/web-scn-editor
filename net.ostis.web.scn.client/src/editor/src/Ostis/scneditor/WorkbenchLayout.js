Ostis.scneditor.WorkbenchLayout = function (workbenchContainer) {

    var partAliases;
    return {
        init: function (aPartAliases) {

            partAliases = {
                partAliases: aPartAliases
            };
        },

        render: function () {

            var layout = Ostis.scneditor.handlebars.render('workbench_layout', partAliases);
            workbenchContainer.append(layout);
        }
    };
};