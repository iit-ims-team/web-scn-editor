addInitClass(function () {

    Ostis.scneditor.ServerDao.prototype.getFileContent = function (fileName, callback) {

        $.ajax({
            url: 'api/link/content/',
            type: "GET",
            data: {
                "addr": fileName
            },
            success: function (data, textStatus, jqXHR) {

                var linkAddrs = [fileName];
                SCWeb.core.Server.getLinksFormat(linkAddrs,
                    function (formats) {

                        var format;
                        var fileFormats = SCWeb.core.ComponentManager._factories_fmt[formats[fileName]].formats;
                        if ($.inArray('format_png', fileFormats) > -1) {
                            format = 'image';
                            data = 'api/link/content/?addr=' + fileName;
                        } else if ($.inArray('format_html', fileFormats) > -1) {
                            format = 'html';
                        } else if ($.inArray('format_youtubelink', fileFormats) > -1) {
                            format = 'youtube_link';
                        } else if ($.inArray('format_googlemaplink', fileFormats) > -1) {
                            format = 'googlemaplink';
                        } else if ($.inArray('format_webm', fileFormats) > -1) {
                            data = 'api/link/content/?addr=' + fileName;
                            format = 'video';
                        } else if ($.inArray('format_pdf', fileFormats) > -1) {
                            data = fileName;
                            format = 'pdf';
                        }
                        callback(data, format);
                    });
            }
        });
    };

    Ostis.scneditor.ServerDao.prototype.getTemplate = function (tmpl_name, callback) {

        var tmpl_dir = '/static/components/js/scn-editor/handlebars_templates';
        var tmpl_url = tmpl_dir + '/' + tmpl_name + '.html';
        $.ajax({
            url: tmpl_url,
            method: 'GET',
            async: false,
            success: callback
        });
    };

    Ostis.scneditor.ServerDao.prototype.downloadArticle = function (articleName, callback) {

        $.ajax({
            url: '/download/',
            type: "GET",
            dataType: "json",
            data: {
                "articleLabel": articleName
            },
            success: callback
        });
    };

    Ostis.scneditor.ServerDao.prototype.saveArticle = function (jsonArticle) {

        Ostis.scneditor.SCnArticlePersister.saveArticle(jsonArticle);
    };

    Ostis.scneditor.ServerDao.prototype.downloadTemplates = function (callback) {

        $.ajax({
            url: '/download-templates/',
            type: "GET",
            dataType: "json",
            success: callback
        });
    };

    Ostis.scneditor.ServerDao.prototype.uploadFile = function (formData, callback) {

        $.ajax({
            url: "/upload/",
            type: "POST",
            processData: false,
            data: formData,
            contentType: false,
            success: callback
        });
    };

});

/**
 * Stores sc-addrs of children elements which can be included in future
 */
Ostis.scneditor.NodeChildrenAddrStore = function () {

    this.childrenStore = [];
};

Ostis.scneditor.NodeChildrenAddrStore.addChildAddr = function (parentAddr, childAddr) {

    var children = this.childrenStore[parentAddr];
    if (!children) {
        children = [];
        this.childrenStore[parentAddr] = children;
    }
    if (!children[childAddr]) {
        childAddr.push(childAddr);
    }
};

Ostis.scneditor.NodeChildrenAddrStore.getChildren = function (parent) {

    return this.childrenStore[parent];
};

Ostis.scneditor.NodeChildrenAddrStore.clear = function () {

    return this.childrenStore = [];
};

Ostis.scneditor.PersistanceErrorLogger = {
    errorNodeNotSaved: function () {

        console.log('BeginNode not saved');
    },

    errorSetNodeNotSaved: function () {

        console.log('BeginNode=set not created');
    },

    errorArcNotSaved: function () {

        console.log('Arc not created between nodes');
    },

    errorSetElemNotSaved: function () {

        console.log('Element of set is not saved');
    },

    errorContourElemNotSaved: function () {

        console.log('Element of contour not save')
    },

};

Ostis.scneditor.SCnArticlePersister = (function () {

    var allNodesInArticle = [];
    var nodeScAddrMap = {};

    var onSetSave = function (nodeSaveDfd, structAddr) {

        nodeSaveDfd.done(function (beginNode) {

            if (structAddr) {
                addContourElement(structAddr, beginNode.scaddr);
            }
            console.log('set scaddr=' + beginNode.scaddr);
            beginNode.SCNodes.forEach(function (setNode) {
                Ostis.scneditor.ScStoreUpdater.updateNode(setNode).done(function (setNode) {

                    console.log('setNode: label=' + setNode.label + ', scaddr=' + setNode.scaddr);
                    window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_3F_A_F,
                        [
                            beginNode.scaddr,
                            sc_type_arc_pos_const_perm,
                            setNode.scaddr
                        ]).done(function (results) {

                        if (structAddr) {
                            addContourElement(structAddr, results[0][2]);
                        }
                        saveNode(setNode, structAddr);
                    }).fail(function () {

                        window.sctpClient.create_arc(sc_type_arc_pos_const_perm, beginNode.scaddr, setNode.scaddr).done(function (scaddr) {

                            if (structAddr) {
                                addContourElement(structAddr, scaddr);
                            }
                            console.log('arc=(' + beginNode.label + ',' + setNode.label + '), scaddr=' + scaddr);
                            saveNode(setNode, structAddr);
                        }).fail(Ostis.scneditor.PersistanceErrorLogger.errorArcNotSaved);
                    });
                }).fail(Ostis.scneditor.PersistanceErrorLogger.errorSetElemNotSaved);
            });
        }).fail(Ostis.scneditor.PersistanceErrorLogger.errorSetNodeNotSaved);
    };

    var addContourElement = function (structAddr, elemAddr) {

        window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_3F_A_F,
            [
                structAddr,
                sc_type_arc_pos_const_perm,
                elemAddr
            ]).fail(function () {
            console.log("addContourElement " + structAddr + " -> " + elemAddr);
            window.sctpClient.create_arc(sc_type_arc_pos_const_perm, structAddr, elemAddr)
                .fail(Ostis.scneditor.PersistanceErrorLogger.errorArcNotSaved);
        });
    };

    var onContourSave = function (nodeSaveDfd, structAddr) {

        nodeSaveDfd.done(function (beginNode) {

            if (structAddr) {
                addContourElement(structAddr, beginNode.scaddr);
            }
            beginNode.SCNodes.forEach(function (contourNode) {
                saveNode(contourNode, beginNode.scaddr);
            });
        }).fail(Ostis.scneditor.PersistanceErrorLogger.errorContourElemNotSaved);
    };

    var onNodeSave = function (nodeSaveDfd, structAddr) {

        nodeSaveDfd.done(function (beginNode) {

            if (structAddr) {
                addContourElement(structAddr, beginNode.scaddr);
            }
            console.log('save node label=' + beginNode.label + ', scaddr=' + beginNode.scaddr);

            if (beginNode.SCSynonims != undefined)
                beginNode.SCSynonims.forEach(function (arc) {
                    var endNode = arc.SCNode;
                    console.log('find  SCSynonims');
                    endNode.scaddr = beginNode.scaddr;
                    Ostis.scneditor.ScStoreUpdater.updateNode(endNode).done(function (endNode) {

                        console.log(beginNode.label + ' (SCSynonims) ' + endNode.label + ', scaddr=' + endNode.scaddr);
                        saveNode(endNode, structAddr);
                    }).fail(Ostis.scneditor.PersistanceErrorLogger.errorNodeNotSaved);
                });

            if (beginNode.SCArcs != undefined)
                beginNode.SCArcs.forEach(function (arc) {
                    var endNode = arc.SCNode;
                    console.log('iterated node=' + endNode.label);
                    Ostis.scneditor.ScStoreUpdater.updateNode(endNode).done(function (endNode) {

                        console.log(beginNode.label + ' (SCArcs) ' + endNode.label + ', scaddr=' + endNode.scaddr);
                        var arcType = Ostis.scneditor.SCn2SCTypeTranslator.calcScArcType(arc.type);

                        if (Ostis.scneditor.SCn2SCTypeTranslator.isArcDirect(arc.type)) {

                            window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_3F_A_F,
                                [
                                    beginNode.scaddr,
                                    arcType,
                                    endNode.scaddr
                                ]).done(function (results) {

                                addAttributes(results[0][1], arc.SCAttributes, structAddr);
                                saveNode(endNode, structAddr);
                            }).fail(function () {

                                window.sctpClient.create_arc(arcType, beginNode.scaddr, endNode.scaddr).done(function (scaddr) {

                                    if (structAddr) {
                                        addContourElement(structAddr, scaddr);
                                    }
                                    arc.scaddr = scaddr;
                                    console.log('arc=(' + beginNode.label + ',' + endNode.label + '), scaddr=' + scaddr);

                                    saveNode(endNode, structAddr);
                                    addAttributes(scaddr, arc.SCAttributes, structAddr);
                                }).fail(Ostis.scneditor.PersistanceErrorLogger.errorArcNotSaved);
                            }).done(function () {
                                console.log("arc already exist");
                            });
                        } else {

                            window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_3F_A_F,
                                [
                                    endNode.scaddr,
                                    arcType,
                                    beginNode.scaddr
                                ]).done(function (results) {

                                addAttributes(results[0][1], arc.SCAttributes, structAddr);
                                saveNode(endNode, structAddr);
                            }).fail(function () {

                                window.sctpClient.create_arc(arcType, endNode.scaddr, beginNode.scaddr).done(function (scaddr) {

                                    if (structAddr) {
                                        addContourElement(structAddr, scaddr);
                                    }
                                    arc.scaddr = scaddr;
                                    console.log('reverseArc=(' + endNode.label + ',' + beginNode.label + '), scaddr=' + scaddr);

                                    saveNode(endNode, structAddr);
                                    addAttributes(scaddr, arc.SCAttributes, structAddr);
                                }).fail(Ostis.scneditor.PersistanceErrorLogger.errorArcNotSaved);
                            }).done(function () {
                                console.log("arc already exist");
                            });
                        }
                    }).fail(Ostis.scneditor.PersistanceErrorLogger.errorNodeNotSaved);
                });
        }).fail(Ostis.scneditor.PersistanceErrorLogger.errorNodeNotSaved);
    };

    var addAttributes = function (arcAddr, attributes, structAddr) {

        attributes.forEach(function (attr) {
            // TODO: needed  to  change JSON representation of article
            var node = {
                scaddr: attr.scaddr || attr.nodeValue.scaddr,
                label: attr.nodeValue.settedText,
                obj: attr.nodeValue
            };
            if (structAddr) {
                addContourElement(structAddr, node.scaddr);
            }
            Ostis.scneditor.ScStoreUpdater.updateNode(node).done(function (node) {

                window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_3F_A_F,
                    [
                        node.scaddr,
                        sc_type_arc_pos_const_perm,
                        arcAddr
                    ]).fail(function () {

                    window.sctpClient.create_arc(sc_type_arc_pos_const_perm, node.scaddr, arcAddr).done(function (scaddr) {

                        if (structAddr) {
                            addContourElement(structAddr, scaddr);
                        }
                        console.log("attribute added attr.label=" + node.label + " arcAddr=" + arcAddr);
                    }).fail(function () {

                        console.log("fail to add attribute " + node.label);
                    });
                }).done(function () {
                    console.log("arc already exist");
                });
            });
        });
    };

    var saveNode = function (node, structAddr) {

        var updateNodeDfd = Ostis.scneditor.ScStoreUpdater.updateNode(node);
        if (node.contour) {
            onContourSave(updateNodeDfd, structAddr);
        } else if (node.set) {
            onSetSave(updateNodeDfd, structAddr);
        }
        onNodeSave(updateNodeDfd, structAddr);
    };

    var translateAllNodesBeforeArticle = function (jsonArticle) {

        allNodesInArticle = [];
        nodeScAddrMap = {};
        getAllNodes(jsonArticle);
        var length = allNodesInArticle.length;
        console.log("Get " + length + " nodes");
        //console.log(allNodesInArticle);
        allNodesInArticle.forEach(function (node) {
            if (node.scaddr && node.label !== Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER) {
                nodeScAddrMap[node.label] = node.scaddr;
            }
        });
        return new Promise(function (resolve, reject) {
            translateAllSync(allNodesInArticle, 0, length, resolve, reject);
        });
    };

    var getAllNodes = function (node) {

        if (node.SCNodes != undefined) {
            node.SCNodes.forEach(function (scNode) {
                pushInAllArticleNodes(scNode);
                getAllNodes(scNode);
            });
        }
        if (node.SCSynonims != undefined) {
            node.SCSynonims.forEach(function (arc) {
                var endNode = arc.SCNode;
                pushInAllArticleNodes(endNode);
                getAllNodes(endNode);
            });
        }
        if (node.SCArcs != undefined) {
            node.SCArcs.forEach(function (arc) {
                var endNode = arc.SCNode;
                pushInAllArticleNodes(endNode);
                getAllNodes(endNode);
                if (arc.SCAttributes != undefined) {
                    arc.SCAttributes.forEach(function (attr) {
                        var node = {
                            scaddr: attr.scaddr || attr.nodeValue.scaddr,
                            label: attr.nodeValue.settedText,
                            obj: attr.nodeValue
                        };
                        pushInAllArticleNodes(node);
                    });
                }
            });
        }
    };

    var pushInAllArticleNodes = function (node) {

        if (!(node.contour || node.set || node.file)) {
            allNodesInArticle.push(node);
        }
    };

    var translateAllSync = function (allNodesInArticle, now, max, resolve, reject) {

        var node = allNodesInArticle[now];

        var translateNext = function () {
            nodeScAddrMap[node.label] = node.scaddr;
            var next = now + 1;
            if (next != max) {
                translateAllSync(allNodesInArticle, next, max, resolve, reject);
            } else {
                resolve();
            }
        };

        var translateNodeInMemory = function () {
            Ostis.scneditor.ScStoreUpdater.updateNode(node).done(function (node) {
                console.log("Translate node[" + now + "] with label " + node.label);
                translateNext(node);
            }).fail(function () {
                console.log("Fail when translate node[" + now + "] with label " + node.label);
                reject(node);
            });
        };

        if (node.scaddr) {
            console.log("Node[" + now + "] with label " + node.label + " already have scaddr");
            translateNext();
        } else {
            if (node.label == Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER) {
                translateNodeInMemory();
            } else if (nodeScAddrMap[node.label] != null) {
                console.log("Node[" + now + "] with label " + node.label + " find in map");
                var scaddr = nodeScAddrMap[node.label];
                node.scaddr = scaddr;
                node.obj.scaddr = scaddr;
                translateNext(node);
            } else {
                SCWeb.core.Server.findIdentifiersSubStr(node.label, function (data) {
                    var findIdtf = false;
                    typeLoop: for (key in data) { // sys , main, common
                        var list = data[key];
                        for (idx in list) { // idx : 0 - addr ; 1 - idtf
                            var addr = list[idx][0];
                            var idtf = list[idx][1];
                            if (node.label == idtf && addr != null) {
                                findIdtf = true;
                                node.scaddr = addr;
                                node.obj.scaddr = addr;
                                break typeLoop;
                            }
                        }
                    }
                    if (findIdtf) {
                        console.log("Find idtf for node[" + now + "] with label " + node.label + " in base");
                        translateNext(node);
                    } else {
                        translateNodeInMemory();
                    }
                });
            }
        }
    };

    return {
        saveArticle: function (jsonArticle) {

            // TODO: needed  to  change JSON representation of article
            if (jsonArticle.scaddr) {
                jsonArticle.SCNodes[0].scaddr = jsonArticle.scaddr;
            } else {
                jsonArticle.scaddr = jsonArticle.SCNodes[0].scaddr;
            }

            console.log("JSON Article");
            console.log(jsonArticle);

            translateAllNodesBeforeArticle(jsonArticle).then(function () {
                console.log("Start translate article");
                //console.log(nodeScAddrMap);
                Ostis.scneditor.NodeChildrenAddrStore.clear();
                jsonArticle.SCNodes.forEach(function (articleNode) {
                    articleNode.isRootArticleNode = true;
                    saveNode(articleNode);
                });
            });
        }
    };
})();
