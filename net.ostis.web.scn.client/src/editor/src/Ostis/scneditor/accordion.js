(function ($) {

    $.fn.accordion = function () {
        var $thisOld;

    };

    // Options: signGroups for each accordion section, also you can add
    // accordionClass for additional styling
    // For cute example of accordion usage see toolbar.js
    Ostis.scneditor.Accordion = function (options) {

        this.options = options;
        this.templateName = 'accordion_buttons';
        this.$html = this.generateHtml();
        this.$addArcButtons = this.$html.find('.accordion-button');
        this.activate();
    };

    Ostis.scneditor.Accordion.prototype.generateHtml = function () {

        return $(Ostis.scneditor.handlebars.render(this.templateName, this.options));
    };

    Ostis.scneditor.Accordion.prototype.activate = function () {

        var $html = this.$html;
        var addArcClickCallback = this.options.addArcClickCallback;
        $html.accordion();
        this.$addArcButtons.each(function () {

            $button = $(this);
            var title = $button.data('sign-title');
            var imgName = $button.data('img');
            $button.click(function () {

                addArcClickCallback(title)
            });
            $button.tooltip({
                html: true,
                title: function () {

                    var $img = $("<img/>");
                    $img.attr("src", "/static/components/js/scn-editor/editor_images/" + imgName);
                    return $img;
                },
                placement: "right"
            });
        })
    };

})(jQuery);
