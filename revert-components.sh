sc_web_path=$1

append_line()
{
    if grep -Fxq "$3" $1
    then
        # code if found
        echo -en "Link to " $blue"$2"$rst "already exists in " $blue"$1"$rst "\n"
    else
        # code if not found
        echo -en "Append '" $green"$2"$rst "' -> " $green"$1"$rst "\n"
        echo $3 >> $1
    fi
}

append_js()
{
    append_line $1 $2 "<script type=\"text/javascript\" charset=\"utf-8\" src=\"/static/$2\"></script>"
}

append_css()
{
    append_line $1 $2 "<link rel=\"stylesheet\" type=\"text/css\" href=\"/static/$2\" />"
}

echo "Begin registration of scn-editor in the sc-web module.\n ..."

append_css $sc_web_path components/js/scn-editor/scn_editor.css
append_js $sc_web_path components/js/scn-editor/handlebars-v2.0.0.js
append_js $sc_web_path components/js/scn-editor/scn_editor.js

echo "Finish scn-editor registration in the sc-web module."
