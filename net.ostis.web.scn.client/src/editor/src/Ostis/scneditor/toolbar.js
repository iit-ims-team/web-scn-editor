(function ($) {

    $.fn.toolbar = function (opt) {

        var options = $.extend({
            addArcClickCallback: function () {

            },
            addAttributeClickCallback: function () {

            },
            changeArcTypeClickCallback: function () {

            },
            toContourClickCallback: function () {

            },
            toSetClickCallback: function () {

            },
            toInputClickCallback: function () {

            },
            changeLinkClickCallback: function () {

            },
            downloadClickCallback: function () {

            },
            saveClickCallback: function () {

            },
            uploadFileCallback: function () {

            },
            selectTemplateCallback: function () {

            },
            toggleSettingsNode: function () {

            },
            toJsonClickCallback: function () {

            },
            toScsClickCallback: function () {

            },
            signGroups: []
        }, opt);
        return this.each(function () {

            if (options.method === "configDisabling") {
                if (options.isSelectedNodeArc === true) {
                    for (var i = 0; i < this.buttonsForArc.length; i++) {
                        this.buttonsForArc[i].disabled = false;
                    }
                } else {
                    for (var i = 0; i < this.buttonsForArc.length; i++) {
                        this.buttonsForArc[i].blur();
                        this.buttonsForArc[i].disabled = true;
                    }
                }
                if (options.canSelectedNodeAddArc === true) {
                    this.accordion.$addArcButtons.disabled = false;
                } else {
                    this.accordion.$addArcButtons.blur();
                    this.accordion.$addArcButtons.disabled = true;
                }
                if (options.canSelectedNodeChangeNodeValue === true) {
                    for (var i = 0; i < this.changeNodeValueButtons.length; ++i) {
                        this.changeNodeValueButtons[i].disabled = false;
                    }
                } else {
                    for (var i = 0; i < this.changeNodeValueButtons.length; ++i) {
                        this.changeNodeValueButtons[i].blur();
                        this.changeNodeValueButtons[i].disabled = true;
                    }
                }
            } else {
                var $toolbarContainer = $(this);
                for (var count = 0; count < options.signGroups.length - 1; count++) {
                    var signGroups = options.signGroups[count].signs;
                    var fowardedSingGroups = [];
                    signGroups.forEach(function (item) {
                        var fowardedSign = item.clone();
                        fowardedSign.changeDirection();
                        fowardedSingGroups.push(fowardedSign);
                    });
                    for (var b = 0; b < fowardedSingGroups.length; b++) {
                        options.signGroups[count].signs.push(fowardedSingGroups[b]);
                    }

                }
                this.accordion = new Ostis.scneditor.Accordion({
                    signGroups: options.signGroups,
                    addArcClickCallback: options.addArcClickCallback,
                    accordionClass: 'col-3'
                });
                var $toolbar = $(Ostis.scneditor.handlebars.render('toolbar'));
                $toolbarContainer.append($toolbar);
                insertAccordion.call(this, $toolbar)
                fillTemplatesComponent($toolbar);
                initToolbarArrays.call(this, $toolbar);
                initToolbarButtons.call(this, $toolbar)
            }
        });

        function fillTemplatesComponent($toolbar) {

            var $select = $toolbar.find('.template-select');
            var articlesJson = [];
            articlesJson[0] = JSON.parse('{"SCNodes":[{"SCArcs":[{"SCAttributes":[{"label":"основной sc-идентификатор*"}],"SCNode":{"SCArcs":[],"set":true,"SCNodes":[{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Русский язык"},"type":"∊"}],"SCSynonims":[],"label":"бинарное отношение"},{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Английский язык"},"type":"∍"}],"SCSynonims":[],"label":"binary relation"}],"SCSynonims":[]},"type":"⇒"},{"SCAttributes":[{"label":"строгое включение*"}],"SCNode":{"SCArcs":[],"set":true,"SCNodes":[{"SCArcs":[],"SCSynonims":[],"label":"симметричное отношение"},{"SCArcs":[],"SCSynonims":[],"label":"квазибинарное отношение"},{"SCArcs":[],"SCSynonims":[],"label":"неориентированное отношение"},{"SCArcs":[],"SCNodes":[{"SCArcs":[{"SCAttributes":[{"label":"определение*"}],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"множества, пересечение которых равно пустому множеству, называются непересекающимеся"},"type":"⇒"}],"SCSynonims":[],"label":"непересекающиеся множества*"}],"SCSynonims":[],"label":"непересекающиеся множества*","contour":true}],"SCSynonims":[]},"type":"⇒"}],"SCSynonims":[],"label":"бинарное отношение"}],"label":"бинарное отношение","contour":true}');
            articlesJson[0].label = 'бинарное отношение';
            articlesJson[1] = JSON.parse('{"SCNodes":[{"SCArcs":[{"SCAttributes":[{"label":"основной sc-идентификатор*"}],"SCNode":{"SCArcs":[],"set":true,"SCNodes":[{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Русский язык"},"type":"∊"}],"SCSynonims":[],"label":"Проект OSTIS"},{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Английский язык"},"type":"∍"}],"SCSynonims":[],"label":"OSTIS project"}],"SCSynonims":[]},"type":"⇒"},{"SCAttributes":[{"label":"основной подпроект*"}],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Проект IMS.OSTIS"},"type":"⇒"},{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"проект"},"type":"∊"}],"SCSynonims":[],"label":"Проект OSTIS"}],"label":"Проект OSTIS","contour":true}');
            articlesJson[1].label = 'Проект';
            articlesJson[2] = JSON.parse('{"SCNodes":[{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"множество"},"type":"∊"},{"SCAttributes":[{"label":"включение*"}],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"объект"},"type":"⇐"},{"SCAttributes":[],"SCNode":{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"афроамериканец"},"type":"∊"}],"SCSynonims":[],"label":"Барак Обама"},"type":"∍"},{"SCAttributes":[{"label":"включение*"}],"SCNode":{"SCArcs":[{"SCAttributes":[{"label":"включение*"}],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"француз"},"type":"⇒"}],"SCSynonims":[],"label":"европеец"},"type":"⇒"}],"SCSynonims":[],"label":"человек"}],"label":"человек","contour":true}');
            articlesJson[2].label = 'человек';
            for (var i = 0; i < articlesJson.length; ++i) {
                $select.append(createTemplateOption(articlesJson[i]));
            }
            $select.on("change", function () {

                options.selectTemplateCallback(articlesJson[$select.find("option:selected").index() - 1]);
            });

//	    Ostis.scneditor.SCnEditor
//		    .getDao()
//		    .downloadTemplates(
//			    function(articlesJson) {
//
//				articlesJson = [];
//				articlesJson[0] = '{"SCNodes":[{"SCArcs":[{"SCAttributes":[{"label":"основной sc-идентификатор*"}],"SCNode":{"SCArcs":[],"set":true,"SCNodes":[{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Русский язык"},"type":"∊"}],"SCSynonims":[],"label":"бинарное отношение"},{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Английский язык"},"type":"∍"}],"SCSynonims":[],"label":"binary relation"}],"SCSynonims":[]},"type":"⇒"},{"SCAttributes":[{"label":"строгое включение*"}],"SCNode":{"SCArcs":[],"set":true,"SCNodes":[{"SCArcs":[],"SCSynonims":[],"label":"симметричное отношение"},{"SCArcs":[],"SCSynonims":[],"label":"квазибинарное отношение"},{"SCArcs":[],"SCSynonims":[],"label":"неориентированное отношение"},{"SCArcs":[],"SCNodes":[{"SCArcs":[{"SCAttributes":[{"label":"определение*"}],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"множества, пересечение которых равно пустому множеству, называются непересекающимеся"},"type":"⇒"}],"SCSynonims":[],"label":"непересекающиеся множества*"}],"SCSynonims":[],"label":"непересекающиеся множества*","contour":true}],"SCSynonims":[]},"type":"⇒"}],"SCSynonims":[],"label":"бинарное отношение"}],"label":"бинарное отношение","contour":true}';
//				articlesJson[1] = '{"SCNodes":[{"SCArcs":[{"SCAttributes":[{"label":"основной sc-идентификатор*"}],"SCNode":{"SCArcs":[],"set":true,"SCNodes":[{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Русский язык"},"type":"∊"}],"SCSynonims":[],"label":"Проект OSTIS"},{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Английский язык"},"type":"∍"}],"SCSynonims":[],"label":"OSTIS project"}],"SCSynonims":[]},"type":"⇒"},{"SCAttributes":[{"label":"основной подпроект*"}],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"Проект IMS.OSTIS"},"type":"⇒"},{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"проект"},"type":"∊"}],"SCSynonims":[],"label":"Проект OSTIS"}],"label":"Проект OSTIS","contour":true}';
//				articlesJson[2] = '{"SCNodes":[{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"множество"},"type":"∊"},{"SCAttributes":[{"label":"включение*"}],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"объект"},"type":"⇐"},{"SCAttributes":[],"SCNode":{"SCArcs":[{"SCAttributes":[],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"афроамериканец"},"type":"∊"}],"SCSynonims":[],"label":"Барак Обама"},"type":"∍"},{"SCAttributes":[{"label":"включение*"}],"SCNode":{"SCArcs":[{"SCAttributes":[{"label":"включение*"}],"SCNode":{"SCArcs":[],"SCSynonims":[],"label":"француз"},"type":"⇒"}],"SCSynonims":[],"label":"европеец"},"type":"⇒"}],"SCSynonims":[],"label":"человек"}],"label":"человек","contour":true}';
//
//				for (var i = 0; i < articlesJson.length; ++i) {
//				    $select.append(createTemplateOption(articlesJson[i]));
//				}
//				$select.on("change", function() {
//
//				    options.selectTemplateCallback(articlesJson[$select.find(
//					    "option:selected").index() - 1]);
//				});
//			    });
        }
        ;

        function initToolbarButtons($toolbar) { // Yes, very ugly

            $toolbar.find('#addAttributeButton').click(options.addAttributeClickCallback);
            $toolbar.find('#changeArcTypeButton').click(options.changeArcTypeClickCallback);
            $toolbar.find('#changeNodeValueToSetButton').click(options.toSetClickCallback);
            $toolbar.find('#changeNodeValueToContourButton').click(options.toContourClickCallback);
            $toolbar.find('#changeNodeValueToTextButton').click(options.toInputClickCallback);
            $toolbar.find('#changeLinkValueButton').click(options.changeLinkClickCallback);
            $toolbar.find('#toolbarSaveButton').click(options.saveClickCallback);
            $toolbar.find('#exportToJsonButton').click(options.toJsonClickCallback);
            $toolbar.find('#exportToScsButton').click(options.toScsClickCallback);
            $toolbar.find('#downloadArticle').click(options.downloadClickCallback);
            $toolbar.find('#settingsButton').click(options.toggleSettingsNode);
            $toolbar.find('#fileInput').uploadComponent({
                callback: options.uploadFileCallback
            });
            $('.bootlbG').click(toggleToolbarCallback($toolbar));
            jQuery($("#toolbarToggleButton")).hide();
            $('.historyPanelContainer').toggleClass('isHidden');
        }

        function toggleToolbarCallback($toolbar) {

            return function () {

                $('.content-center-container').toggleClass('no-toolbar');
                $('.WToolbarContainer').toggleClass('isHidden');
            }
        }

        function insertAccordion($toolbar) {

            var $accordionContainer = $toolbar.find('#accordionContainer');
            //var $accordionCont = this.accordion.$html;
            $accordionContainer.append(this.accordion.$html);
            $(".accUl").wrap("<div class='accordion'></div>");
        }

        function initToolbarArrays($toolbar) {

            this.buttonsForArc = $toolbar.find('.button-for-arc');
            this.changeNodeValueButtons = $toolbar.find('.change-node-value-button');
        }

        function createTemplateOption(articleJson) {

            return Ostis.scneditor.handlebars.render('toolbar_template_option', articleJson.label);
        }
    };
})(jQuery);