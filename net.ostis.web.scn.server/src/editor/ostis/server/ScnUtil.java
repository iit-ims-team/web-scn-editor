package editor.ostis.server;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.ServletContext;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ScnUtil {

    private static final String ARTICLES_PATH = "./scn_editor/articles/";

    private static final String FILE_EXTENSION = ".json";

    private static final String PATH_TO_SAVED_FILES = "./saved_files/";

    private static final String TEMPLATES_FOLDER = "/templates";

    public static String readTemplateArticles(ServletContext servletContext) throws IOException {
        String templatesFolderPath = null;
        try {
            templatesFolderPath = servletContext.getResource(TEMPLATES_FOLDER).getPath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        File templatesFolder = new File(templatesFolderPath);
        String[] fileNames = templatesFolder.list();
        JSONArray articles = new JSONArray();
        try {
            for (String fileName : fileNames) {
                articles.add(new JSONParser().parse(readFile(templatesFolder + "/" + fileName)));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return articles.toJSONString();
    }

    public static String readArticle(String articleName) throws IOException {
        String fileName = articleName + FILE_EXTENSION;
        Path folderPath = Paths.get(ARTICLES_PATH);
        if (!Files.exists(folderPath)) {
            Files.createDirectories(folderPath);
        }
        return readFile(folderPath.toString() + "/" + fileName);
    }

    public static byte[] getFileContent(String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(PATH_TO_SAVED_FILES + fileName));
    }

    public static void saveArticle(String articleStr) throws IOException {
        JSONObject article = null;
        try {
            article = (JSONObject) new JSONParser().parse(articleStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String fileName = article.get("label") + FILE_EXTENSION;
        Path articlesPath = Paths.get(ARTICLES_PATH);
        if (!Files.exists(articlesPath)) {
            Files.createDirectories(articlesPath);
        }
        List<String> articleLines = new ArrayList<>();
        articleLines.add(article.toJSONString());
        Files.write(Paths.get(articlesPath.toString() + "/" + fileName), articleLines, StandardCharsets.UTF_8);
    }

    public static String uploadArticle(Part uploadingFilePart) throws IOException {
        Path pathToSavedFiles = Paths.get(PATH_TO_SAVED_FILES);
        if (!Files.exists(pathToSavedFiles)) {
            Files.createDirectories(pathToSavedFiles);
        }
        String extension = getExtension(getFilename(uploadingFilePart));
        int filePostfix = 1;
        String fileName = "a" + filePostfix + "." + extension;
        Path filePath = Paths.get(pathToSavedFiles + "/" + fileName);
        while (Files.exists(filePath)) {
            filePostfix += 1;
            fileName = "a" + filePostfix + "." + extension;
            filePath = Paths.get(pathToSavedFiles + "/" + fileName);
        }
        Part part = uploadingFilePart;
        Files.copy(part.getInputStream(), filePath);
        return fileName;
    }

    private static String readFile(String filePath) throws IOException {
        StringBuilder article = new StringBuilder();
        List<String> lines = Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8);
        for (String line : lines) {
            article = article.append(line);
        }
        return article.toString();
    }

    private static String getExtension(String filename) {
        String[] splittedFilename = filename.split("\\.");
        return splittedFilename[splittedFilename.length - 1];
    }

    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1)
                        .substring(filename.lastIndexOf('\\') + 1);
            }
        }
        return null;
    }
}
