addInitClass(function () {

    $.extend(Ostis.scneditor.SCnNode.prototype, {
        createJQueryNode: function () {

            var $div = $("<div/>");
            $div.append(this.nodeValue.getJQueryNode());

            this.bindSelectNodeByClick($div);
            this.$node = $div;
        },

        getDelCommand: function () {

            return new Ostis.scneditor.RemoveSCNodeCommand(this, this.parentSCNode);
        },

        hasSelecting: function () {

            var hasSelecting = false;
            if (Ostis.scneditor.SCnEditor.isSelected(this) === true) {
                hasSelecting = true;
            } else {
                if (this.nodeValue.hasSelecting() === true) {
                    hasSelecting = true;
                } else {
                    for (var scArcIndex = 0; scArcIndex < this.getSCArcs().length; ++scArcIndex) {
                        var scArc = this.getSCArcs()[scArcIndex];
                        if (scArc.hasSelecting() === true) {
                            hasSelecting = true;
                            break;
                        }
                    }
                }
            }
            return hasSelecting;
        },

        insertSCArc: function (newChild, indexNewChild) {

            this.configureNewArcJQueryNode(newChild.getJQueryNode());
            if (indexNewChild != 0) {
                this.getSCArcs()[indexNewChild - 1].getJQueryNode().after(newChild.getJQueryNode());
            } else {
                if (this.getSCArcs().length === 0) {
                    this.addFirstSCArcJQueryNode(newChild.getJQueryNode());
                } else {
                    this.getSCArcs()[0].getJQueryNode().before(newChild.getJQueryNode());
                }
            }
            this.getSCArcs().splice(indexNewChild, 0, newChild);
        },

        configureNewArcJQueryNode: function ($arc) {

            // Do nothing
        },

        addFirstSCArcJQueryNode: function ($arcNode) {

            this.getJQueryNode().append($arcNode);
        },

        getSelectableSCnNodes: function () {

            var selectableSCnNodes = [];
            selectableSCnNodes.push(this);
            Ostis.scneditor.ArrayUtils.addAll(this.nodeValue.getSelectableSCnNodes(), selectableSCnNodes);
            Ostis.scneditor.ArrayUtils.addAll(AbstractSCnElement.getSelectableNodesFromArray(this.getSCArcs()), selectableSCnNodes);
            return selectableSCnNodes;
        },

        getText: function () {

            return this.nodeValue.getText();
        },

        setText: function (text) {

            this.nodeValue.setText(text);
        },

        setFocus: function () {

            this.nodeValue.setFocus();
        },

        getNodeValue: function () {

            return this.nodeValue;
        },

        setNodeValue: function (nodeValue) {

            var hasChangeAllocationToBottom = false;
            if (this.nodeValue.hasSelecting() === true) {
                Ostis.scneditor.SCnEditor.setSelectedSCnNode(this);
                hasChangeAllocationToBottom = true;
            }
            this.nodeValue.getJQueryNode().after(nodeValue.getJQueryNode()).detach();
            this.nodeValue = nodeValue;
            if (hasChangeAllocationToBottom === true) {
                Ostis.scneditor.SCnEditor.changeAllocationToBottom();
            }
        },

        edit: function () {

            return this.nodeValue.edit();
        },

        changeNodeValueTo: function (newNodeValue) {

            Ostis.scneditor.addCommand(new Ostis.scneditor.ChangeNodeTypeCommand(this, newNodeValue));
        },

        addSCArcClick: function (binaryRelation) {

            Ostis.scneditor.addCommand(new Ostis.scneditor.addSCArcCommand(binaryRelation, this));
        },

        getSCArcIndex: function (child) {

            for (var childIndex = 0; childIndex < this.getSCArcs().length; ++childIndex) {
                if (this.getSCArcs()[childIndex].getId() == child.getId()) {
                    return childIndex;
                }
            }
        },

        addSCArc: function (newChild) {

            this.insertSCArc(newChild, this.getSCArcs().length);
        },

        getSCArcs: function () {

            return this.scArcs;
        },

        remove: function () {

            var hasChangeAllocationToBottom = false;
            if (this.hasSelecting() === true) {
                Ostis.scneditor.SCnEditor.setSelectedSCnNode(this);
                Ostis.scneditor.SCnEditor.changeAllocationToTop()
                hasChangeAllocationToBottom = true;
            }
            this.parentSCNode.removeSCNode();
            if (hasChangeAllocationToBottom === true) {
                Ostis.scneditor.SCnEditor.changeAllocationToBottom();
            }
        },

        removeSCArc: function (removingChild) {

            for (var childIndex = 0; childIndex < this.getSCArcs().length; ++childIndex) {
                if (this.getSCArcs()[childIndex].getId() === removingChild.getId()) {
                    removingChild.getJQueryNode().detach();
                    this.getSCArcs().splice(childIndex, 1);
                    return true;
                }
            }
            return false;
        },

        removeAllSCArcs: function () {

            while (this.getSCArcs().length > 0) {
                this.getSCArcs()[0].remove();
            }
        },

        toJSON: function () {

            var jsonRepresentation = {};
            $.extend(jsonRepresentation, this.getNodeValue().toJSON());
            jsonRepresentation.SCArcs = [];
            jsonRepresentation.SCSynonims = [];
            this.getSCArcs().forEach(function (scArc) {

                if (scArc.getSign().forwardSign !== "=") {
                    jsonRepresentation.SCArcs.push(scArc.toJSON());
                } else {
                    jsonRepresentation.SCSynonims.push(scArc.toJSON());
                }
            });
            return jsonRepresentation;
        },

        leaveSelecting: function (newSelectedNode) {

            Ostis.scneditor.SCnNode.superclass.leaveSelecting.apply(this);
            if (newSelectedNode === undefined || newSelectedNode.nodeValue === undefined || newSelectedNode.nodeValue.isEditing === undefined
                || this.getNodeValue().getId === undefined || newSelectedNode.getId() !== this.getNodeValue().getId()
                || newSelectedNode.nodeValue.isEditing() === false) {
                this.getNodeValue().leaveSelecting();
            }
        },

        ctrlEnterClick: function () {

            this.addSCArcClick(new Ostis.scneditor.BinaryRelation(Ostis.scneditor.SCnArticle.getDefaultAddingArcSign(), this));
            return false;
        },

        clone: function (parent) {

            var clone = new Ostis.scneditor.SCnNode(parent);
            clone.createJQueryNode();
            this.getSCArcs().forEach(function (arc) {

                clone.addSCArc(arc.clone(clone));
            });
            clone.setNodeValue(this.getNodeValue().clone(clone));
            return clone;
        },

        canAddArc: function () {

            return true;
        },

        canChangeNodeValue: function () {

            return true;
        },

        saveInMemory: function (relatedElement) {

            var defResult = new jQuery.Deferred();
            var doneFunc = jQuery.proxy(function (node) {

                console.log('label=' + node.label + ', scaddr=' + node.scaddr);
                defResult.resolve(node);
                this.getSCArcs().forEach(function (arcToSave) {

                    if (arcToSave.getSign() !== "=") {
                        arcToSave.saveInMemory(node);
                    } else {
                        // execute save of Synonym her.
                    }
                });

            }, this);
            Ostis.scneditor.ScStoreUpdater.updateNode(this.getNodeValue()).done(doneFunc).fail(function () {
                console.log('Node not saved ' + node.label);
            });
            return defResult;
        }
    });
});