module.exports = function (grunt) {

    var tasks = require('./grunt_tasks.js')();

    grunt.initConfig(tasks);

    require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-file-append');
    grunt.loadNpmTasks('grunt-exec');
    grunt.registerTask('build', ['concat', 'copy']);
    grunt.registerTask('default', ['build', 'exec:renewComponentsHtml', 'watch']);
    grunt.registerTask('serve', ['concurrent:startScripts']);
};

