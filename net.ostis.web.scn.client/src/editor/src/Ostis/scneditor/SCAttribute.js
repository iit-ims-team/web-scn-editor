addInitClass(function () {

    $.extend(Ostis.scneditor.SCAttribute.prototype, {
        createJQueryNode: function () {

            var $attribute = $("<span/>");
            $attribute.append(this.nodeValue.getJQueryNode());
            $attribute.addClass("scAttribute");
            $attribute.append($("<div class='atrribute-colon'>:</div>"));
            this.$node = $attribute;
            this.bindSelectNodeByClick(this.$node);
            this.nodeValue.scaddr = this.scaddr;
        },

        getDelCommand: function () {

            return new Ostis.scneditor.RemoveSCAttributeCommand(this, this.parentSCNode);
        },

        remove: function () {

            var hasChangeAllocationToBottom = false;
            if (this.hasSelecting() === true) {
                Ostis.scneditor.SCnEditor.setSelectedSCnNode(this);
                Ostis.scneditor.SCnEditor.changeAllocationToTop();
                hasChangeAllocationToBottom = true;
            }
            this.parentSCNode.removeSCAttribute(this);
            if (hasChangeAllocationToBottom === true) {
                Ostis.scneditor.SCnEditor.changeAllocationToBottom();
            }
        },

        hasSelecting: function () {

            return Ostis.scneditor.SCnEditor.isSelected(this) === true;
        },

        getSelectableSCnNodes: function () {

            var selectableSCnNodes = [];
            selectableSCnNodes.push(this);
            return selectableSCnNodes;
        },

        getText: function () {

            return this.nodeValue.getText();
        },

        setText: function (text) {

            this.nodeValue.setText(text);
        },

        afterUserSetText: function () {

            this.parentSCNode.getJQueryNode().click();
        },

        setFocus: function () {

            this.nodeValue.setFocus();
        },

        edit: function () {

            return this.nodeValue.edit();
        },

        toJSON: function () {

            var jsonRepresentation = {};
            jsonRepresentation.scaddr = this.scaddr;
            $.extend(jsonRepresentation, this.nodeValue.toJSON());
            return jsonRepresentation;
        },

        leaveSelecting: function () {

            Ostis.scneditor.SCnNode.superclass.leaveSelecting.apply(this);
            this.nodeValue.leaveSelecting();
        },

        clone: function (parent) {

            var clone = new Ostis.scneditor.SCAttribute(parent);
            clone.setText(this.getText());
            return clone;
        },

        appendTo: function (parent) {

            this.parentSCNode = parent;
            parent.addSCAttribute(this);
        },

        canAppendTo: function (scNode) {

            return scNode.canAddAttribute();
        }
    });
});
