<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="net.ostis.dbapi.SettingSetterObject"%>
<%@page import="javax.servlet.http.Cookie" %>
<%@page import="java.net.URLDecoder" %>
<%@ page import="javax.servlet.*" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Settings</title>
    <link rel="stylesheet" href="./resources/user-forms.css" type="text/css" />
    <script src="./resources/jsonrpc.js" type="text/javascript" ></script>
    <script src="./resources/profileOperations.js" type="text/javascript"></script>
    <script src="./resources/cookieUserAPI.js" type="text/javascript" ></script>
    <script type="text/javascript">
        var locate = <%ServletContext context = session.getServletContext(); String realContextPath = context.getRealPath(request.getContextPath()); out.print("\"" + realContextPath.replace("\\", "\\\\\\\\") + "\"");%>;
        jsonrpc = new JSONRpcClient("JSON-RPC");
        jsonrpc.DatabaseFilename.setPath(locate);
    </script>


</head>
<body onload="initSettingsPage()">
<form action="index.jsp" id="inputSettingsForm" name="inputSettings">
    Величина отступа:<br>
<p><input type="radio" name="ident" value="5"> 5<Br>
    <input type="radio" name="ident" value="10"> 10<Br>
    <input type="radio" name="ident" value="15"> 15<Br>
    <input type="radio" name="ident" value="20"> 20</p>

    Конфигурация элементов:<br>
<p><input type="radio" name="skin" value="defaultSkin"> Default Style<Br>
    <input type="radio" name="skin" value="secondStyle"> Style N2<Br>
    <input type="radio" name="skin" value="newStyle"> Style N3</p>

    Цвет фона:<br>
<p><input type="radio" name="bgcolor" value="white"> White<Br>
    <input type="radio" name="bgcolor" value="yellow"> Yellow<Br>
    <input type="radio" name="bgcolor" value="grey"> Grey</p>
    Цвет шрифта UI редактора:<br>
<p><input type="radio" name="fontcolor" value="black"> Black<Br>
    <input type="radio" name="fontcolor" value="red"> Red<Br>
    <input type="radio" name="fontcolor" value="blue"> Blue</p>
    Цвет шрифта контента:<br>
<p><input type="radio" name="contentcolor" value="black"> Black<Br>
    <input type="radio" name="contentcolor" value="red"> Red<Br>
    <input type="radio" name="contentcolor" value="blue"> Blue</p>
    Набор SC-элементов:
<p><input type="radio" name="scskin" value="defaultSCSkin"> Default Skin<Br>
    <input type="radio" name="scskin" value="customSCSkin"> Custom Skin</p>
    Панель сохранения:
<p><input type="radio" name="fileSavePanel" value="0"> Off<Br>
    <input type="radio" name="fileSavePanel" value="1"> On</p><br>
    <br>
    <input class="btn" type="button" onclick="setSettings(inputSettings)" value="Подтвердить"/>  <input class="btn"type="submit" value="На главную"/>
</form>





<jsp:useBean id="JSONRPCBridge" scope="session"
             class="com.metaparadigm.jsonrpc.JSONRPCBridge" />

<%
    SettingSetterObject settingSetterObject = new SettingSetterObject();
    JSONRPCBridge.registerObject("SettingSetterObject", settingSetterObject);

%>



</body>
</html>

