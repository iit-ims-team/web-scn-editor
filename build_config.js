module.exports = function (property) {

//    Change scn-editor-build-dest path according to your actual path    
//    var scWebPath = '/mnt/d_win/development/ims/ostis/sc-web/';
    var scWebPath = '../sc-web/';
    var scScriptsPath = '../scripts';
    var config = {
        'scn-editor-build-dest': scWebPath + 'client/static/components/js/scn-editor/',
        'scs-cmp-build-dest': scWebPath + 'client/static/components/js/scs/',
        'scs-css-build-dest': scWebPath + 'client/static/components/css/',
        'components-def-folder': scWebPath + 'client/templates',
        'components-def-file': scWebPath + 'client/templates/components.html',
        'ims-scripts-folder': scScriptsPath
    };

    return property ? config[property] : config;
};