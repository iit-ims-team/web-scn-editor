Ostis.scneditor.JsonToScsConverter = {
    LABEL: "label",

    SC_NODE: "SCNode",

    FILE: "file",

    FILE_NAME: "fileName",

    SC_NODES: "SCNodes",

    SC_ARCS: "SCArcs",

    SC_SYNONIMS: "SCSynonims",

    SC_ATTRIBUTES: "SCAttributes",

    SET: "set",

    CONTOUR: "contour",

    convertType: function (jsonType) {
        if ('⇒' == jsonType) {
            return '=>';
        } else if ('∊' == jsonType) {
            return '->';
        }
    },

    convertArticle: function (jsonArticle, scsArr) {
        var promises = [];
        var resolvingResultsStore = {};
        resolvingResultsStore.contentArray = [];
        resolvingResultsStore.systemIdentifiersArray = [];
        resolvingResultsStore.labelToScAddrMap = [];
        resolvingResultsStore.labelToScAddrMap[jsonArticle.label] = jsonArticle.scaddr;

        var scs = "";
        var scNodes = jsonArticle[this.SC_NODES];
        var arrayForResolving = this.collectIdsForResolving(jsonArticle);
        arrayForResolving.idsForResolvingSysIdtf.push(jsonArticle.scaddr);

        var defResolvingNrelSysIdtf = $.Deferred();
        SCWeb.core.Server.resolveScAddr(['nrel_system_identifier'], function (keynodes) {
            var nrelSysIdIdtf = keynodes['nrel_system_identifier'];
            console.log('nrel_system_identifier addr ' + nrelSysIdIdtf);
            var resolvingSysIdentifiers = Ostis.scneditor.JsonToScsConverter.getSysIds(arrayForResolving.idsForResolvingSysIdtf, nrelSysIdIdtf, resolvingResultsStore.systemIdentifiersArray);

            resolvingSysIdentifiers.done(function () {
                defResolvingNrelSysIdtf.resolve();
            });
        });
        promises.push(defResolvingNrelSysIdtf);

        var defGetContentForFiles = $.Deferred();
        var res = this.getContentForFiles(arrayForResolving.idsForResolvingContent, resolvingResultsStore.contentArray);
        res.done(
            function () {
                for (var i = 0; i < scNodes.length; ++i) {
                    var scNode = scNodes[i];
                    scs += Ostis.scneditor.JsonToScsConverter.convertSCNode(resolvingResultsStore, scNode, "");
                    scs += ";;\n";
                }

                scsArr.push(scs);
                defGetContentForFiles.resolve(scs);
            });
        promises.push(defGetContentForFiles);

        return $.when.apply(undefined, promises).promise();
    },

    getContentForFiles: function (array, contentArray) {
        var _i, index;
        var promises = [];

        for (index = _i = 0; _i < array.length; index = ++_i) {
            (function (index) {
                var def = $.Deferred();

                Ostis.scneditor.ServerDao.prototype.getFileContent(array[index], function (res) {
                    console.log('content of ' + array[index] + ': ' + res);
                    contentArray[array[index]] = res;
                    def.resolve();
                });

                promises.push(def);
            })(index);
        }

        return $.when.apply(undefined, promises).promise();
    },

    getSysIds: function (ids, nrelSysIdAddr, resultSaveArray) {
        var _i, index;
        var promises = [];

        for (index = _i = 0; _i < ids.length; index = ++_i) {
            (function (index) {
                var def = $.Deferred();
                window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F, [
                    ids[index],
                    sc_type_arc_common | sc_type_const,
                    sc_type_link,
                    sc_type_arc_pos_const_perm,
                    nrelSysIdAddr]).done(function (linkNodes) {
                    window.sctpClient.get_link_content(linkNodes[0][2], 'string').done(function (content) {
                        resultSaveArray[ids[index]] = content;
                        console.log("success resolving sysIdtf: " + content);
                        def.resolve();
                    });
                })
                    .fail(function () {
                        console.log("fail during resolving " + ids[index]);
                    });
                promises.push(def);
            })(index);
        }

        return $.when.apply(undefined, promises).promise();
    },

    collectIdsForResolving: function (jsonArticle) {
        var array = {};
        array.idsForResolvingContent = [];
        array.idsForResolvingSysIdtf = [];

        var scNodes = jsonArticle[this.SC_NODES];

        for (var i = 0; i < scNodes.length; ++i) {
            var scNode = scNodes[i];
            this.collectSCNode(scNode, array);
        }

        return array;
    },

    convertSCArcToSCs: function (resolvingResultsStore, scArcJSON, indent) {
        var scs = indent;
        scs += this.convertType(scArcJSON["type"]);

        var scAttributes = scArcJSON[this.SC_ATTRIBUTES];

        for (var i = 0; i < scAttributes.length; ++i) {
            var scAttr = scAttributes[i];
            scs += scAttr.getText();
            scs += ": ";
        }

        scs += this.convertSCNode(resolvingResultsStore, scArcJSON[this.SC_NODE], indent, true);

        return scs;
    },

    convertSCNode: function (resolvingResultsStore, scNode, indent, isAfterSign) {
        var scs = "";

        if (isAfterSign !== true) {
            scs += indent;
        }

        if (scNode[this.SET] === true) {
            var scNodesOfSet = scNode[this.SC_NODES];
            scs += "{\n";
            for (var i = 0; i < scNodesOfSet.length; ++i) {
                var scNodeOfSet = scNodesOfSet[i];
                scs += this.convertSCNode(resolvingResultsStore, scNodeOfSet, indent + "    ");
                scs += ";\n";
            }
            scs += indent + "}";
        } else if (scNode[this.CONTOUR] === true) {
            scs += "[*\n";
            var scNodesOfContour = scNode[this.SC_NODES];

            for (var i = 0; i < scNodesOfContour.length; ++i) {
                var scNodeOfContour = scNodesOfContour[i];
                scs += this.convertSCNode(resolvingResultsStore, scNodeOfContour, indent + "    ");
                scs += ";\n";
            }

            scs += indent + ";*]";
        } else if (scNode[this.FILE] === true) {
            scs += "[" + resolvingResultsStore.contentArray[scNode[this.FILE_NAME]] + "]";
        } else {
            if (scNode.scaddr !== undefined) {
                scs += resolvingResultsStore.systemIdentifiersArray[scNode.scaddr];
            } else {
                scs += resolvingResultsStore.systemIdentifiersArray[resolvingResultsStore.labelToScAddrMap[scNode[this.LABEL]]];

            }
        }

        scs += this.convertSCArcsAndSCSynonims(resolvingResultsStore, scNode[this.SC_ARCS], scNode[this.SC_SYNONIMS], indent);

        return scs;
    },

    convertSCArcsAndSCSynonims: function (resolvingResultsStore, scArcs, scSynonims, indent) {
        var scs = "";

        if (scArcs.length != 0 || scSynonims.length != 0) {
            scs += "(*\n";

            for (var i = 0; i < scArcs.length; ++i) {
                var scArc = scArcs[i];
                scs += this.convertSCArcToSCs(resolvingResultsStore, scArc, indent + "    ");
                scs += ";\n";
            }

            for (var i = 0; i < scSynonims.length; ++i) {
                var scSynonim = scSynonims[i];
                scs += this.convertSCSynonimToSCs(resolvingResultsStore, scSynonim, indent + "    ");
                scs += ";\n";
            }

            scs += indent + ";*)";
        }

        return scs;
    },

    convertSCSynonimToSCs: function (resolvingResultsStore, scSynonim, indent) {
        var scs = indent;
        scs += "=";
        scs += this.convertSCNode(resolvingResultsStore, scSynonim, indent, true);

        return scs;
    },

    convertSCArcToSCs: function (resolvingResultsStore, scArcJSON, indent) {
        var scs = indent;
        scs += this.convertType(scArcJSON["type"]);
        var scAttributes = scArcJSON[this.SC_ATTRIBUTES];
        for (var i = 0; i < scAttributes.length; ++i) {
            var scAttr = scAttributes[i];
            scs += resolvingResultsStore.systemIdentifiersArray[scAttr.scaddr];
            scs += ": ";
        }
        scs += this.convertSCNode(resolvingResultsStore, scArcJSON[this.SC_NODE], indent, true);

        return scs;
    },

    collectSCNode: function (scNode, array) {
        if (scNode[this.SET] === true) {
            var scNodesOfSet = scNode[this.SC_NODES];
            for (var i = 0; i < scNodesOfSet.length; ++i) {
                var scNodeOfSet = scNodesOfSet[i];
                this.collectSCNode(scNodeOfSet, array);
            }
        } else if (scNode[this.CONTOUR] === true) {
            var scNodesOfContour = scNode[this.SC_NODES];
            for (var i = 0; i < scNodesOfContour.length; ++i) {
                var scNodeOfContour = scNodesOfContour[i];
                this.collectSCNode(scNodeOfContour, array);
            }
        } else if (scNode[this.FILE] === true) {
            array.idsForResolvingContent.push(scNode[this.FILE_NAME]);
        } else if (scNode.scaddr !== undefined) {
            array.idsForResolvingSysIdtf.push(scNode.scaddr);
        }

        this.collectSCArcsAndSCSynonims(scNode[this.SC_ARCS], scNode[this.SC_SYNONIMS], array);
    },

    collectSCArcsAndSCSynonims: function (scArcs, scSynonims, array) {
        if (scArcs.length != 0 || scSynonims.length != 0) {
            for (var i = 0; i < scArcs.length; ++i) {
                var scArc = scArcs[i];
                this.collectSCArcToSCs(scArc, array);
            }
            for (var i = 0; i < scSynonims.length; ++i) {
                var scSynonim = scSynonims[i];
                this.collectSCSynonimToSCs(scSynonim, array);
            }
        }
    },

    collectSCSynonimToSCs: function (scSynonim, array) {
        this.collectSCNode(scSynonim, array);
    },

    collectSCArcToSCs: function (scArcJSON, array) {
        var scAttributes = scArcJSON[this.SC_ATTRIBUTES];
        for (var i = 0; i < scAttributes.length; ++i) {
            var scAttr = scAttributes[i];
            array.idsForResolvingSysIdtf.push(scAttributes[i].scaddr)
        }

        this.collectSCNode(scArcJSON[this.SC_NODE], array);
    }
};