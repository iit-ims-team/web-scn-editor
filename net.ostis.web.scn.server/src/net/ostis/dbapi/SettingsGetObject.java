package net.ostis.dbapi;

import java.sql.*;

/**
 * Created by antonkw on 03.12.14.
 */
public class SettingsGetObject {
    public String getAllSettings(String entryKey, String login, String password) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE login = '"+ login + "';");

        if (!rs.next())
            return "wrong";

        if (!Md5Hash.md5(rs.getString("id") + password + rs.getString("salt")).equals(rs.getString("password")))
            return "wrong";

        String id = rs.getString(1);
        String entryHash = Md5Hash.md5(id + entryKey + rs.getString("salt"));
        String sql = "UPDATE ostisUsers SET entryHash = '" + entryHash + "' WHERE id = '" + id  + "';";
        stmt.executeUpdate(sql);
        rs = stmt.executeQuery("select * from userSettings WHERE id = '"+ id + "';");
        String allSettings = id + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4) + " " + rs.getString(5) + " " + rs.getString(6) + " " + rs.getString(7) + " " + rs.getString(8);
        stmt.close();
        c.close();
        return  allSettings;

    }
}
