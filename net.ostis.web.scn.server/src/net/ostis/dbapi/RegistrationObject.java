package net.ostis.dbapi;

import java.io.*;
import java.sql.*;

/**
 * Created by antonkw on 01.12.14.
 */

public class RegistrationObject {

    public int addNewUser (String username, String pass, String entryKey) throws IOException, ClassNotFoundException, SQLException {
        int id = 0;
        if (isUserExist(username))
            return id;
        String lastUserIdfilename = DatabaseFilename.getLastUserIdConfigName();
        System.out.println(lastUserIdfilename);
        File file = new File(lastUserIdfilename);
        BufferedReader br = null;
        try {
            br = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream( file ), "UTF-8"
                    )
            );
        } catch (UnsupportedEncodingException | FileNotFoundException e) {
            e.printStackTrace();
        }
        String line = null;
        try {
            assert br != null;
            if ((line = br.readLine()) != null) {
                id = Integer.parseInt(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        br.close();
        addUserToDataBase(id, username, pass, entryKey);
        try (PrintWriter out = new PrintWriter(new File(lastUserIdfilename).getAbsoluteFile())) {
            int nextId = id + 1;
            out.print(nextId);
        }
        return id;
    }

    public void addUserToDataBase(int id, String username, String pass, String entryKey) throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = connection.createStatement();
        stmt.setQueryTimeout(5);
        String salt = RandomString.generateString();
        pass = Md5Hash.md5(Integer.toString(id) + pass + salt);
        String entryHash = Md5Hash.md5(Integer.toString(id) + entryKey + salt);
        String sql = "INSERT INTO ostisUsers VALUES ('" + id + "', '" + username + "', '" + pass  + "', '" + salt + "', '" + entryHash +"');";
        stmt.executeUpdate(sql);
        stmt.close();
        connection.close();
        addDefaultSettingsForNewUser(id);
    }

    public void addDefaultSettingsForNewUser(String entryKey, int id) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE id = '"+ id + "';");
        if (!Md5Hash.md5(id + entryKey + rs.getString("salt")).equals(rs.getString("entryHash"))) {
            System.out.print("lol1");
            return;
        }
        String sql = "INSERT INTO userSettings VALUES ('" + id + "', '10', 'defaultStyle', 'white', 'black', 'black', 'defaultSCSkin', '0');";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }


    public void addDefaultSettingsForNewUser(int id) throws ClassNotFoundException, SQLException {
        Connection c = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        c = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = c.createStatement();
        String sql = "INSERT INTO userSettings VALUES ('" + id + "', '10', 'defaultStyle', 'white', 'black', 'black', 'defaultSCSkin', '0');";
        stmt.executeUpdate(sql);
        stmt.close();
        c.close();
    }

    public boolean isUserExist (String login) throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Statement stmt = null;
        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:" + DatabaseFilename.getDataBaseName());
        stmt = connection.createStatement();
        stmt.setQueryTimeout(5);  // set timeout to 30 sec.
        // statement.executeUpdate(".separator \" \"");
        ResultSet rs = stmt.executeQuery("select * from ostisUsers WHERE login = '"+ login + "';");
        if (rs.next()) {
            stmt.close();
            connection.close();
            return true;
        }
        stmt.close();
        connection.close();
        return false;
    }
}
