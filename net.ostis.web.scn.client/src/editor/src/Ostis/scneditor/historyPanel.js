(function ($) {

    $.fn.historyPanel = function (method, opt) {

        var options = $.extend({}, opt);
        return this.each(function () {

            var $this = $(this);
            if (method === "addCommand") {
                var $commandItem = $(Ostis.scneditor.handlebars.render('history_panel_item', {
                    text: options.commandText
                }));
                $commandItem.on('click', options.commandClickCallback);
                Ostis.scneditor.SCnArticle.historyPanel.find("> li:gt(" + options.commandIndex + ")").remove();
                Ostis.scneditor.SCnArticle.historyPanel.find("> li:eq(" + options.commandIndex + ")").remove();
                Ostis.scneditor.SCnArticle.historyPanel.append($commandItem);
            } else if (method === "selectCommand") {
                $this.find(".historyPanel > li").eq(options.commandIndex).css('color', 'black');
            } else if (method === "unSelectCommand") {
                $this.find(".historyPanel > li").eq(options.commandIndex).css('color', 'gray');
            } else if (method === "removeLastCommand") {
                $this.find(".historyPanel > li:last").remove();
            } else {
                var $historyPanelContainer = $(Ostis.scneditor.handlebars.render('history_panel')),
                    $historyPanel = $historyPanelContainer
                        .find('.historyPanel'),
                    $toggleButton = $historyPanelContainer.find('.historyPanelToggleButton');
                Ostis.scneditor.SCnArticle.historyPanel = $historyPanel;
                $toggleButton.bind('click', function () {

                    $('.content-center-container').toggleClass('no-history');
                    $('.historyPanelContainer').toggleClass('isHidden');
                });
                $this.prepend($historyPanelContainer);
            }
        });
    };
})(jQuery);
