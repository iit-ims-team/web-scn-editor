(function ($) {

    $.fn.selectSignDropdownComponent = function (opt) {

        var options = $.extend({}, opt);
        return this.each(function () {

            var scnNode = options.scnNode;
            var $dropdownToggle = $("<a/>");
            $dropdownToggle.attr("href", "#");
            $dropdownToggle.attr("data-toggle", "dropdown");
            $dropdownToggle.addClass("SCnFieldMarker selectedSCnNode");
            $dropdownToggle.attr("role", "button");
            $dropdownToggle.append(scnNode.getSign().getText());
            var signGroups = options.signGroups;
            var $dropdown = $("<div/>");
            var $signsDropdownMenus = [];
            for (var i = 0; i < signGroups.length; i++) {
                var signGroup = signGroups[i];
                var backwardItem;
                var backwardSignGroup = [];
                signGroups[i].forEach(function (item) {
                    backwardItem = item.clone();
                    backwardSignGroup.push(backwardItem);
                });
                backwardSignGroup.forEach(function (item) {
                    item.changeDirection();
                });
                var $signsDropdownMenu = createSignItemsList(scnNode, signGroup.concat(backwardSignGroup));
                if (containsRelation(signGroup, scnNode.getSign())) {
                    $signsDropdownMenu.css('display', 'block');
                    $dropdown.currentMenu = $signsDropdownMenu;
                } else {
                    $signsDropdownMenu.css('display', 'none');
                }
                createNextAndPrevButton($dropdown, $signsDropdownMenu);
                $signsDropdownMenus.push($signsDropdownMenu);
            }

            $dropdown.append($dropdownToggle);
            for (var i = 0; i < $signsDropdownMenus.length; i++) {
                var $signsDropdownMenu = $signsDropdownMenus[i];
                if (i === 0) {
                    $signsDropdownMenu.prev = $signsDropdownMenus[signGroups.length - 1];
                } else {
                    $signsDropdownMenu.prev = $signsDropdownMenus[i - 1];
                }
                if (i === $signsDropdownMenus.length - 1) {
                    $signsDropdownMenu.next = $signsDropdownMenus[0];
                } else {
                    $signsDropdownMenu.next = $signsDropdownMenus[i + 1];
                }
                $dropdown.append($signsDropdownMenu);
            }
            var focusIn = false;
            $dropdown.mouseenter(function () {
                focusIn = true;
            });
            $dropdown.mouseleave(function () {
                focusIn = false;
            });
            $dropdown.focusin(function () {
                focusIn = true;
            });
            $dropdown.focusout(function () {
                if (!focusIn) {
                    scnNode.getJQueryNode().find("> .SCnField > .dropdown")
                        .after(Ostis.scneditor.BinaryRelation.createSCnFieldMarker(scnNode))
                        .detach();
                }
                focusIn = false;
            });
            $dropdown.addClass("dropdown open");
            $dropdown.on('keydown', function (e) {
                changeMenu($dropdown, e.which);
            });
            $(this).prepend($dropdown);
            $dropdown.currentMenu.find('a').eq(0).focus();
        });

        function createSignItemsList(scnNode, relationSigns) {

            var $dropdownMenu = $("<ul/>");
            $dropdownMenu.addClass("dropdown-menu");
            $dropdownMenu.attr("role", "menu");
            $dropdownMenu.attr("aria-labelledby", "drop5");
            for (var signIndex = 0; signIndex < relationSigns.length; ++signIndex) {
                var $li = $("<li/>");
                $li.attr("role", "presentation");
                var $div = $("<a/>");
                $div.attr("role", "menuitem");
                $div.attr("tabindex", "-1");
                $div.attr("href", "#");
                $div.text(relationSigns[signIndex].getText());
                $li.append($div);
                $dropdownMenu.append($li);
                options.bindSetSignRelationCallback(scnNode, $li);
            }
            return $dropdownMenu;
        }

        function createNextAndPrevButton($dropdown, $signsDropdownMenu) {
            var $buttonGroup = $('<div class="btn-group btn-dropdown-menu-group">');
            var $buttonPrev = $('<button type="button" class="btn btn-default btn-xs">' +
                '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>' +
                '</button>');
            $buttonGroup.append($buttonPrev);
            $buttonPrev.bind("click", function () {
                changeMenu($dropdown, options.selectLeftCode);
            });
            var $buttonNext = $('<button type="button" class="btn btn-default btn-xs">' +
                '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' +
                '</button>');
            $buttonGroup.append($buttonNext);
            $buttonNext.bind("click", function () {
                changeMenu($dropdown, options.selectRightCode);
            });
            $signsDropdownMenu.prepend($buttonGroup);
        }

        function changeMenu($dropdown, keycode) {
            var nextMenu;
            if (keycode == options.selectLeftCode) {
                nextMenu = $dropdown.currentMenu.prev;
            } else if (keycode == options.selectRightCode) {
                nextMenu = $dropdown.currentMenu.next;
            } else {
                return true;
            }
            changeMenuTabs($dropdown.currentMenu, nextMenu);
            $dropdown.currentMenu = nextMenu;
            $dropdown.currentMenu.find('a').eq(0).focus();
            return false;
        }
    };

    function changeMenuTabs(currentMenu, nextMenu) {

        currentMenu.css('display', 'none');
        nextMenu.css('display', 'block');
    }

    function containsRelation(relations, relation) {

        for (var relationIndex in relations) {
            if (relation.forwardSign == relations[relationIndex].forwardSign) {
                return true;
            }
        }
        return false;
    }
})(jQuery);
