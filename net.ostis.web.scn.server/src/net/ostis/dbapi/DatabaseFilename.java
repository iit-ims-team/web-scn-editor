package net.ostis.dbapi;

import java.io.File;

/**
 * Created by antonkw on 08.12.14.
 */
public class DatabaseFilename {
    static private String filename = "";
    public static String getPath() {
        return filename;
    }
    public static String getLastUserIdConfigName() {
        return filename + File.separator + "resources" + File.separator + "databasefiles" + File.separator + "lastUserId.config";
    }
    public static String getDataBaseName() {
        System.out.print(filename + File.separator + "resources" + File.separator + "databasefiles" + "users.db");
        return filename + File.separator + "resources" + File.separator + "databasefiles" +  File.separator + "users.db";

    }
    public static void setPath(String path) {
        System.out.print(path);
        filename = path;
    }
}
