Ostis.scneditor.ScStoreUpdater = (function () {

    return {

        /**
         * node - js object with the following attributes:
         * <li>label</li>
         * <br>
         * <li>type</li>
         * <br>
         */
        updateNode: function (node) {

            var defRes = new jQuery.Deferred();

            var createMainIdtf = function (node) {
                node.obj.needUpdateInSC = false;
                if (node.label !== Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER) {
                    console.log("Create nrel_main_idtf for node : " + node.label);
                    window.sctpClient.create_link().done(function (linkAddr) {
                        window.sctpClient.set_link_content(linkAddr, node.label).done(function () {
                            window.sctpClient.create_arc(( sc_type_arc_common | sc_type_const ), node.scaddr, linkAddr).done(function (arcAddr) {
                                window.sctpClient.create_arc(sc_type_arc_pos_const_perm, window.scKeynodes.nrel_main_idtf, arcAddr).fail(function () {
                                    console.log('fail create_arc(link, main idtf)');
                                });
                            }).fail(function () {
                                console.log('fail create_arc(node, link)');
                            });
                            window.sctpClient.create_arc(sc_type_arc_pos_const_perm, SCWeb.core.Translation.getCurrentLanguage(), linkAddr).fail(function () {
                                console.log('fail create_arc(link, language)');
                            });
                        }).fail(function () {
                            console.log('fail set_link_content to sc-link');
                        });
                    }).fail(function () {
                        console.log('fail create_link for node with addr=' + node.scaddr);
                    });
                } else {
                    return false;
                }
            };

            var updateTranslation = function (node) {
                node.obj.needUpdateInSC = false;
                console.log("Try update main idtf for " + node.label);
                window.sctpClient.iterate_constr(
                    SctpConstrIter(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F,
                        [parseInt(node.scaddr),
                            sc_type_arc_common | sc_type_const,
                            sc_type_link,
                            sc_type_arc_pos_const_perm,
                            window.scKeynodes.nrel_main_idtf
                        ],
                        {"x": 2}),
                    SctpConstrIter(SctpIteratorType.SCTP_ITERATOR_3F_A_F,
                        [window.SCWeb.core.Server._current_language,
                            sc_type_arc_pos_const_perm,
                            "x"
                        ])
                ).done(function (results) {

                    var linkAddr = results.get(0, "x");
                    console.log("we got link");
                    window.sctpClient.set_link_content(linkAddr, node.label)
                        .fail(function () {

                            console.log("fail set_link_content " + node.label)
                        });
                }).fail(function () {

                    console.log("Not find idtf for " + node.label);
                    createMainIdtf(node);
                });
            };

            var updateLinkContent = function (node) {
                node.needUpdateInSC = false;
                if (node.type === 'pdf') {
                    return;
                }
                if (node.type !== 'string') {
                    var scGarbage = Ostis.scneditor.KeynodesHandler.scKeynodes.sc_garbage;
                    var nrelFormat = window.scKeynodes.nrel_format;
                    var formatHtml = window.scKeynodes.format_html;

                    window.sctpClient.iterate_elements(SctpIteratorType.SCTP_ITERATOR_5F_A_A_A_F,
                        [node.scaddr,
                            sc_type_arc_common | sc_type_const,
                            sc_type_node,
                            sc_type_arc_pos_const_perm,
                            window.scKeynodes.nrel_format
                        ]
                    ).done(function (results) {
                        if (results[0][2] !== formatHtml) {
                            window.sctpClient.create_arc(sc_type_arc_pos_const_perm, scGarbage, results[0][1])
                        }
                    }).fail(function () {
                        console.log("Not find nrel_format");
                    });

                    window.sctpClient.create_arc(sc_type_arc_common | sc_type_const, node.scaddr, formatHtml)
                        .done(function (arc_addr) {
                            window.sctpClient.create_arc(sc_type_arc_pos_const_perm, nrelFormat, arc_addr)
                                .fail(function () {
                                    console.log("Not create nrelFormat");
                                });
                        }).fail(function () {
                        console.log("Not create " + node.scaddr + "=> format_html");
                    });
                }

                window.sctpClient.set_link_content(node.scaddr, node.content)
                    .fail(function () {
                        console.log("fail in set_link_content in updateLinkContent" + node.scaddr)
                    });
            };

            if (node.file) {
                if (node.scaddr) {
                    if (node.obj.needUpdateInSC) {
                        console.log("updateLink: " + node.obj.content + ", scaddr=" + node.obj.scaddr);
                        updateLinkContent(node.obj);
                    }
                    defRes.resolve(node);
                } else {
                    console.log("createLink: " + node.obj.content);
                    window.sctpClient.create_link().done(function (linkAddr) {
                        node.scaddr = linkAddr;
                        node.obj.scaddr = linkAddr;
                        console.log("updateLink: " + node.obj.content + ", scaddr=" + node.obj.scaddr);
                        updateLinkContent(node.obj);
                        defRes.resolve(node);
                    }).fail(function () {
                        console.log("fail to create link");
                        defRes.reject();
                    });
                }
            } else if (node.contour) {
                if (node.scaddr) {
                    defRes.resolve(node);
                } else {
                    window.sctpClient.create_node(Ostis.scneditor.SCn2SCTypeTranslator.calcScNodeType(node)).done(function (scaddr) {

                        console.log("createNewStruct: " + scaddr);
                        node.scaddr = scaddr;
                        node.obj.scaddr = scaddr;
                        defRes.resolve(node);
                    }).fail(function () {

                        console.log("fail to create struct");
                        ;
                        defRes.reject();
                    });
                }
            } else if (node.set) {
                if (node.scaddr) {
                    defRes.resolve(node);
                } else {
                    window.sctpClient.create_node(Ostis.scneditor.SCn2SCTypeTranslator.calcScNodeType(node)).done(function (scaddr) {

                        console.log("createNewTuple: " + scaddr);
                        node.scaddr = scaddr;
                        node.obj.scaddr = scaddr;
                        defRes.resolve(node);
                    }).fail(function () {

                        console.log("fail to  create  tuple");
                        ;
                        defRes.reject();
                    });
                }
            } else if (node.scaddr) {
                if (node.obj.needUpdateInSC) {
                    console.log("updateNode: " + node.label + ", scaddr=" + node.scaddr);
                    updateTranslation(node);
                }
                defRes.resolve(node);
            } else {
                if (node.SCSynonims != undefined && node.SCSynonims.length > 0) {
                    window.sctpClient.create_node(sc_type_node_struct | sc_type_const).done(function (scaddr) {

                        console.log("createNewStruct: " + scaddr);
                        node.scaddr = scaddr;
                        node.obj.scaddr = scaddr;
                        createMainIdtf(node);
                        defRes.resolve(node);
                    }).fail(function () {

                        console.log("fail to create struct");
                        ;
                        defRes.reject();
                    });
                } else {
                    window.sctpClient.create_node(Ostis.scneditor.SCn2SCTypeTranslator.calcScNodeType(node)).done(function (scaddr) {

                        console.log("createNewNode: " + node.label + ", scaddr=" + scaddr);
                        node.scaddr = scaddr;
                        node.obj.scaddr = scaddr;
                        createMainIdtf(node);
                        defRes.resolve(node);
                    }).fail(function () {

                        console.log("fail create_node for node=" + node.label);
                        defRes.reject();
                    });
                }
            }

            return defRes.promise();
        }
    };
})();