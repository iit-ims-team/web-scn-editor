Ostis.scneditor.ArrayUtils = (function () {

    return {
        /**
         * Copy elements reference from source array to target array
         *
         * @param sourceArray -
         *                array which element reference will be copied.
         * @param targetArray -
         *                array which will contains new elements
         */
        addAll: function (sourceArray, targetArray) {

            for (var elemIndex = 0; elemIndex < sourceArray.length; ++elemIndex) {
                targetArray.push(sourceArray[elemIndex]);
            }
        }
    };
})();
