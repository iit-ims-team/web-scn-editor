addInitClass(function () {

    $.extend(Ostis.scneditor.FileNodeValue.prototype, {
        setContent: function (fileContent, format) {
            if (format === 'image') {
                this.setImage(fileContent);
            } else if (format === 'html') {
                this.setHtml(fileContent);
            } else if (format === 'youtube_link') {
                this.setYoutubeLink(fileContent);
            } else if (format === 'googlemaplink') {
                this.setGooglemaplink(fileContent);
            } else if (format === 'video') {
                this.setVideo(fileContent);
            } else if (format === 'pdf') {
                this.setPdf(fileContent);
            } else {
                this.setText(fileContent);
            }
        },

        setText: function (content) {
            this.data = content;
            this.content = content;
            this.type = 'string';
            this.getJQueryNode().text(this.content);
        },

        setHtml: function (content) {
            this.data = content;
            this.content = content;
            this.type = 'html';
            this.getJQueryNode().html(this.content);
        },

        setImage: function (content) {
            this.data = content;
            this.content = '<img src="' + content + '" style="width: 100%; height: 100%;" />';
            this.type = 'image';
            this.getJQueryNode().html(this.content);
        },

        setYoutubeLink: function (data) {
            this.data = data;
            this.content = '<iframe title="YouTube video player" class="youtube-player" type="text/html" width="640" ' +
                'height="390" src="http://www.youtube.com/embed/' + data + '?wmode=transparent" frameborder="0" ' +
                'allowFullScreen></iframe>';
            this.type = 'youtube';
            this.getJQueryNode().html(this.content);
        },

        setGooglemaplink: function (data) {
            this.data = data;
            this.content = '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" ' +
                'marginwidth="0" src="' + data + '" style="color:#0000FF;text-align:left">View Larger Map' +
                '</a></small></iframe>';
            this.type = 'googlemap';
            this.getJQueryNode().html(this.content);
        },

        clone: function () {
            var clone = new Ostis.scneditor.FileNodeValue(this.fileName);
            return clone;
        },

        setVideo: function (content) {
            this.data = content;
            this.content = '<video src="' + content
                + '" style="max-width: 100%; max-height: 100%;" controls></video>';
            this.type = 'video';
            this.getJQueryNode().html(this.content);
        },

        setPdf: function (scaddr) {
            this.data = 'pdf content - ' + scaddr;
            this.content = 'pdf content - ' + scaddr;
            this.type = 'pdf';
            this.getJQueryNode().html('<div id="window' + scaddr + '"</div>');
            var sandbox = new SCWeb.core.ComponentSandbox({
                container: "window" + scaddr,
                addr: scaddr,
                is_struct: false,
                format_addr: 'format_pdf',
                canEdit: true
            });
            PdfComponent.factory(sandbox)
        },

        setUpdateInSC: function () {
            this.needUpdateInSC = true;
        }
    });
});
