addInitClass(function () {

    $.extend(Ostis.scneditor.ImageFileNodeValue.prototype, {
        setImg: function ($img) {

            this.getJQueryNode().find("> img").detach();
            this.getJQueryNode().append($img);
        },

        clone: function () {

            var clone = new Ostis.scneditor.ImageFileNodeValue(this.fileName);
            return clone;
        }
    });
});
