/**
 * Created by antonkw on 06.12.14.
 */


function showElementById(id) {
    document.getElementById(id).style.display="inline";
}

function hideElementById(id) {
    document.getElementById(id).style.display="none";
}

function initLoginPanel() {
    try {
        if (tryLogin()) {
            showElementById("userTable");
            setUsernameOnForm();
        }
        else {
            showElementById("loginForm");
        }


    }
    catch(e) {
        alert(e.description);
    }
}

function setUsernameOnForm() {
    document.getElementById("userName").innerHTML=getUsername();
}

function loginPanelsAction() {
    setUsernameOnForm();
    hideElementById("loginForm");
    showElementById("userTable");
}

function getUsername()  {
    return getCookie("user");
}

function deleteCookie(name) {
    document.cookie = name + "=" + "; expires=Thu, 01 Jan 1970 00:00:01 GMT";
}

function logOut() {
    deleteCookie("isLogin");
    deleteCookie("identValue");
    deleteCookie("generalSkinName");
    deleteCookie("backgroundColor");
    deleteCookie("uiFontColor");
    deleteCookie("contentTextColor");
    deleteCookie("scElementsSkinName");
    deleteCookie("isFileSavePanelOn");
    deleteCookie("entryKey");
    deleteCookie("id");
    deleteCookie("user");
    hideElementById("userTable");
    showElementById("loginForm");
}

function goToSettings() {
    location.href = "../settings.jsp";
}

function clearInputByValue(id){
    try {
        if (id.value == "логин" || id.value == "пароль")
            id.value = '' ;
    } catch(e) {
        alert(e.description);
    }

}

function tryForSymbols(str) {
    return !/^\s*(\w+)\s*$/.test(str);
}



function registrateNewUser(form) {
    try {
        if (form.username.value.length < 3 || form.username.value.length > 15) {
            alert("login length must be larger than 2");
            return;
        }
        if (form.username.value.length > 15) {
            alert("login length must be smaller than 16");
            return;
        }
        if (form.username.value.length < 3) {
            alert("pass length must be larger than 2");
            return;
        }
        if (form.username.value.length > 15) {
            alert("pass length must be smaller than 16");
            return;
        }
        if (tryForSymbols(form.username.value) || tryForSymbols(form.pass.value) ){
            alert("incorrect symbols!");
            return;
        }
        jsonrpc = new JSONRpcClient("JSON-RPC");
        var entryKey = makeRandomString();
        result = jsonrpc.RegistrationObject.addNewUser(form.username.value, form.pass.value, entryKey);
        if (result != 0) {
            var d = new Date();
            d.setTime(d.getTime() + (15*24*60*60*1000));
            document.cookie="isLogin=true; path=/; expires=" + d.toUTCString();
            document.cookie="identValue=10; path=/; expires=" + d.toUTCString();
            document.cookie="generalSkinName=defaultSkin; path=/; expires=" + d.toUTCString();
            document.cookie="backgroundColor=white; path=/; expires=" + d.toUTCString();
            document.cookie="uiFontColor=black; path=/; expires=" + d.toUTCString();
            document.cookie="contentTextColor=black; path=/; expires=" + d.toUTCString();
            document.cookie="scElementsSkinName=defaultSCSkin; path=/; expires=" + d.toUTCString();
            document.cookie="isFileSavePanelOn=0; path=/; expires=" + d.toUTCString();
            document.cookie="id=" + result + "; path=/; expires=" + d.toUTCString();
            document.cookie="entryKey=" + entryKey + "; path=/; expires=" + d.toUTCString();
            document.cookie="user=" + form.username.value + "; path=/; expires=" + d.toUTCString();
            alert("Вы зарегистрированы");
            loginPanelsAction();
        }
        else {
            alert("come up with other username!")
        }
    } catch(e) {
        alert(e.description);
    }
}

function makeRandomString() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 8; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function setFormParametersFromCookies() {
    try {
        var len = document.inputSettings.ident.length;
        for (i = 0; i < len; i++) {
            if ( document.inputSettings.ident[i].value == getCookie("identValue")) {

                document.inputSettings.ident[i].checked = true;
            }
        }
        len = document.inputSettings.skin.length;
        for (i = 0; i < len; i++) {
            if ( document.inputSettings.skin[i].value == getCookie("generalSkinName")) {
                document.inputSettings.skin[i].checked = true;
            }
        }
        len = document.inputSettings.bgcolor.length;
        for (i = 0; i < len; i++) {
            if ( document.inputSettings.bgcolor[i].value == getCookie("backgroundColor")) {
                document.inputSettings.bgcolor[i].checked = true;
            }
        }
        len = document.inputSettings.fontcolor.length;
        for (i = 0; i < len; i++) {
            if ( document.inputSettings.fontcolor[i].value == getCookie("uiFontColor")) {
                document.inputSettings.fontcolor[i].checked = true;
            }
        }len = document.inputSettings.contentcolor.length;
        for (i = 0; i < len; i++) {
            if ( document.inputSettings.contentcolor[i].value == getCookie("contentTextColor")) {
                document.inputSettings.contentcolor[i].checked = true;
            }
        }len = document.inputSettings.scskin.length;
        for (i = 0; i < len; i++) {
            if ( document.inputSettings.scskin[i].value == getCookie("scElementsSkinName")) {
                document.inputSettings.scskin[i].checked = true;
            }
        }
        len = document.inputSettings.fileSavePanel.length;
        for (i = 0; i < len; i++) {
            if ( document.inputSettings.fileSavePanel[i].value == getCookie("isFileSavePanelOn")) {
                document.inputSettings.fileSavePanel[i].checked = true;
            }
        }
    } catch(e) {
        alert(e.description);
    }
}

function setSettings(form) {
    try {
        var login = getCookie("isLogin");
        var id = getId();
        jsonrpc = new JSONRpcClient("JSON-RPC");        jsonrpc.SettingSetterObject.setAllSettings(getCookie("entryKey"), id, form.ident.value, form.skin.value, form.bgcolor.value, form.fontcolor.value, form.contentcolor.value, form.scskin.value, form.fileSavePanel.value);
        var d = new Date();
        d.setTime(d.getTime() + (15*24*60*60*1000));
        document.cookie="isLogin=true; path=/; expires=" + d.toUTCString();
        document.cookie="identValue=" + form.ident.value + "; path=/; expires=" + d.toUTCString();
        document.cookie="generalSkinName="+ form.skin.value + "; path=/; expires=" + d.toUTCString();
        document.cookie="backgroundColor=" + form.bgcolor.value + "; path=/; expires=" + d.toUTCString();
        document.cookie="uiFontColor=" + form.fontcolor.value + "; path=/; expires=" + d.toUTCString();
        document.cookie="contentTextColor=" + form.contentcolor.value + "; path=/; expires=" + d.toUTCString();
        document.cookie="scElementsSkinName=" +  form.scskin.value +"; path=/; expires=" + d.toUTCString();
        document.cookie="isFileSavePanelOn=" +  form.fileSavePanel.value + "; path=/; expires=" + d.toUTCString();
        document.cookie="id=" + id + "; path=/; expires=" + d.toUTCString();
        alert("Настройки сохранены!");
    } catch(e) {
        alert(e.description);
    }
}

function initSettingsPage() {
    try {
        if (tryLogin()) {
            showElementById("inputSettingsForm");
            setFormParametersFromCookies();

        }
        else
            alert("settings.jsp only for authorized users!");
    } catch(e) {
        alert(e.description);
    }
}

function login(form) {
    try {

        jsonrpc = new JSONRpcClient("JSON-RPC");
        var entryKey = makeRandomString();
        result = jsonrpc.SettingsGetObject.getAllSettings(entryKey, form.username.value, form.pass.value);
        if (result != "wrong") {
            var res = result.split(" ");
            var d = new Date();
            d.setTime(d.getTime() + (15*24*60*60*1000));
            document.cookie="user=" + form.username.value + "; path=/; expires=" + d.toUTCString();
            document.cookie="isLogin=true; path=/; expires=" + d.toUTCString();
            document.cookie="identValue=" + res[1] + "; path=/; expires=" + d.toUTCString();
            document.cookie="generalSkinName=" + res[2] + "; path=/; expires=" + d.toUTCString();
            document.cookie="backgroundColor=" + res[3] + "; path=/; expires=" + d.toUTCString();
            document.cookie="uiFontColor=" + res[4] + "; path=/; expires=" + d.toUTCString();
            document.cookie="contentTextColor=" + res[5] + "; path=/; expires=" + d.toUTCString();
            document.cookie="scElementsSkinName=" + res[6] + "; path=/; expires=" + d.toUTCString();
            document.cookie="isFileSavePanelOn=" + res[7] + "; path=/; expires=" + d.toUTCString();
            document.cookie="id=" + res[0] + "; path=/; expires=" + d.toUTCString();
            document.cookie="entryKey=" + entryKey + "; path=/; expires=" + d.toUTCString();
            loginPanelsAction();
        }
        else {
            alert("incorrect login/pass");
        }
    } catch(e) {

    }

}

