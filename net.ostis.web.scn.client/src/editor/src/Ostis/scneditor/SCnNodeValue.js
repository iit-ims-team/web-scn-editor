addInitClass(function () {

    $.extend(Ostis.scneditor.SCnNodeValue.prototype, {

        getJQueryNode: function () {

            if (this.$node == null) {
                this.createJQueryNode();
            }
            return this.$node;
        },

        createJQueryNode: function () {

            this.$node = Ostis.scneditor.SCnNodeValue.createInput(this);
            Ostis.scneditor.SCnNodeValue.addAutocompletion(this.$node, this);
            this.$node = this.$node.parent();
            if (this.settedText) {
                this.textBeforeEditing = this.settedText;
                this.setText(this.settedText)
            }
        },

        getText: function () {

            return this.settedText;
        },

        setText: function (text) {

            this.settedText = text || Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER;
            this.getJQueryNode().text(this.settedText);
        },

        setFocus: function () {
            this.getJQueryNode().find('input').putCursorAtEnd();
        },

        edit: function () {
            var $input = this.$node;
            var $div = $("<div/>");
            $div.addClass('textNodeValue');
            this.$node = $div;
            $input.after($div).detach();
            var $inp = Ostis.scneditor.SCnNodeValue.createInput(this);
            Ostis.scneditor.SCnNodeValue.addAutocompletion($inp, this);
            this.$node = $inp.parent();
            $div.after(this.$node).detach();

            !this.isEditing() && this.getJQueryNode().trigger("dblclick");
            this.setFocus();
            this.textBeforeEditing = this.settedText;
            return false;
        },

        hasChildren: function () {

            return false;
        },

        hasSelecting: function () {

            return false;
        },

        toJSON: function () {

            var jsonRepresentation = {};
            jsonRepresentation.label = this.getText();
            jsonRepresentation.scaddr = this.scaddr;
            jsonRepresentation.obj = this;
            return jsonRepresentation;
        },

        getSelectableSCnNodes: function () {

            return [];
        },

        isEditing: function () {

            return this.getJQueryNode().find("input").length > 0;
        },

        cancelEditing: function () {
            this.setText(this.textBeforeEditing);
            this.leaveSelecting()
        },

        leaveSelecting: function () {
            if (this.isEditing() === true) {
                this.textBeforeEditing = this.settedText;
                this.setTextFromInput();
            }
        },

        setTextFromInput: function (selectParentAfterClick) {

            var inputValue = $(this.getJQueryNode().find("input")[1]).val();
            if (this.settedText !== inputValue) {
                Ostis.scneditor.addCommand(new Ostis.scneditor.SetTextSCNodeCommand(this, inputValue));
                if (this.parentSCNode != undefined && selectParentAfterClick === true) {
                    this.parentSCNode.afterUserSetText();
                }
            } else {
                this.setText(inputValue);
            }
        },

        clone: function () {

            var clone = new Ostis.scneditor.SCnNodeValue();
            clone.createJQueryNode();
            clone.setText(this.getText());
            return clone;
        }

    });

    Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER = "...";

    Ostis.scneditor.SCnNodeValue.bindSetTextByEnter = function (scnNode, $input) {

        $input.keypress(function (keypressEvent) {

            if (keypressEvent.keyCode == Ostis.scneditor.KeyCode.ENTER_CODE) {
                scnNode.setTextFromInput(true);
            }
        });
    };

    Ostis.scneditor.SCnNodeValue.bindCancelEditing = function (scnNode, $input) {
        $input.keydown(function (evt) {
            evt.keyCode == Ostis.scneditor.KeyCode.ESC_CODE && scnNode.cancelEditing()
        });
    };

    Ostis.scneditor.SCnNodeValue.createInput = function (scnNode) {

        var $input = $("<input/>");
        $input.attr("type", "text");
        $input.attr("data-name", "true");
        $input.keydown(function (keydownEvent) {
            if (keydownEvent.keyCode !== Ostis.scneditor.KeyCode.UP_CODE
                && keydownEvent.keyCode !== Ostis.scneditor.KeyCode.DOWN_CODE
                && keydownEvent.ctrlKey === false) {
                keydownEvent.stopPropagation();
            }
            if (scnNode.$node !== undefined && scnNode.$node.parent().find(".tt-suggestion").length !== 0 &&
                (keydownEvent.keyCode === Ostis.scneditor.KeyCode.UP_CODE
                || keydownEvent.keyCode === Ostis.scneditor.KeyCode.DOWN_CODE)) {
                keydownEvent.stopPropagation();
                return false;
            }
        });
        var text = scnNode.getText();
        if (text === Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER) {
            text = "";
        }
        $input.val(text);
        Ostis.scneditor.SCnNodeValue.bindSetTextByEnter(scnNode, $input);
        Ostis.scneditor.SCnNodeValue.bindCancelEditing(scnNode, $input);
        return $input;
    };

    Ostis.scneditor.SCnNodeValue.addAutocompletion = function ($inputElement, scnNode) {
        $inputElement.addClass('typeahead');
        $inputElement.addClass('scn-input');
        $inputElement.typeahead({
                minLength: 3,
                highlight: true
            },
            {
                name: 'idtf',
                source: function (query, cb) {
                    $inputElement.addClass('search-processing');
                    SCWeb.core.Server.findIdentifiersSubStr(query, function (data) {
                        keys = [];
                        for (key in data) {
                            var list = data[key];
                            for (idx in list) {
                                var value = list[idx]
                                keys.push({name: value[1], addr: value[0], group: key});
                            }
                        }

                        cb(keys);
                        $inputElement.removeClass('search-processing');
                    });
                },
                displayKey: 'name',
                templates: {
                    suggestion: function (item) {

                        //glyphicon glyphicon-globe
                        var html = '';
                        if (item.group === scKeynodes.nrel_idtf) {
                            return '<p class="sc-content">' + item.name + '</p>';
                        } else {
                            var cl = 'glyphicon glyphicon-user';
                            if (item.group === scKeynodes.nrel_system_idtf) {
                                cl = 'glyphicon glyphicon-globe';
                            }
                            return '<p><span class="tt-suggestion-icon ' + cl + '"></span>' + item.name + '</p>';
                        }
                        return '<p>' + item.name + '</p>';
                    }
                }
            }
        ).bind('typeahead:selected', function (evt, item) {
            evt.stopPropagation();
            scnNode.scaddr = item.addr;
            scnNode.setText(item.name);
        });

        //Somehow, typehead plugin sets spellcheck to false, so, for now, i'll set it manually here
        $inputElement.attr('spellcheck', true);
    };
});
