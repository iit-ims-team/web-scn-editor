package editor.ostis.server;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@MultipartConfig
@WebServlet("/upload/")
public class UploadServlet extends HttpServlet {

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = ScnUtil.uploadArticle(request.getPart("uploadingFile"));
        response.setContentType("application/text");
        response.getOutputStream().print(fileName);
        response.getOutputStream().flush();
    }
}