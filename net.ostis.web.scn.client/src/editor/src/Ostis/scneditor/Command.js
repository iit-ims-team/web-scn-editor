Ostis.scneditor.addCommand = function (command) {

    command.execute();
    if (Ostis.scneditor.isAddingCommandsPrevented() === true) {
        Ostis.scneditor.setPreventAddingCommands(false);
        return;
    }
    if (Ostis.scneditor.lastExecutedCommandIndex != null) {
        Ostis.scneditor.commands.splice(Ostis.scneditor.lastExecutedCommandIndex + 1, Ostis.scneditor.commands.length
            - Ostis.scneditor.lastExecutedCommandIndex - 1);
    }
    Ostis.scneditor.commands.push(command);
    Ostis.scneditor.lastExecutedCommandIndex = Ostis.scneditor.commands.length - 1;
    (function () {

        var commandIndex = Ostis.scneditor.lastExecutedCommandIndex;
        Ostis.scneditor.SCnArticle.getHistoryElement().historyPanel("addCommand", {
            commandText: command.description(),
            commandIndex: commandIndex,
            commandClickCallback: function () {

                if (commandIndex < Ostis.scneditor.lastExecutedCommandIndex) {
                    for (var i = Ostis.scneditor.lastExecutedCommandIndex; i > commandIndex; --i) {
                        Ostis.scneditor.unexecuteCommand();
                    }
                    Ostis.scneditor.lastExecutedCommandIndex = commandIndex;
                } else {
                    for (var i = Ostis.scneditor.lastExecutedCommandIndex + 1; i <= commandIndex; ++i) {
                        Ostis.scneditor.executeUnexecutedCommand();
                    }
                    Ostis.scneditor.lastExecutedCommandIndex = commandIndex;
                }
            }
        });
    })();

};

Ostis.scneditor.setPreventAddingCommands = function (arePrevented) {
    Ostis.scneditor.preventAddingCommands = arePrevented;
};

Ostis.scneditor.isAddingCommandsPrevented = function () {
    return Ostis.scneditor.preventAddingCommands;
};


Ostis.scneditor.executeUnexecutedCommand = function () {

    if (Ostis.scneditor.lastExecutedCommandIndex !== (Ostis.scneditor.commands.length - 1)) {
        Ostis.scneditor.SCnArticle.getHistoryElement().historyPanel("selectCommand", {
            commandIndex: Ostis.scneditor.lastExecutedCommandIndex + 1
        });
        Ostis.scneditor.commands[Ostis.scneditor.lastExecutedCommandIndex + 1].execute();
        Ostis.scneditor.lastExecutedCommandIndex += 1;
    }
};

Ostis.scneditor.unexecuteCommand = function () {

    if (Ostis.scneditor.lastExecutedCommandIndex >= 0) {
        Ostis.scneditor.SCnArticle.getHistoryElement().historyPanel("unSelectCommand", {
            commandIndex: Ostis.scneditor.lastExecutedCommandIndex
        });
        Ostis.scneditor.commands[Ostis.scneditor.lastExecutedCommandIndex].unexecute();
        Ostis.scneditor.lastExecutedCommandIndex -= 1;
    }
};

Ostis.scneditor.addSCArcCommand = function (child, parentSCnNode) {

    this.child = child;
    this.parentSCnNode = parentSCnNode;
};

Ostis.scneditor.addSCArcCommand.prototype.execute = function () {

    this.parentSCnNode.addSCArc(this.child);
    Ostis.scneditor.SCnArticle.setDefaultAddingArcSign(this.child.getSign().clone());
};

Ostis.scneditor.addSCArcCommand.prototype.unexecute = function () {

    this.child.remove();
};

Ostis.scneditor.addSCArcCommand.prototype.description = function () {

    return 'Добавление дуги ' + this.child.getSign().getText();
};

Ostis.scneditor.ChangeArcTypeCommand = function (arc) {

    this.arc = arc;
};

Ostis.scneditor.ChangeArcTypeCommand.prototype.execute = function () {

    this.arc.changeArcType();
};

Ostis.scneditor.ChangeArcTypeCommand.prototype.unexecute = function () {

    this.arc.changeArcType();
};

Ostis.scneditor.ChangeArcTypeCommand.prototype.description = function () {

    return 'Изменение типа дуги';
};

Ostis.scneditor.CleanSCnArticleCommand = function (scnArticle) {

    this.scnArticle = scnArticle;
    this.oldKeyword = scnArticle.getNodeValue().getText();
    this.oldSCArcs = [];
    Ostis.scneditor.ArrayUtils.addAll(scnArticle.getSCArcs(), this.oldSCArcs);
};

Ostis.scneditor.CleanSCnArticleCommand.prototype.execute = function () {

    this.scnArticle.clean();
};

Ostis.scneditor.CleanSCnArticleCommand.prototype.unexecute = function () {

    this.scnArticle.setText(this.oldKeyword);
    var scnArticle = this.scnArticle;
    this.oldSCArcs.forEach(function (oldArc) {

        scnArticle.addSCArc(oldArc);
    });
};

Ostis.scneditor.CleanSCnArticleCommand.prototype.description = function () {

    return 'Очищение статьи';
};

Ostis.scneditor.RemoveSCArcCommand = function (removingRelation, parentSCnNode) {

    this.removingRelation = removingRelation;
    this.parentSCnNode = parentSCnNode;
    this.childIndex = parentSCnNode.getSCArcIndex(removingRelation);
};

Ostis.scneditor.RemoveSCArcCommand.prototype.execute = function () {

    this.removingRelation.remove();
};

Ostis.scneditor.RemoveSCArcCommand.prototype.unexecute = function () {

    this.parentSCnNode.insertSCArc(this.removingRelation, this.childIndex);
};

Ostis.scneditor.RemoveSCArcCommand.prototype.description = function () {

    return 'Удаление дуги ' + this.removingRelation.getSign().getText();
};

Ostis.scneditor.SetTextSCNodeCommand = function (scNode, newText) {

    this.scNode = scNode;
    if (scNode.getText() != null) {
        this.oldText = scNode.getText();
    } else {
        this.oldText = "";
    }
    this.newText = newText;
    this.oldUpdateStatus = scNode.needUpdateInSC;
};

Ostis.scneditor.SetTextSCNodeCommand.prototype.execute = function () {

    this.scNode.setText(this.newText);
    this.scNode.needUpdateInSC = true;
};

Ostis.scneditor.SetTextSCNodeCommand.prototype.unexecute = function () {

    this.scNode.setText(this.oldText);
    this.scNode.needUpdateInSC = this.oldUpdateStatus;
};

Ostis.scneditor.SetTextSCNodeCommand.prototype.description = function () {

    return 'Изменение текста';
};

Ostis.scneditor.ChangeSignBinaryRelationCommand = function (binaryRelation, newSign) {

    this.binaryRelation = binaryRelation;
    this.oldSign = binaryRelation.getSign().clone();
    this.newSign = newSign;
};

Ostis.scneditor.ChangeSignBinaryRelationCommand.prototype.execute = function () {

    this.binaryRelation.setSign(this.newSign);
};

Ostis.scneditor.ChangeSignBinaryRelationCommand.prototype.unexecute = function () {

    this.binaryRelation.setSign(this.oldSign);
};

Ostis.scneditor.ChangeSignBinaryRelationCommand.prototype.description = function () {

    return 'Изменение типа отношения на ' + this.newSign.getText();
};

Ostis.scneditor.AddAttributeCommand = function (scnNode) {

    this.scnNode = scnNode;
    this.newAttribute = new Ostis.scneditor.SCAttribute(this.scnNode);
};

Ostis.scneditor.AddAttributeCommand.prototype.execute = function () {

    this.scnNode.addSCAttribute(this.newAttribute);
};

Ostis.scneditor.AddAttributeCommand.prototype.unexecute = function () {

    this.newAttribute.remove();
};

Ostis.scneditor.AddAttributeCommand.prototype.description = function () {

    return 'Добавление аттрибута';
};

Ostis.scneditor.ChangeNodeTypeCommand = function (scnNode, newNodeValue) {

    this.scnNode = scnNode;
    this.oldNodeValue = scnNode.getNodeValue();
    this.newNodeValue = newNodeValue;
};

Ostis.scneditor.ChangeNodeTypeCommand.prototype.execute = function () {

    this.scnNode.setNodeValue(this.newNodeValue);
};

Ostis.scneditor.ChangeNodeTypeCommand.prototype.unexecute = function () {

    this.scnNode.setNodeValue(this.oldNodeValue);
};

Ostis.scneditor.ChangeNodeTypeCommand.prototype.description = function () {

    return 'Изменение типа узла';
};

Ostis.scneditor.ChangeLink = function (scLink, newData, newType) {

    this.scLink = scLink;
    this.oldData = scLink.data;
    this.newData = newData;
    this.oldType = scLink.type;
    this.newType = newType;
    this.oldUpdateStatus = scLink.needUpdateInSC;
};

Ostis.scneditor.ChangeLink.prototype.execute = function () {

    this.scLink.setContent(this.newData, this.newType);
    this.scLink.needUpdateInSC = true;
};

Ostis.scneditor.ChangeLink.prototype.unexecute = function () {

    this.scLink.setContent(this.oldData, this.oldType);
    this.scLink.needUpdateInSC = this.oldUpdateStatus;
};

Ostis.scneditor.ChangeLink.prototype.description = function () {

    return 'Изменение sc-ссылки';
};

Ostis.scneditor.RemoveSCAttributeCommand = function (attribute, scnRelation) {

    this.attribute = attribute;
    this.scnRelation = scnRelation;
};

Ostis.scneditor.RemoveSCAttributeCommand.prototype.execute = function () {

    var attributes = this.scnRelation.getSCAttributes();
    for (var attrIndex = 0; attrIndex < attributes.length; ++attrIndex) {
        if (attributes[attrIndex].getId() == this.attribute.getId()) {
            this.removingAttrIndex = attrIndex;
            break;
        }
    }
    this.attribute.remove();
};

Ostis.scneditor.RemoveSCAttributeCommand.prototype.unexecute = function () {

    this.scnRelation.insertSCAttribute(this.attribute, this.removingAttrIndex);
};

Ostis.scneditor.RemoveSCAttributeCommand.prototype.description = function () {

    return 'Удаление аттрибута';
};

Ostis.scneditor.AddSCNodeCommand = function (scParent, scNode) {

    this.scParent = scParent;
    this.scNode = scNode;
};

Ostis.scneditor.AddSCNodeCommand.prototype.execute = function () {

    this.scParent.addSCNode(this.scNode);
};

Ostis.scneditor.AddSCNodeCommand.prototype.unexecute = function () {

    this.scNode.remove();
};

Ostis.scneditor.AddSCNodeCommand.prototype.description = function () {

    return 'Добавление узла';
};

Ostis.scneditor.AddKeywordCommand = function (scContour, newScContour) {

    this.scContour = scContour;
    this.newScContour = newScContour;
};

Ostis.scneditor.AddKeywordCommand.prototype.execute = function () {

    this.scContour.addContour(this.newScContour);
};

Ostis.scneditor.AddKeywordCommand.prototype.unexecute = function () {

    this.scContour.removeContour(this.newScContour);
};

Ostis.scneditor.AddKeywordCommand.prototype.description = function () {

    return 'Добавление ключевого узла';
};

Ostis.scneditor.RemoveKeywordCommand = function (deleteContour) {

    this.deleteContour = deleteContour;
    this.scContourParents = deleteContour.scContourParents;
    this.deleteSrticle = new Ostis.scneditor.CleanSCnArticleCommand(deleteContour);
};

Ostis.scneditor.RemoveKeywordCommand.prototype.execute = function () {

    this.deleteSrticle.execute();
    this.scContourParents.removeContour(this.deleteContour)
};

Ostis.scneditor.RemoveKeywordCommand.prototype.unexecute = function () {


    this.scContourParents.addContour(this.deleteContour);
    this.deleteSrticle.unexecute();
};

Ostis.scneditor.RemoveKeywordCommand.prototype.description = function () {

    return 'Удаление ключевого узла';
};

Ostis.scneditor.RemoveSCNodeCommand = function (scNode, scParentArc) {

    this.scNode = scNode;
    this.scParentArc = scParentArc;
    this.scParentNode = scParentArc.parentSCNode;
    this.scArcIndex = this.scParentNode.getSCArcIndex(scParentArc);
};

Ostis.scneditor.RemoveSCNodeCommand.prototype.execute = function () {

    this.scNode.remove();
};

Ostis.scneditor.RemoveSCNodeCommand.prototype.unexecute = function () {

    this.scParentNode.insertSCArc(this.scParentArc, this.scArcIndex);
};

Ostis.scneditor.RemoveSCNodeCommand.prototype.description = function () {

    return 'Удаление узла';
};

Ostis.scneditor.RemoveSCNodesCommand = function (elements) {

    this.elements = elements;
};

Ostis.scneditor.RemoveSCNodesCommand.prototype.execute = function () {

    this.commands = [];
    for (var i = this.elements.length - 1; i >= 0; i--) {
        var delCommand = this.elements[i].getDelCommand();
        if (delCommand !== null) {
            this.commands.push(delCommand);
            delCommand.execute();
        } else {
            Ostis.scneditor.setPreventAddingCommands(true);
        }
    }
};

Ostis.scneditor.RemoveSCNodesCommand.prototype.unexecute = function () {

    for (var i = this.commands.length - 1; i >= 0; i--) {
        this.commands[i].unexecute();
    }
};

Ostis.scneditor.RemoveSCNodesCommand.prototype.description = function () {

    return 'Удаление узлов';
};

Ostis.scneditor.ConvertFromJSONCommand = function (articleNode, articleJSON) {

    this.articleNode = articleNode;
    this.articleJSON = articleJSON;
    this.cleanArticleCommand = new Ostis.scneditor.CleanSCnArticleCommand(articleNode);
    this.wasUnexecuted = false;
};

Ostis.scneditor.ConvertFromJSONCommand.prototype.execute = function () {

    this.cleanArticleCommand.execute();
    if (this.wasUnexecuted === false) {
        Ostis.scneditor.JSONParser.makeSCArticleFromJSON(this.articleJSON, this.articleNode);
    } else {
        this.articleNode.setText(this.newKeyword);
        var scnArticle = this.articleNode;
        this.newArcs.forEach(function (newArc) {

            scnArticle.addSCArc(newArc);
        });
    }
};

Ostis.scneditor.ConvertFromJSONCommand.prototype.unexecute = function () {

    this.wasUnexecuted = true;
    this.newKeyword = this.articleNode.getNodeValue().getText();
    this.newArcs = [];
    Ostis.scneditor.ArrayUtils.addAll(this.articleNode.getSCArcs(), this.newArcs);
    this.articleNode.clean();
    this.cleanArticleCommand.unexecute();
};

Ostis.scneditor.ConvertFromJSONCommand.prototype.description = function () {

    return 'Конвертация из json';
};

Ostis.scneditor.AppendToCommand = function (parentNode, childNode) {

    this.parentNode = parentNode;
    this.childNode = childNode;
};

Ostis.scneditor.AppendToCommand.prototype.execute = function () {

    this.childNode.appendTo(this.parentNode);
};

Ostis.scneditor.AppendToCommand.prototype.unexecute = function () {

    this.childNode.remove();
};

Ostis.scneditor.AppendToCommand.prototype.description = function () {

    return 'Добавление в конец';
};

Ostis.scneditor.ChangeDirectionCommand = function (arc) {

    this.arc = arc;
};

Ostis.scneditor.ChangeDirectionCommand.prototype.execute = function () {

    this.arc.getSign().changeDirection();
    this.arc.updateSignView();
};

Ostis.scneditor.ChangeDirectionCommand.prototype.unexecute = function () {

    this.arc.getSign().changeDirection();
    this.arc.updateSignView();
};

Ostis.scneditor.ChangeDirectionCommand.prototype.description = function () {

    return 'Изменение направления';
};
