Ostis.scneditor.LAST_SCN_NODE_ID = -1;

addInitClass(function () {

    $.extend(AbstractSCnElement.prototype, {
        getId: function () {

            return this.id;
        },

        getJQueryNode: function () {

            if (this.$node == null) {
                this.createJQueryNode();
            }
            return this.$node;
        },

        changeNodeValueTo: function () {

            // Do nothing.
        },

        addAttributeClick: function () {

            // Do nothing.
        },

        changeArcTypeClick: function () {

            // Do nothing.
        },

        bindSelectNodeByClick: function ($node) {

            var scnNode = this;
            $node.mousedown(function (e) {

                // Prevent selecting text on selecting nodes.
                if (e.ctrlKey || e.shiftKey) {
                    e.preventDefault();
                }
            });
            $node.keydown(function (e) {

                // Prevent selecting text on selecting nodes.
                if (e.ctrlKey) {
                    e.preventDefault();
                }
            });
            $node.bind("click", $node, function (clickEvent) {
                var $node = clickEvent.data;
                Ostis.scneditor.SCnEditor.setSelectedSCnNode(scnNode, clickEvent.shiftKey, clickEvent.ctrlKey);
                return false;
            });
            $node.bind("dblclick", function (clickEvent) {
                Ostis.scneditor.SCnEditor.editSelectedNode();
                return false;
            });
            $node.each(function (index, node) {
                $(node.childNodes).each(function (index, childNode) {
                    $(childNode).bind("click", scnNode, function (clickEvent) {
                        var $node = clickEvent.data;
                        Ostis.scneditor.SCnEditor.setSelectedSCnNode(
                            scnNode,
                            clickEvent.shiftKey,
                            clickEvent.ctrlKey,
                            clickEvent.currentTarget);
                        return false;
                    });
                    //console.log(childNode);
                });
            });
        },

        forEachChildNode: function ($node, action) {
            $node.each(function (index, node) {
                $(node.childNodes).each(function (index, childNode) {
                    if (action) {
                        action($(childNode));
                    }
                });
            });
        },

        addSCArcClick: function (binaryRelation) {

            // Do nothing.
        },

        rightClick: function (keydownEvent) {

            if (this.getSelectableSCnNodes().length > 1) {
                var isShiftKey = keydownEvent === undefined ? false : keydownEvent.shiftKey;
                Ostis.scneditor.SCnEditor.setSelectedSCnNode(this.getSelectableSCnNodes()[1], isShiftKey);
            }
        },

        leftClick: function (keydownEvent) {

            this.parentSCNode.childWasLeftClick(keydownEvent);
        },

        childWasLeftClick: function (keydownEvent) {

            var isShiftKey = keydownEvent === undefined ? false : keydownEvent.shiftKey;
            Ostis.scneditor.SCnEditor.setSelectedSCnNode(this, isShiftKey);
        },

        getDelCommand: function () {

            return null;
        },

        tClick: function () {

            // Do nothing.
        },

        leaveSelecting: function () {
            var jQueryNode = this.getJQueryNode();
            jQueryNode.removeClass("selectedSCnNode");
            this.forEachChildNode(jQueryNode, function (childNode) {
                childNode.removeClass("selectedSCnNode");
            });
        },

        ctrlEnterClick: function () {

            return true;
        },

        setSelected: function (selectedChildNode) {
            var selectedElement = selectedChildNode ? selectedChildNode : this.getJQueryNode();
            $(selectedElement).addClass("selectedSCnNode");
        },

        canChangeNodeValue: function () {

            return false;
        },

        isArc: function () {

            return false;
        },

        canAddArc: function () {

            return false;
        },

        canAddSCNodeOfSet: function () {

            return false;
        },

        canAppendTo: function (parent) {

            return false;
        },

        afterUserSetText: function () {

            // Do nothing.
        },

        rClick: function () {

            // Do nothing.
        }

    });

    AbstractSCnElement.getSelectableNodesFromArray = function (nodes) {

        var selectableNodes = [];
        nodes.forEach(function (node) {

            Ostis.scneditor.ArrayUtils.addAll(node.getSelectableSCnNodes(), selectableNodes);
        });
        return selectableNodes;
    };
});
