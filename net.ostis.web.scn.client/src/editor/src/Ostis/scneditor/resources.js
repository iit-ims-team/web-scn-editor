Ostis.scneditor.resources = new Object();

addInitClass(function () {

    SCWeb.core.Server.resolveScAddr(["ui_scneditor_cansel_changes_question"], function (scaddr) {
        SCWeb.core.Server.resolveIdentifiers([scaddr['ui_scneditor_cansel_changes_question']], function (idtfs) {
            Ostis.scneditor.resources['canselChangesQuestion'] = idtfs[scaddr['ui_scneditor_cansel_changes_question']];
        });
    });
});