Ostis.scneditor.KeynodesHandler = {
    systemIds: ['sc_garbage'],
    systemUiIds: ['ui_editor_button_add_attribute', 'ui_editor_button_change_arc_type',
        'ui_editor_button_add_contour', 'ui_editor_button_add_element',
        'ui_editor_button_add_text', 'ui_editor_button_save',
        'ui_editor_button_json_export', 'ui_editor_button_scs_export',
        'ui_editor_button_download', 'ui_editor_select_template',
        'ui_editor_button_link',
        'ui_editor_accordion_constant_arcs',
        'ui_editor_accordion_variable_arcs',
        'ui_editor_accordion_other_arcs', 'ui_editor_label_toolbar',
        'ui_editor_label_history'],
    systemUiId2selectorId: {
        'ui_editor_button_add_attribute': '#addAttributeButton',
        'ui_editor_button_change_arc_type': '#changeArcTypeButton',
        'ui_editor_button_add_contour': '#changeNodeValueToContourButton',
        'ui_editor_button_add_element': '#changeNodeValueToSetButton',
        'ui_editor_button_add_text': '#changeNodeValueToTextButton',
        'ui_editor_button_link': '#changeLinkValueButton',
        'ui_editor_button_save': '#toolbarSaveButton',
        'ui_editor_button_json_export': '#exportToJsonButton',
        'ui_editor_button_scs_export': '#exportToScsButton',
        'ui_editor_button_download': '#downloadArticle',
        'ui_editor_select_template': '#selectedTemplate',
        'ui_editor_accordion_constant_arcs': '',
        'ui_editor_accordion_variable_arcs': '',
        'ui_editor_accordion_other_arcs': '',
        'ui_editor_label_toolbar': '#toolbarToggleButton'
        // 'ui_editor_label_history': '#historyPanelButton'
    },
    scKeynodes: {},
    noDefaultCommandNodes: [],
    uiElemAddreses: [],
    init: function (addrs) {
        this.noDefaultCommandNodes = [];
        this.uiElemAddreses = [];
        this.initSystemIds();
        for (var systemIdInd = 0; systemIdInd < this.systemUiIds.length; systemIdInd++) {
            var sysIdName = this.systemUiIds[systemIdInd];
            if (addrs[sysIdName]) {
                console.log('Resolved keynode for editor ui, ' + sysIdName + ': '
                    + addrs[sysIdName]);
                this.uiElemAddreses.push(addrs[sysIdName]);
                this.noDefaultCommandNodes.push({
                    scAddr: addrs[sysIdName],
                    node: $(this.systemUiId2selectorId[sysIdName])
                });
            } else {
                console.log('keynode for editor ui, ' + sysIdName + ' not resolved');
            }
        }
    },

    initSystemIds: function () {
        var self = this;
        SCWeb.core.Server.resolveScAddr(this.systemIds, function (keynodes) {
            Object.getOwnPropertyNames(keynodes).forEach(function (key) {
                console.log('Resolved keynode: ' + key + ' = ' + keynodes[key]);
                self.scKeynodes[key] = keynodes[key];
            });
        });
    },

    bindUiToMemory: function () {
        this.noDefaultCommandNodes.forEach(function (nodeInfo) {
            nodeInfo.node.addClass('sc-no-default-cmd').attr('sc_addr', nodeInfo.scAddr);
        });
    },

    addContentAddrs: function (addrs) {
        this.uiElemAddreses = this.uiElemAddreses.concat(addrs);
    }
};