/**
 * Created by antonkw on 06.12.14.
 */
function getCookie(name) {
    var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function tryLogin() {
    return getCookie("isLogin") == "true";
}

function isIdentValueExist() {
    return getCookie("identValue") != undefined;
}

function isGeneralSkinNameExist() {
    return getCookie("generalSkinName") != undefined;
}

function isBackgroundColorExist() {
    return getCookie("backgroundColor") != undefined;
}

function isUiFontColorExist() {
    return getCookie("uiFontColor") != undefined;
}

function isContentTextColorExist() {
    return getCookie("contentTextColor") != undefined;
}

function isScElementsSkinNameExist() {
    return getCookie("scElementsSkinName") != undefined;
}

function isFileSavePanelOnExist() {
    return getCookie("isFileSavePanelOn") != undefined;
}

function isEntryKeyExist() {
    return getCookie("entryKey") != undefined;
}

function isIdExist() {
    return getCookie("id") != undefined;
}

function isUserExist() {
    return getCookie("user") != undefined;
}

function getIdentValue() {
    return getCookie("identValue");
}

function getGeneralSkinName() {
    return getCookie("generalSkinName");
}

function getBackgroundColor() {
    return getCookie("backgroundColor");
}

function getUiFontColor() {
    return getCookie("uiFontColor");
}

function getContentTextColor() {
    return getCookie("contentTextColor");
}

function getScElementsSkinName() {
    return getCookie("scElementsSkinName");
}

function getFileSavePanelOn() {
    return getCookie("isFileSavePanelOn");
}

function getEntryKey() {
    return getCookie("entryKey");
}

function getId() {
    return getCookie("id");
}

function getUser() {
    return getCookie("user");
}

function getDefaultIdentValue() {
    return "10";
}

function getDefaultGeneralSkinName() {
    return "defaultSkin";
}

function getDefaultBackgroundColor() {
    return "white";
}

function getDefaultUiFontColor() {
    return "black";
}

function getDefaultContentTextColor() {
    return "black";
}

function getDefaultScElementsSkinName() {
    return "defaultSCSkin";
}

function getDefaultFileSavePanelOn() {
    return "0";
}

function getScsLevelOne () {
    if (!tryLogin()) {
        return "login error";
    }
    var scs;
    scs = "/* SCs_code0.1.0.Level 1 */" + "\n" +
        "user | sc_arc_main#arc1 | settings;;" + "\n" +
        "settings | sc_arc_common#pair1 | uiFontColor;;" + "\n" +
        "settings | sc_arc_common#pair2 | generalScinName;;" + "\n" +
        "settings | sc_arc_common#pair3 | contentTextColor;;" + "\n" +
        "settings | sc_arc_common#pair3 | contentTextColor;;" + "\n" +
        "settings | sc_arc_common#pair4 | backgroundColor;;" + "\n" +
        "settings | sc_arc_common#pair5 | isFileSavePanelOn;;" + "\n" +
        "settings | sc_arc_common#pair6 | identValue;;" + "\n" +
        "settings | sc_arc_common#pair6 | identValue;;" + "\n" +
        "settings | sc_arc_common#pair7 | scElementsSkinName;;" + "\n" +
        "включение* | sc_arc_main#arc2 | sc_arc_common#pair1;;" + "\n" +
        "включение* | sc_arc_main#arc3 | sc_arc_common#pair2;;" + "\n" +
        "включение* | sc_arc_main#arc4 | sc_arc_common#pair3;;" + "\n" +
        "включение* | sc_arc_main#arc5 | sc_arc_common#pair4;;" + "\n" +
        "включение* | sc_arc_main#arc6 | sc_arc_common#pair5;;" + "\n" +
        "включение* | sc_arc_main#arc7 | sc_arc_common#pair6;;" + "\n" +
        "включение* | sc_arc_main#arc8 | sc_arc_common#pair7;;" + "\n" +
        getUiFontColor() + " | sc_arc_common#pair8 | uiFontColor;;" + "\n" +
        getContentTextColor() + " | sc_arc_common#pair9 | contentTextColor;;" + "\n" +
        getGeneralSkinName() + " | sc_arc_common#pair10 | generalSkinName;;" + "\n" +
        getBackgroundColor() + " | sc_arc_common#pair11 | backgroundColor;;" + "\n" +
        getFileSavePanelOn() + " | sc_arc_common#pair12 | isFileSavePanelOn;;" + "\n" +
        getIdentValue() + " | sc_arc_common#pair13 | identValue;;" + "\n" +
        getScElementsSkinName() + " | sc_arc_common#pair14 | uiFontColor;;" + "\n" +
        "value* | sc_arc_main#arc9 | sc_arc_common#pair8;;" + "\n" +
        "value* | sc_arc_main#arc10 | sc_arc_common#pair9;;" + "\n" +
        "value* | sc_arc_main#arc11 | sc_arc_common#pair10;;" + "\n" +
        "value* | sc_arc_main#arc12 | sc_arc_common#pair11;;" + "\n" +
        "value* | sc_arc_main#arc13 | sc_arc_common#pair12;;" + "\n" +
        "value* | sc_arc_main#arc14 | sc_arc_common#pair13;;" + "\n" +
        "value* | sc_arc_main#arc15 | sc_arc_common#pair14;;" + "\n" +
        "colorsAvailableForUiFont | sc_arc_main#arc16 | " + getUiFontColor() +";;" + "\n" +
        "colorsAvailableForContentText | sc_arc_main#arc17 | " + getContentTextColor() +";;" + "\n" +
        "availableGeneralSkins | sc_arc_main#arc18 | " + getGeneralSkinName() +";;" + "\n" +
        "availableBackgroundColors | sc_arc_main#arc19 | " + getBackgroundColor() +";;" + "\n" +
        "availableScElementsSkins | sc_arc_main#arc20 | " + getFileSavePanelOn() +";;" + "\n" +
        "availableIdentValueIdentification | sc_arc_main#arc21 | " + getIdentValue() +";;" + "\n" +
        "availableSavaPanelStatus | sc_arc_main#arc22 | " + getScElementsSkinName() +";;";
    return scs;

}

function showSettingsInScsLevelOne() {
    alert(getScsLevelOne());
}




