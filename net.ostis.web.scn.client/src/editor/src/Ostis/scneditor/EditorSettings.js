(function () {

    Ostis.scneditor.editorSettings.compileNode = function () {

        var settingsNode = Ostis.scneditor.handlebars.render('settings');
        return settingsNode;
    };

    Ostis.scneditor.editorSettings.renderNode = function ($beforeNode) {

        var $settingsNode = $(Ostis.scneditor.editorSettings.compileNode());
        $beforeNode.after($settingsNode);
        checkInputsFromCookiesValues();
        attachHandlersToSettingsNode($settingsNode);
    };

    Ostis.scneditor.editorSettings.getSettingsValuesFromCookies = function () {

        return settingsValues = {
            ident: getCookie("identValue"),
            skin: getCookie("generalSkinName"),
            bgcolor: getCookie("backgroundColor"),
            fontcolor: getCookie("uiFontColor"),
            contentcolor: getCookie("contentTextColor"),
            scskin: getCookie("scElementsSkinName"),
            fileSavePanel: getCookie("isFileSavePanelOn")
        };

    }

    var checkInputsFromCookiesValues = function () { // TODO: Understand why
        // cannot check the inputs
        // when the query selector
        // is settingsNode argument

        var cookiesSettings = Ostis.scneditor.editorSettings.getSettingsValuesFromCookies(),
            valuesNames = Object.keys(cookiesSettings);
        for (var i = 0; i < valuesNames.length; i++) {
            var valueName = valuesNames[i], $inputToCheck = $('#settingsContainer').find(
                '[name="' + valueName + '"]' + '[value="' + cookiesSettings[valueName] + '"]');
            $inputToCheck.prop('checked', true);
        }
    }

    var saveSettings = function () {

        var settings = getSettingsFromInputs(), id = getId(), jsonrpc = new JSONRpcClient("JSON-RPC");

        jsonrpc.SettingSetterObject.setAllSettings(getCookie("entryKey"), id, settings.ident, settings.skin, settings.bgcolor, settings.fontcolor,
            settings.contentcolor, settings.scskin, settings.fileSavePanel);

        Ostis.scneditor.utils.setCookie('isLogin', true, 15);
        Ostis.scneditor.utils.setCookie('identValue', settings.ident, 15);
        Ostis.scneditor.utils.setCookie('generalSkinName', settings.skin, 15);
        Ostis.scneditor.utils.setCookie('backgroundColor', settings.bgcolor, 15);
        Ostis.scneditor.utils.setCookie('uiFontColor', settings.fontcolor, 15);
        Ostis.scneditor.utils.setCookie('contentTextColor', settings.contentcolor, 15);
        Ostis.scneditor.utils.setCookie('scElementsSkinName', settings.scskin, 15);
        Ostis.scneditor.utils.setCookie('isFileSavePanelOn', settings.fileSavePanel, 15);
        Ostis.scneditor.utils.setCookie('id', id, 15);
    }

    var getSettingsFromInputs = function () {

        var settings = {};
        $('#settingsContainer input:checked').each(function () {

            var $this = $(this);
            settings[$this.attr('name')] = $this.attr('value')
        });
        return settings;
    }

    var attachHandlersToSettingsNode = function ($settingsNode) {

        var $saveButton = $settingsNode.find('#saveSettingsButton'),
            $cancelButton = $settingsNode.find('#cancelSettingsButton');

        var saveHandler = function () {

            saveSettings();
            $settingsNode.remove();
        };

        var cancelHandler = function () {

            $settingsNode.remove();
        }

        $saveButton.click(saveHandler);
        $cancelButton.click(cancelHandler);
    }

})()
