addInitClass(function () {

    $.extend(Ostis.scneditor.SCnContourValue.prototype, {

        keyWords: [],

        hasChildren: function () {

            return true;
        },

        getDelCommand: function () {

            return null;
        },

        createJQueryNode: function () {

            this.$node = $("<span/>");
            this.$node.addClass("SCnTransContent");
            var $SCnKeyword = $("<div/>");
            $SCnKeyword.addClass("SCnKeyword");
            $SCnKeyword.append(this.nodeValue.getJQueryNode());
            this.$node.append($SCnKeyword);
            this.bindSelectNodeByClick(this.$node);
        },

        configureNewArcJQueryNode: function () {

            // Do nothing.
        },

        clean: function () {

            this.setText(Ostis.scneditor.SCnNodeValue.EMPTY_TEXT_PLACEHOLDER);
            this.removeAllSCArcs();
        },

        addFirstSCArcJQueryNode: function ($arcNode) {

            this.getJQueryNode().find(".SCnKeyword").first().after($arcNode);
        },

        changeNodeValueTo: function (newNodeValue) {

            if (!this.scContourParents) {
                this.addNewSCnKeyword();
            }
        },

        addNewSCnKeyword: function () {

            Ostis.scneditor.addCommand(new Ostis.scneditor.AddKeywordCommand(this,
                new Ostis.scneditor.SCnContourValue(this.parentSCNode)));
        },

        removeSCnKeyword: function () {

            Ostis.scneditor.addCommand(new Ostis.scneditor.RemoveKeywordCommand(this));
        },

        addContour: function (scContour) {

            scContour.scContourParents = this;
            scContour.createJQueryNode();
            scContour.$node.removeClass("SCnTransContent");
            scContour.$node.addClass("SCnTransContentNext");
            this.$node.append(scContour.getJQueryNode());
            this.keyWords.push(scContour);
        },

        removeContour: function (scContour) {

            this.$node.find(scContour.getJQueryNode()).remove();
            this.keyWords = this.keyWords.filter(function (item) {
                return item !== scContour;
            });
        },

        toJSON: function () {

            var jsonRepresentation = {};
            jsonRepresentation.label = this.getText();
            jsonRepresentation.SCNodes = [];
            jsonRepresentation.SCNodes.push(Ostis.scneditor.SCnContourValue.superclass.toJSON.apply(this));
            this.keyWords.forEach(function (item) {
                jsonRepresentation.SCNodes.push(Ostis.scneditor.SCnContourValue.superclass.toJSON.apply(item));
            });
            jsonRepresentation.contour = true;
            jsonRepresentation.scaddr = this.scaddr;
            jsonRepresentation.obj = this;
            return jsonRepresentation;
        },

        clone: function (parent) {

            var clone = new Ostis.scneditor.SCnContourValue(parent);
            clone.createJQueryNode();
            this.getSCArcs().forEach(function (arc) {

                clone.addSCArc(arc.clone(clone));
            });
            clone.keyWords = [];
            this.keyWords.forEach(function (item) {
                clone.keyWords.push(item);
            });
            if (this.scContourParents) {
                clone.scContourParents = this.scContourParents;
            }
            clone.setNodeValue(this.getNodeValue().clone(clone));
            return clone;
        }
    });
});
