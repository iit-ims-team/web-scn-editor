Ostis.scneditor.WorkbenchActivator = (function () {

    var workbench;
    return {
        start: function (workbenchContainer, sandbox) {

            workbench = new Ostis.scneditor.Workbench(workbenchContainer);
            for (var i = 0; i < Ostis.scneditor.initFunctions.length; i++) {
                Ostis.scneditor.initFunctions[i]();
            }
            workbench.init();
            workbench.render();
            var dfd = new jQuery.Deferred();
            sandbox.resolveAddrs(
                Ostis.scneditor.KeynodesHandler.systemUiIds, //
                function (addrs) {
                    Ostis.scneditor.SCnArticle.newInstance();
                    Ostis.scneditor.KeynodesHandler.init(addrs);
                    Ostis.scneditor.KeynodesHandler.bindUiToMemory();
                    dfd.resolve();
                });
            return dfd.promise();
        },
        destroy: function () {

            workbench.destroy();
        }
    };
})();
